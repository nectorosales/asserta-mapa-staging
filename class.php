<?php

Class CSV{
	private $rows = array();
	private $cleanRows = array();
	private $fields;
	private $file;
	private $f;

	public function __construct($file = null){
		$this->file = $file;
		if ( !$file ) throw new Exception("Debe especificarse un fichero, o una ruta hacia el fichero");
	}
	public function open(){
		if ( $this->file ) $this->f = fopen($this->file, "r");
	}
	public function fetch_row(){
		$txt = stream_get_line($this->f, 0, "\n");
		if ( $txt !== false ){
			$row = str_getcsv ( $txt, ";");
			return $row;
		}

		return false;
	}
	public function num_rows(){
		$lines = 0;
		while ( (stream_get_line($this->f, 0, "\n"))!==false ) $lines += 1;
		rewind($this->f);
		return $lines;
	}
}


Class CentroSanitario Extends RowManager{
	public static $table = "centros_sanitarios";
	public static $pk = "id";
	public static $fields = array("id_usuario", "id_tipo_organizacion", "descripcion", "id_provincia", "id_pais", "id_servicio", "nombre", "url", "creado", "observaciones_genero", "observaciones_idiomas", "requisitos_acceso", "gratuito", "concertado", "subvencionado", "con_cargo_al_paciente", "observaciones_costes", "nombre_responsable", "apellidos_responsable", "direccion", "direccion2", "municipio", "codigo_postal", "email", "telefono", "pagina_web", "horario", "lat", "lng");

	public static function importar(){
		$file = "tmp/asserta-mapa.csv";

		$csv = new CSV($file);
		$csv->open();

		$row = $csv->fetch_row(); //nos saltamos la primera
		$row = $csv->fetch_row(); //nos saltamos la segunda
		$row = $csv->fetch_row(); 

		while ( $row = $csv->fetch_row() ){
			/*id_organizacion*/
			if ( !$row[0] ) continue;

			if ( $row[2] )  $tipo_organizacion = new TipoOrganizacion(array("nombre"=>$row[2]));
			else $tipo_organizacion = new TipoOrganizacion(array("nombre"=>$row[1]));

			$organizacion = new Organizacion(array(
				"nombre" => $row[0],
				"id_tipo_organizacion" => $tipo_organizacion->get("id")
			));

			/*id_provincia*/
			if ( !($row[81] && $row[83]) ) continue;

			$pais = new Pais(array("nombre"=>$row[83]));

			switch ( strtolower($pais->nombre) ){
				case "cat/esp": case "espanya": $pais->nombre = "España"; break;
				case "it": case "italy": $pais->nombre = "Italia"; break;
				case "čr": $pais->nombre = "Česká republika"; break;
				case "greece": $pais->nombre = "Ελλάδα"; break;
				case "germany": $pais->nombre = "Deutschland"; break;
				case "uk": case "england": $pais->nombre = "United Kingdom"; break;
			}

			$provincia = new Provincia(array(
				"id_pais"=>$pais->get("id"),
				"nombre"=>$row[81]));

			switch ( strtolower($provincia->nombre) ){
				case "-": case "---": $provincia->nombre = "N/A"; break;
				case "almería": $provincia->nombre = "Almería"; break;
				case "attika": $provincia->nombre = "Attica"; break;
				case "(Center of Athens)": $provincia->nombre = "Athens"; break;
				case "bologna":case "bo": $provincia->nombre = "Bologna"; break;
			}

			$centro_sanitario = new CentroSanitario(array(
				"nombre"=>$row[0],
				"municipio" => $row[80]
			));
			$centro_sanitario->id = $centro_sanitario->get("id");

			if ( !$centro_sanitario->id ){
				$centro_sanitario = new CentroSanitario(array(
					"id_organizacion"=>$organizacion->get("id"),
					"id_provincia"=>$provincia->get("id"),
					"id_pais"=>$provincia->get("id_pais"),
					"nombre"=>$row[0],
					"observaciones_genero"=>$row[16],
					"observaciones_idiomas"=>$row[75],
					"requisitos_acceso"=>$row[30],
					"observaciones_costes"=>$row[36],
					"nombre_responsable"=>$row[76],
					"apellidos_responsable"=>$row[77],
					"direccion"=>$row[78],
					"direccion2"=>$row[79],
					"municipio" => $row[80],
					"codigo_postal" => $row[82],
					"email" => $row[84],
					"telefono" => $row[85],
					"pagina_web" => $row[86],
					"horario" => $row[87],
					"gratuito" => $row[32] != "",
				));

				if($row[33] != "") $centro_sanitario->concertado = 1;
				if($row[34] != "") $centro_sanitario->subvencionado = 1;
				if($row[35] != "") $centro_sanitario->con_cargo_al_paciente = 1;

				$centro_sanitario->id = $centro_sanitario->insert();
				$centro_sanitario->url = $centro_sanitario->id . "-" . urlfy($centro_sanitario->nombre);
				$centro_sanitario->update();
			}

			for ( $i = 6; $i < 19; $i++ ){
				if ( $i > 10 && $i <=13 ) continue;
				if ( $i == 16 ) continue;

				if ( trim($row[$i]) == '' ) continue;

				$poblacion = new Poblacion(array("nombre"=>$row[$i]));
				$poblacion->id = $poblacion->get("id");

				q("INSERT INTO poblaciones_atendidas(id_centro_sanitario,id_poblacion) VALUES($centro_sanitario->id, $poblacion->id)");
			}

			for ( $i = 11; $i < 29; $i++ ){
				if($i > 12 && $i < 19) continue;
				if ( trim($row[$i]) == '' ) continue;

				$profesional = new Profesional(array("nombre"=>$row[$i]));
				$profesional->id = $profesional->get("id");
					
				q("INSERT INTO profesionales_centros(id_centro_sanitario,id_profesional) VALUES($centro_sanitario->id, $profesional->id)");
			}

			for ( $i = 37; $i<= 74; $i++ ){
				if($row[$i] == NULL ) continue;
				$idioma = new Idioma(array("nombre"=>$row[$i]));

				switch ( $idioma->nombre ){
					case "Arabic" : $idioma->codigo = "ar"; break;
					case "Basque" : $idioma->codigo = "ba"; break;
					case "Bulgarian" : $idioma->codigo = "bg"; break;
					case "Catalan" : $idioma->codigo = "ca"; break;
					case "Chinese" : $idioma->codigo = "zh"; break;
					case "Croatian" : $idioma->codigo = "hr"; break;
					case "Czech" : $idioma->codigo = "cs"; break;
					case "Danish" : $idioma->codigo = "da"; break;
					case "Dutch" : $idioma->codigo = "nl"; break;
					case "English" : $idioma->codigo = "en"; break;
					case "Estonian" : $idioma->codigo = "et"; break;
					case "Finnish" : $idioma->codigo = "fi"; break;
					case "French" : $idioma->codigo = "fr"; break;
					case "Galician" : $idioma->codigo = "de"; break;
					case "German" : $idioma->codigo = "el"; break;
					case "Greek" : $idioma->codigo = "hi"; break;
					case "Hindi" : $idioma->codigo = "hu"; break;
					case "Hungarian" : $idioma->codigo = "ga"; break;
					case "Italian" : $idioma->codigo = "it"; break;
					case "Japanese" : $idioma->codigo = "ja"; break;
					case "Korean" : $idioma->codigo = "ko"; break;
					case "Latvian" : $idioma->codigo = "lv"; break;
					case "Lithuanian" : $idioma->codigo = "lt"; break;
					case "Luxembourgish" : $idioma->codigo = "lb"; break;
					case "Maltese" : $idioma->codigo = "mt"; break;
					case "Polish" : $idioma->codigo = "pl"; break;
					case "Portuguese" : $idioma->codigo = "pt"; break;
					case "Romanian" : $idioma->codigo = "ro"; break;
					case "Russian" : $idioma->codigo = "ru"; break;
					case "Scottish Gaelic" : $idioma->codigo = "gd"; break;
					case "Slovak" : $idioma->codigo = "sk"; break;
					case "Slovenian" : $idioma->codigo = "sl"; break;
					case "Spanish" : $idioma->codigo = "es"; break;
					case "Swedish" : $idioma->codigo = "sw"; break;
					case "Turkish" : $idioma->codigo = "tr"; break;
					case "Urdu" : $idioma->codigo = "ur"; break;
					case "Welsh" : $idioma->codigo = "cy"; break;
					case "Irish" : $idioma->codigo = "ga"; break;
				}

				$idioma->id = $idioma->get("id");
				if ( !$idioma->id  ) $idioma->id = $idioma->insert();

				$idioma_centro = array();
				$idioma_centro = new IdiomaCentro(array("id_centro_sanitario"=>$centro_sanitario->id, "id_idioma"=>$idioma->id));
				if (!$idioma_centro->get("id")) $idioma_centro->insert();
			}


			$codigo_idioma_local = Idioma::detectar($row[3]);
			if ( $codigo_idioma_local ){
				$idioma = idiomas::first("codigo = '".$codigo_idioma_local."'");
				if( $idioma ) {
					$idioma_centro = new IdiomaCentro(array("id_centro_sanitario"=>$centro_sanitario->id, "id_idioma"=>$idioma->id)); 
					$idioma_centro->id = $idioma_centro->get("id");

					if ( $idioma_centro->id ){
						$idioma_centro->descripcion = $row[3];
						$idioma_centro->predeterminado = 1;
						$idioma_centro->update();
					}
				}
			}

			if($row[4]){
				$idioma = idiomas::first("codigo = 'en'");
				if( $idioma ) {
					$idioma_centro = new IdiomaCentro(array("id_centro_sanitario"=>$centro_sanitario->id, "id_idioma"=>$idioma->id)); 
					$idioma_centro->id = $idioma_centro->get("id");

					if ( $idioma_centro->id ){
						$idioma_centro->descripcion = $row[4];
						$idioma_centro->update();
					}
				}
			}

			$codigo_idioma_otro = Idioma::detectar($row[5]);
			if ( $codigo_idioma_otro ){
				$idioma = idiomas::first("codigo = '".$codigo_idioma_otro."'");
				if( $idioma ) {
					$idioma_centro = new IdiomaCentro(array("id_centro_sanitario"=>$centro_sanitario->id, "id_idioma"=>$idioma->id)); 
					$idioma_centro->id = $idioma_centro->get("id");

					if ( $idioma_centro->id ){
						$idioma_centro->descripcion = $row[5];
						$idioma_centro->update();
					}
				}
			}


		}
	}

	public function poblaciones_atendidas(){
		//return PoblacionAtendida::find(array("id_centro_sanitario"=>$this->id));

		$result = q("select id_poblacion FROM poblaciones_atendidas WHERE id_centro_sanitario = " . $this->id);
		
		$arr = array();
		while ( $row = $result->fetch_object() ){
			$arr[] = $row->id_poblacion;
		}

		return poblaciones::find(array("id" => $arr));
	}

	public function profesionales_centros(){
		$result = q("select id_profesional FROM profesionales_centros WHERE id_centro_sanitario = " . $this->id);

		$arr = array();
		while ( $row = $result->fetch_object() ){
			$arr[] = $row->id_profesional;
		}

		return profesionales::find(array("id" => $arr));
	}

	public function idiomas_hablados(){
		$result = q("select * FROM idiomas_centros WHERE id_centro_sanitario = " . $this->id);

		$descripciones = array();
		$ids = array();

		while ( $row = $result->fetch_object() ){
			$ids[] = $row->id_idioma;
			$i = array("id_idioma" => $row->id_idioma,"descripcion" => $row->descripcion, "predeterminado" => $row->predeterminado);
			array_push($descripciones, $i);
		}

		$idiomas_hablados = idiomas::find(array("id" => $ids));
		
		foreach ($idiomas_hablados as $idioma) {
			foreach ($descripciones as $descripcion) {
				//echo "<pre>"; print_r($idioma); echo "</pre>";
				//echo "<pre>"; print_r($descripcion); echo "</pre>"; die();
				if($idioma->id == $descripcion["id_idioma"]){
					$idioma->descripcion = $descripcion["descripcion"];
					$idioma->predeterminado = $descripcion["predeterminado"]=="1"?true:false;
				}
			}
		}

		return $idiomas_hablados;
	}

	public static function distancias($mi_lat, $mi_lng){
		
		$result = q("select id, nombre, (6371 * ACOS(SIN(RADIANS(lat)) * SIN(RADIANS(".$mi_lat.")) + COS(RADIANS(lng - ".$mi_lng.")) * COS(RADIANS(lat)) * COS(RADIANS(".$mi_lat.")))) AS distance FROM centros_sanitarios ORDER BY distance ASC");

		$arr = array();
		while ( $row = $result->fetch_object() ){
			$arr[] = array("id"=>$row->id, "distance"=>$row->distance);	
		}

		return $arr;

	}

	/*public function descripcion_idiomas(){
		$result = q("select id FROM idiomas_centros WHERE id_centro_sanitario = " . $this->id);

		$arr = array();
		while ( $row = $result->fetch_object() ){
			$arr[] = $row->id;
		}
		return idiomas_centros::find(array("id" => $arr));
	}*/

	public function lookupAddress(){
		$provincia = new Provincia($this->get("id_provincia"));
		$pais = new Pais($this->get("id_pais"));

		return $provincia->get("nombre").", ".
			$pais->get("nombre").", ".
			$this->get("direccion").", ".
			$this->get("direccion2").", ".
			$this->get("municipio").", ".
			$this->get("codigo_postal")
		;
	}

	public function fullData(){
		$cs = clone ($this);

		//Nombre del pais
		$p = new Pais($cs->id_pais);
		$cs->pais = $p->get("nombre");

		$to = new TipoOrganizacion($cs->id_tipo_organizacion);
		$cs->tipo_organizacion = $to->get("nombre");
		
		//Poblaciones atendidas
		$cs->poblaciones_atendidas = $cs->poblaciones_atendidas();

		//profesionales ofrecidos
		$cs->profesionales_centros = $cs->profesionales_centros();

		//Idiomas_hablados
		$cs->idiomas_hablados = $cs->idiomas_hablados();

		//Descripcion idiomas
		//$cs->descripcion_idiomas = $cs->descripcion_idiomas();

		return $cs;
	}

}

Class Idioma Extends RowManager{
	public static $table = "idiomas";
	public static $pk = "id";
	public static $fields = array("nombre", "codigo");

	public static function importar(){
		$file = "tmp/asserta-mapa.csv";

		$csv = new CSV($file);
		$csv->open();

		$row = $csv->fetch_row(); //nos saltamos la primera
		$row = $csv->fetch_row(); //nos saltamos la segunda
		$row = $csv->fetch_row(); 
		
		$idioma = array();

		for ( $i = 37; $i<= 74; $i++ ){
			if($row[$i] == NULL ) continue;
			
			$idioma = new Idioma(array("nombre"=>$row[$i]));

			switch ( $idioma->nombre ){
				case "Arabic" : $idioma->codigo = "ar"; break;
				case "Basque" : $idioma->codigo = "ba"; break;
				case "Bulgarian" : $idioma->codigo = "bg"; break;
				case "Catalan" : $idioma->codigo = "ca"; break;
				case "Chinese" : $idioma->codigo = "zh"; break;
				case "Croatian" : $idioma->codigo = "hr"; break;
				case "Czech" : $idioma->codigo = "cs"; break;
				case "Danish" : $idioma->codigo = "da"; break;
				case "Dutch" : $idioma->codigo = "nl"; break;
				case "English" : $idioma->codigo = "en"; break;
				case "Estonian" : $idioma->codigo = "et"; break;
				case "Finnish" : $idioma->codigo = "fi"; break;
				case "French" : $idioma->codigo = "fr"; break;
				case "Galician" : $idioma->codigo = "de"; break;
				case "German" : $idioma->codigo = "el"; break;
				case "Greek" : $idioma->codigo = "hi"; break;
				case "Hindi" : $idioma->codigo = "hu"; break;
				case "Hungarian" : $idioma->codigo = "ga"; break;
				case "Italian" : $idioma->codigo = "it"; break;
				case "Japanese" : $idioma->codigo = "ja"; break;
				case "Korean" : $idioma->codigo = "ko"; break;
				case "Latvian" : $idioma->codigo = "lv"; break;
				case "Lithuanian" : $idioma->codigo = "lt"; break;
				case "Luxembourgish" : $idioma->codigo = "lb"; break;
				case "Maltese" : $idioma->codigo = "mt"; break;
				case "Polish" : $idioma->codigo = "pl"; break;
				case "Portuguese" : $idioma->codigo = "pt"; break;
				case "Romanian" : $idioma->codigo = "ro"; break;
				case "Russian" : $idioma->codigo = "ru"; break;
				case "Scottish Gaelic" : $idioma->codigo = "gd"; break;
				case "Slovak" : $idioma->codigo = "sk"; break;
				case "Slovenian" : $idioma->codigo = "sl"; break;
				case "Spanish" : $idioma->codigo = "es"; break;
				case "Swedish" : $idioma->codigo = "sw"; break;
				case "Turkish" : $idioma->codigo = "tr"; break;
				case "Urdu" : $idioma->codigo = "ur"; break;
				case "Welsh" : $idioma->codigo = "cy"; break;
				case "Irish" : $idioma->codigo = "ga"; break;
			}

			$idioma->id = $idioma->get("id");
			$idioma->nombre = $idioma->get("nombre");
			if ( !$idioma->id && !$idioma->nombre ) $idioma->insert();
		}
	}

	public static function lenguajes(){
		$result = q("select nombre FROM idiomas");
		
		$arr = array();
		while ( $row = $result->fetch_object() ){
			$arr[] = $row->nombre;
		}

		return idiomas::find(array("nombre" => $arr));
	}

	public static function detectar($texto){
		$authorization = "Authorization: Bearer f2ecabf938534b3b8d7f438577fb7844";

		$ch = curl_init(); 
		curl_setopt($ch,CURLOPT_URL,"https://ws.detectlanguage.com/0.2/detect"); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization' , $authorization ));
	    curl_setopt($ch,CURLOPT_POST,true); 
		curl_setopt($ch,CURLOPT_POSTFIELDS,array("q"=>urlencode($texto))); 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    $result = curl_exec($ch);
    	curl_close($ch);

    	$data = json_decode($result);

    	if ( empty($data->data) ) return null;
    	if ( !array_key_exists(0, $data->data->detections) ) return null;
    	return $data->data->detections[0]->language;
	}
}

Class IdiomaCentro Extends RowManager{
	public static $table = "idiomas_centros";
	public static $pk = "id";
	public static $fields = array("id_centro_sanitario", "id_idioma", "descripcion", "predeterminado");
}


Class Organizacion Extends RowManager{
	public static $table = "organizaciones";
	public static $pk = "id";
	public static $fields = array("id_tipo_organizacion", "nombre", "descripcion");

	public static function importar(){
		$file = "tmp/asserta-mapa.csv";

		$csv = new CSV($file);
		$csv->open();

		$row = $csv->fetch_row(); //nos saltamos la primera
		$row = $csv->fetch_row(); //nos saltamos la segunda
		$row = $csv->fetch_row(); //nos saltamos la tercera

		while ( $row = $csv->fetch_row() ){
			if ( !$row[0] ) continue;

			if ( $row[2] )  $tipo_organizacion = new TipoOrganizacion(array("nombre"=>$row[2]));
			else $tipo_organizacion = new TipoOrganizacion(array("nombre"=>$row[1]));

			$organizacion = new Organizacion(array(
				"nombre" => $row[0],
				"id_tipo_organizacion" => $tipo_organizacion->get("id")
			));
			$organizacion->id = $organizacion->get("id");
			$organizacion->descripcion = $row[3];

			if ( !$organizacion->id ) $organizacion->insert();
		}
	}
}

Class Pais Extends RowManager{
	public static $table = "paises";
	public static $pk = "id";
	public static $fields = array("nombre");

	public static function importar(){
		$file = "tmp/asserta-mapa.csv";

		$csv = new CSV($file);
		$csv->open();

		$row = $csv->fetch_row(); //nos saltamos la primera
		$row = $csv->fetch_row(); //nos saltamos la segunda
		$row = $csv->fetch_row(); //nos saltamos la tercera

		while ( $row = $csv->fetch_row() ){
			if ( !$row[83] ) continue;
			$pais = new Pais(array("nombre"=>$row[83]));

			switch ( strtolower($pais->nombre) ){
				case "cat/esp": case "espanya": $pais->nombre = "España"; break;
				case "it": case "italy": $pais->nombre = "Italia"; break;
				case "čr": $pais->nombre = "Česká republika"; break;
				case "greece": $pais->nombre = "Ελλάδα"; break;
				case "germany": $pais->nombre = "Deutschland"; break;
				case "uk": case "england": $pais->nombre = "United Kingdom"; break;
			}

			$pais->id = $pais->get("id");
			if(!$pais->id) $pais->insert();
		}
	}
}

Class Poblacion Extends RowManager{
	public static $table = "poblaciones";
	public static $pk = "id";
	public static $fields = array("nombre", "orden");

	public static function importar(){
			$file = "tmp/asserta-mapa.csv";

			$csv = new CSV($file);
			$csv->open();

			$row = $csv->fetch_row(); //nos saltamos la primera
			$row = $csv->fetch_row(); //nos saltamos la segunda
			$row = $csv->fetch_row(); //nos saltamos la tercera

			$poblaciones = array();

			for ( $i = 6; $i < 99; $i++ ){
				if( $i <= 10)
					$poblaciones = new Poblacion(array("nombre"=>$row[$i]));
				else if( $i > 13 && $i < 16 )
					$poblaciones = new Poblacion(array("nombre"=>$row[$i]));
				else if( $i > 16 && $i < 19 )
					$poblaciones = new Poblacion(array("nombre"=>$row[$i]));
				else 
					continue;

				$poblaciones->id = $poblaciones->get("id");
				if ( !$poblaciones->id ) $poblaciones->insert();
			}	
		}

	}

Class PoblacionAtendida Extends RowManager{
	public static $table = "poblaciones_atendidas";
	public static $pk = "id";
	public static $fields = array("id_poblacion", "id_centro_sanitario");
}

Class Provincia Extends RowManager{
	public static $table = "provincias";
	public static $pk = "id";
	public static $fields = array("id_pais", "nombre");

	public static function importar(){
			$file = "tmp/asserta-mapa.csv";

			$csv = new CSV($file);
			$csv->open();

			$row = $csv->fetch_row(); //nos saltamos la primera
			$row = $csv->fetch_row(); //nos saltamos la segunda
			$row = $csv->fetch_row(); //nos saltamos la tercera

			while ( $row = $csv->fetch_row() ){
				if ( !($row[81] && $row[83]) ) continue;

				$pais = new Pais(array("nombre"=>$row[83]));

				switch ( strtolower($pais->nombre) ){
					case "cat/esp": case "espanya": $pais->nombre = "España"; break;
					case "it": case "italy": $pais->nombre = "Italia"; break;
					case "čr": $pais->nombre = "Česká republika"; break;
					case "greece": $pais->nombre = "Ελλάδα"; break;
					case "germany": $pais->nombre = "Deutschland"; break;
					case "uk": case "england": $pais->nombre = "United Kingdom"; break;
				}

				$provincia = new Provincia(array(
					"id_pais"=>$pais->get("id"),
					"nombre"=>$row[81]));

				switch ( strtolower($provincia->nombre) ){
					case "-": case "---": $provincia->nombre = "N/A"; break;
					case "almería": $provincia->nombre = "Almería"; break;
					case "attika": $provincia->nombre = "Attica"; break;
					case "(Center of Athens)": $provincia->nombre = "Athens"; break;
					case "bologna":case "bo": $provincia->nombre = "Bologna"; break;
				}




				$provincia->id = $provincia->get("id");
				if(!$provincia->id) $provincia->insert();
			}
		}
}

Class Profesional Extends RowManager{
	public static $table = "profesionales";
	public static $pk = "id";
	public static $fields = array("nombre", "orden");

	public static function importar(){
			$file = "tmp/asserta-mapa.csv";

			$csv = new CSV($file);
			$csv->open();

			$row = $csv->fetch_row(); //nos saltamos la primera
			$row = $csv->fetch_row(); //nos saltamos la segunda
			$row = $csv->fetch_row(); //nos saltamos la tercera

			$profesionales = array();

			for ( $i = 11; $i < 29; $i++ ){
				if( $i <= 12)
					$profesionales = new Profesional(array("nombre"=>$row[$i]));
				else if( $i > 18 && $i < 29 )
					$profesionales = new Profesional(array("nombre"=>$row[$i]));
				else 
					continue;
				
				$profesionales->id = $profesionales->get("id");
				if ( !$profesionales->id ) $profesionales->insert();
			}
		}
}

Class ProfesionalCentro Extends RowManager{
	public static $table = "profesionales_centros";
	public static $pk = "id";
	public static $fields = array("id_profesional", "id_centro_sanitario");
}

Class TipoOrganizacion Extends RowManager{
	public static $table = "tipos_organizaciones";
	public static $pk = "id";
	public static $fields = array("nombre");

	public static function importar(){
		$file = "tmp/asserta-mapa.csv";

		$csv = new CSV($file);
		$csv->open();

		$row = $csv->fetch_row(); //nos saltamos la primera
		$row = $csv->fetch_row(); //nos saltamos la segunda

		while ( $row = $csv->fetch_row() ){
			if ( !$row[1] ) continue;

			if ( $row[2] )  $tipo_organizacion = new TipoOrganizacion(array("nombre"=>$row[2]));
			else $tipo_organizacion = new TipoOrganizacion(array("nombre"=>$row[1]));

			$tipo_organizacion->id = $tipo_organizacion->get("id");
			if ( !$tipo_organizacion->id ) $tipo_organizacion->insert();
		}
	}
}

Class Servicio Extends RowManager{
	public static $table = "servicios";
	public static $pk = "id";
	public static $fields = array("nombre");
}

Class Usuario Extends RowManager{
	public static $table = "usuarios";
	public static $pk = "id";
	public static $fields = array("email", "clave", "token");
}

class_alias("CentroSanitario", "centros_sanitarios");
class_alias("Idioma", "idiomas");
class_alias("IdiomaCentro", "idiomas_centros");
class_alias("Organizacion", "organizaciones");
class_alias("Pais", "paises");
class_alias("Poblacion", "poblaciones");
class_alias("PoblacionAtendida", "poblaciones_atendidas");
class_alias("Servicio", "servicios");
class_alias("Provincia", "provincias");
class_alias("Profesional", "profesionales");
class_alias("ProfesionalCentro", "profesionales_centros");
class_alias("TipoOrganizacion", "tipos_organizaciones");
class_alias("Usuario", "usuarios");



?>