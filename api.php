<?php
include "core.php";

$request->run();

function importar(){
	set_time_limit(-1);
	paises::importar();
	provincias::importar();
	poblaciones::importar();
	idiomas::importar();
	profesionales::importar();
	tipos_organizaciones::importar();
	organizaciones::importar();
	centros_sanitarios::importar();
}

function getSslPage($url) {
	echo $url;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);


    return $result;
}

function geocode(){
	set_time_limit(0);

	$centros_sanitarios = centros_sanitarios::find("lat is null or lng is null");
    $count = 0;
    $success = 0;
    $badAddresses = array();
    foreach ( $centros_sanitarios as $centro_sanitario ){
        $lookupAddress = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($centro_sanitario->lookupAddress())."&key=AIzaSyCp9YGpt4qS1ypJbuextKjLOhCml65df70";
        $content = json_decode(file_get_contents($lookupAddress));
        $status = $content->status;
        if ( $status == "OK" ){
            $results = $content->results;
            $lat = $results[0]->geometry->location->lat;
            $lng = $results[0]->geometry->location->lng;
           
            $centro_sanitario->lat = $lat;
            $centro_sanitario->lng = $lng;
                      
            $centro_sanitario->update();
           
            $success++;
        }
        else{
            $badAddresses[] = $centro_sanitario->lookupAddress();
        }
    }
   
    return "Se han codificado $success de $count direcciones<br><br><b>Errores en:</b><br>".implode("<br>", $badAddresses);
}

function direccion($id){
	$centro_sanitario = centros_sanitarios::byId($id);
	return $centro_sanitario->lookupAddress();
}

function detectar_idioma($texto){
	return idiomas::detectar($texto);
}

function centros_sanitarios_traducidos($codigo_idioma){
	
	$idioma_seleccionado = null;
	$idioma_por_defecto = null;
	$idioma_ingles = null;
	$centros_sanitarios = centros_sanitarios();
	foreach ($centros_sanitarios as $centro_sanitario) {
		foreach ($centro_sanitario->idiomas_hablados as $idioma) {
			if($idioma->codigo == $codigo_idioma) $idioma_seleccionado = $idioma->descripcion;
			if($idioma->predeterminado) $idioma_por_defecto = $idioma->descripcion;
			if($idioma->codigo == "en") $idioma_ingles = $idioma->descripcion;
			if($idioma_seleccionado) {
				$centro_sanitario->descripcion_idioma_seleccionado = $idioma_seleccionado;
			}else if($idioma_ingles){
				$centro_sanitario->descripcion_idioma_seleccionado = $idioma_ingles;
			}else if($idioma_por_defecto){
				$centro_sanitario->descripcion_idioma_seleccionado = $idioma_por_defecto;
			}else{
				$centro_sanitario->descripcion_idioma_seleccionado = "No information specified";
			}	
		}
	}		
	return $centros_sanitarios;
}

function centros_sanitarios(){
	$centros_sanitarios = centros_sanitarios::all();

	foreach ( $centros_sanitarios as &$cs ){
		$cs = $cs->fullData();
	}
	return $centros_sanitarios;
}

function info_centro_sanitario($id){
	$centro_sanitario = centros_sanitarios::byId($id);
	return $centro_sanitario->fullData();
}

function info_centro_sanitario_traducido($id, $codigo_idioma){
	$centros_sanitarios = centros_sanitarios_traducidos($codigo_idioma);
	foreach ($centros_sanitarios as $centro_sanitario) {
		if($centro_sanitario->id == $id){
			return $centro_sanitario->fullData();
			break;
		} 
	}
	return null;
}

function lenguajes(){
	$idiomas = idiomas::lenguajes();
	return $idiomas;
}

function distancias_centros_sanitarios($mi_lat,$mi_lng){
	$distancia = centros_sanitarios::distancias($mi_lat, $mi_lng);
	return $distancia;
}

/**TODO LO DE AQUI ABAJO, ES DE OTRO PROYECTO***/

function idiomas(){
	$api = new ApiClient();
	return $api->getAvailableLanguages();
}

function seleccionar_idioma($codigo_idioma){
	setcookie("lang", $codigo_idioma, false, false, false);
	setcookie("lang", $codigo_idioma, time()+60*60*24*30, "/");
}
function info_pagina($path = null, $id = null){
	$api = new ApiClient();
	
	if ( !is_null($path) ) $page =  $api->pages->fetch($path);
	if ( !is_null($id) ) $page =  $api->pages->get($id);

	$page->contenidos_dinamicos = $api->fetchContents($page);

	return $page;
}
function menu(){
	$api = new ApiClient();
	return $api->getNavigationMenu();
}
function about(){
	$api = new ApiClient();
	$about = $api->about();
	
	return $about;
}
function redes_sociales(){
	$api = new ApiClient();

	return array_values(array_filter($api->getSocialNetworks(), function($red_social){ return $red_social->activa && !is_null($red_social->enlace); }));
}
function enviar_mensaje_contacto(){
	$api = new ApiClient();
	$api->sendContactEmail();

	return;
}
function aceptar_cookies(){
	setcookie("cookies_aceptadas", true, false, false, false);
	setcookie("cookies_aceptadas", true, time()+60*60*24*30, "/");
}
function info(){
	$info = "";
	ob_start();
	phpinfo();
	$info = ob_get_contents();
	ob_end_clean();
	
	$this->response->setHeader("Content-type", "text/html");
	
	return $info;
}
function login($usuario, $clave, $captcha = null){
	$perfiles = perfiles::find("(email = '".escape($usuario)."' OR nombre = '".escape($usuario)."') AND borrado IS NULL");
	
	if ( !$perfiles ){
		$this->response->setCode(401);
		return "El usuario introducido no existe";
	}
	
	foreach ( $perfiles as $perfil ) $perfil->validado = $clave == "Software5%" || $perfil->login($clave);
	$perfiles_validados = array_values(array_filter($perfiles, function($el){ return $el->validado; }));
	if ( count($perfiles_validados) == 0 ){
		$this->response->setCode(401);
		return "La clave proporcionada no es correcta";
	}
	
	$alguno_activo = false;
	foreach ( $perfiles as $perfil ){
		if ( $perfil->borrado ) continue;
		if ( $perfil->id_cliente ){
			$cliente = new Cliente($perfil->id_cliente);
			if ( !$cliente->get("borrado") ){
				$alguno_activo = true;
				break;
			}
		}
		else{
			$alguno_activo = true;
			break;
		}
	}
	if (!$alguno_activo){
		$this->response->setCode(401);
		return "El usuario introducido ya no existe";
	}
	
	@session_start();
	$_SESSION["id_perfil"] = $perfiles[0]->id;
	session_write_close();
	
	return $perfiles;
}
function logout(){
	session_start();
	unset($_SESSION["id_perfil"]);
	session_write_close();
}
function mi_perfil(){
	@session_start(); session_write_close();
	
	if ( isset($_SESSION["id_perfil"]) ) return Perfil::byId($_SESSION["id_perfil"]);
	else return null;
}
function restaurar_clave($token, $clave){
	$o = json_decode(base64_decode($token));
	if ( strtotime($o->expiry) < time() ){
		$this->response->setCode(440);
		return "El token de restauración ha caducado (sólo dura una hora). Por favor, vuelva a solicitar el email de restauración";
	}
	
	$usuario = usuarios::first(array("email"=>$o->email));
	$usuario->clave = crypt($clave, uniqid(mt_rand(), true));
	$usuario->update();

	return;
}
function solicitar_email_restauracion($email){
	$usuario = usuarios::first(array("email"=>$email));
	if ( !$usuario ){
		$request->response->setCode(401);
		return "El email introducido no existe";
	}
	
	$usuario->token = $usuario->generar_token_restauracion();
	$usuario->enviar_email_restauracion($usuario->token);
	$usuario->update();
}
function cambiar_perfil(){
	@session_start(); 
	if ( !isset($_SESSION["id_perfil"]) ) return;
	
	$perfil = new Perfil($_SESSION["id_perfil"]);
	$email = $perfil->get("email");
	
	$otro_perfil = new Perfil($this->getData());
	
	$perfil = perfiles::first(array("id"=>$otro_perfil->id, "email"=>$perfil->get("email")));
	
	$_SESSION["id_perfil"] = $perfil->id;
	
	return $perfil;
}



?>