<?php
require "core.php";
/**
 * Simple PHP Git deploy script
 *
 * Automatically deploy the code using PHP and Git.
 *
 * @version 1.3.1
 * @link    https://github.com/markomarkovic/simple-php-git-deploy/
 */

// =========================================[ Configuration start ]===

/**
 * It's preferable to configure the script using `deploy-config.php` file.
 *
 * Rename `deploy-config.example.php` to `deploy-config.php` and edit the
 * configuration options there instead of here. That way, you won't have to edit
 * the configuration again if you download the new version of `deploy.php`.
 */
chdir(dirname(__FILE__));
//if (file_exists(basename(__FILE__, '.php').'-config.php')) require_once basename(__FILE__, '.php').'-config.php';
/**
 * Protect the script from unauthorized access by using a secret access token.
 * If it's not present in the access URL as a GET variable named `sat`
 * e.g. deploy.php?sat=Bett...s the script is not going to deploy.
 *
 * @var string
 */
//if (!defined('SECRET_ACCESS_TOKEN')) define('SECRET_ACCESS_TOKEN', 'BetterChangeMeNowOrSufferTheConsequences');
if ( !defined('SECRET_ACCESS_TOKEN') ) die("La clave es obligatoria");
if ( !defined('GIT_REMOTE_REPOSITORY') ) die("Repositorio no definido");
if ( !defined('GIT_BRANCH') ) die("Branch no definido");
if ( !defined('GIT_TARGET_DIR') ) die("Directorio no definido");

if ( !defined('GIT_DELETE_FILES') ) define ('GIT_DELETE_FILES', false);
if ( !defined('GIT_EXCLUDE') ) define ('GIT_EXCLUDE', serialize(array('.git',)));
if ( !defined('GIT_TMP_DIR') ) define ('GIT_TMP_DIR', '/tmp/spgd-'.md5(GIT_REMOTE_REPOSITORY).'/');
if ( !defined('GIT_CLEAN_UP') ) define ('GIT_CLEAN_UP', true);
if ( !defined('GIT_VERSION_FILE') ) define ('GIT_VERSION_FILE', GIT_TMP_DIR.'VERSION');
if ( !defined('TIME_LIMIT') ) define ('TIME_LIMIT', 30);
if ( !defined('GIT_BACKUP_DIR') ) define ('GIT_BACKUP_DIR', false);
if ( !defined('USE_COMPOSER') ) define ('USE_COMPOSER', false);
if ( !defined('COMPOSER_OPTIONS') ) define ('COMPOSER_OPTIONS', '--no-dev');
if ( !defined('COMPOSER_HOME') ) define ('COMPOSER_HOME', true);
if ( !defined('EMAIL_ON_ERROR') ) define ('EMAIL_ON_ERROR', false);

// If there's authorization error, set the correct HTTP header.
if (!isset($_GET['sat']) || $_GET['sat'] !== SECRET_ACCESS_TOKEN || SECRET_ACCESS_TOKEN === 'BetterChangeMeNowOrSufferTheConsequences') {
	header('HTTP/1.0 403 Forbidden');
}

ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<title>Simple PHP Git deploy script</title>
	<style>
body { padding: 0 1em; background: #222; color: #fff; }
h2, .error { color: #c33; }
.prompt { color: #6be234; }
.command { color: #729fcf; }
.output { color: #999; }
	</style>
</head>
<body>
<?php
if (!isset($_GET['sat']) || $_GET['sat'] !== SECRET_ACCESS_TOKEN) {
	die('<h2>ACCESS DENIED!</h2>');
}
if (SECRET_ACCESS_TOKEN === 'BetterChangeMeNowOrSufferTheConsequences') {
	die("<h2>You're suffering the consequences!<br>Change the SECRET_ACCESS_TOKEN from it's default value!</h2>");
}
?>
<pre>

Checking the environment ...

Running as <b><?php echo trim(shell_exec('whoami')); ?></b>.

<?php
// Check if the required programs are available
$requiredBinaries = array('git', 'rsync');
if (GIT_BACKUP_DIR !== false) {
	$requiredBinaries[] = 'tar';
	if (!is_dir(GIT_BACKUP_DIR) || !is_writable(GIT_BACKUP_DIR)) {
		die(sprintf('<div class="error">BACKUP_DIR `%s` does not exists or is not writeable.</div>', GIT_BACKUP_DIR));
	}
}
if (USE_COMPOSER === true) {
	$requiredBinaries[] = 'composer --no-ansi';
}
foreach ($requiredBinaries as $command) {
	$path = trim(shell_exec('which '.$command));
	if ($path == '') {
		die(sprintf('<div class="error"><b>%s</b> not available. It needs to be installed on the server for this script to work.</div>', $command));
	} else {
		$version = explode("\n", shell_exec($command.' --version'));
		printf('<b>%s</b> : %s'."\n"
			, $path
			, $version[0]
		);
	}
}
?>

Environment OK.

Deploying <?php echo GIT_REMOTE_REPOSITORY; ?> <?php echo GIT_BRANCH."\n"; ?>
to        <?php echo GIT_TARGET_DIR; ?> ...

<?php
// The commands
$commands = array();

// ========================================[ Pre-Deployment steps ]===

if (!is_dir(GIT_TMP_DIR)) {
	// Clone the repository into the TMP_DIR
	$commands[] = sprintf(
		'git clone --depth=1 --branch %s %s %s'
		, GIT_BRANCH
		, GIT_REMOTE_REPOSITORY
		, GIT_TMP_DIR
	);
} else {
	// TMP_DIR exists and hopefully already contains the correct remote origin
	// so we'll fetch the changes and reset the contents.
	$commands[] = sprintf(
		'git --git-dir="%s.git" --work-tree="%s" fetch origin %s'
		, GIT_TMP_DIR
		, GIT_TMP_DIR
		, GIT_BRANCH
	);
	$commands[] = sprintf(
		'git --git-dir="%s.git" --work-tree="%s" reset --hard FETCH_HEAD'
		, GIT_TMP_DIR
		, GIT_TMP_DIR
	);
}

// Update the submodules
$commands[] = sprintf(
	'git submodule update --init --recursive'
);

// Describe the deployed version
if (GIT_VERSION_FILE !== '') {
	$commands[] = sprintf(
		'git --git-dir="%s.git" --work-tree="%s" describe --always > %s'
		, GIT_TMP_DIR
		, GIT_TMP_DIR
		, GIT_VERSION_FILE
	);
}

// Backup the TARGET_DIR
// without the BACKUP_DIR for the case when it's inside the TARGET_DIR
if (GIT_BACKUP_DIR !== false) {
	$commands[] = sprintf(
		"tar --exclude='%s*' -czf %s/%s-%s-%s.tar.gz %s*"
		, GIT_BACKUP_DIR
		, GIT_BACKUP_DIR
		, basename(GIT_TARGET_DIR)
		, md5(GIT_ARGET_DIR)
		, date('YmdHis')
		, GIT_TARGET_DIR // We're backing up this directory into BACKUP_DIR
	);
}

// Invoke composer
if (USE_COMPOSER === true) {
	$commands[] = sprintf(
		'composer --no-ansi --no-interaction --no-progress --working-dir=%s install %s'
		, GIT_TMP_DIR
		, COMPOSER_OPTIONS
	);
	if (is_dir(COMPOSER_HOME)) {
		putenv('COMPOSER_HOME='.COMPOSER_HOME);
	}
}

// ==================================================[ Deployment ]===

// Compile exclude parameters
$exclude = '';
foreach (unserialize(GIT_EXCLUDE) as $exc) {
	$exclude .= ' --exclude='.$exc;
}
// Deployment command
$commands[] = sprintf(
	'rsync -rltgoDzvO %s %s %s %s'
	, GIT_TMP_DIR
	, GIT_TARGET_DIR
	, (GIT_DELETE_FILES) ? '--delete-after' : ''
	, $exclude
);

// =======================================[ Post-Deployment steps ]===

// Remove the TMP_DIR (depends on CLEAN_UP)
if (GIT_CLEAN_UP) {
	$commands['cleanup'] = sprintf(
		'rm -rf %s'
		, GIT_TMP_DIR
	);
}

// =======================================[ Run the command steps ]===
$output = '';
foreach ($commands as $command) {
	set_time_limit(TIME_LIMIT); // Reset the time limit for each command
	if (file_exists(GIT_TMP_DIR) && is_dir(GIT_TMP_DIR)) {
		chdir(GIT_TMP_DIR); // Ensure that we're in the right directory
	}
	$tmp = array();
	exec($command.' 2>&1', $tmp, $return_code); // Execute the command
	// Output the result
	printf('
<span class="prompt">$</span> <span class="command">%s</span>
<div class="output">%s</div>
'
		, htmlentities(trim($command))
		, htmlentities(trim(implode("\n", $tmp)))
	);
	$output .= ob_get_contents();
	ob_flush(); // Try to output everything as it happens

	// Error handling and cleanup
	if ($return_code !== 0) {
		printf('
<div class="error">
Error encountered!
Stopping the script to prevent possible data loss.
CHECK THE DATA IN YOUR TARGET DIR!
</div>
'
		);
		if (GIT_CLEAN_UP) {
			$tmp = shell_exec($commands['cleanup']);
			printf('


Cleaning up temporary files ...

<span class="prompt">$</span> <span class="command">%s</span>
<div class="output">%s</div>
'
				, htmlentities(trim($commands['cleanup']))
				, htmlentities(trim($tmp))
			);
		}
		$error = sprintf(
			'Deployment error on %s using %s!'
			, $_SERVER['HTTP_HOST']
			, __FILE__
		);
		error_log($error);
		if (EMAIL_ON_ERROR) {
			$output .= ob_get_contents();
			$headers = array();
			$headers[] = sprintf('From: Simple PHP Git deploy script <simple-php-git-deploy@%s>', $_SERVER['HTTP_HOST']);
			$headers[] = sprintf('X-Mailer: PHP/%s', phpversion());
			mail(EMAIL_ON_ERROR, $error, strip_tags(trim($output)), implode("\r\n", $headers));
		}
		break;
	}
}
?>

Done.
</pre>
</body>
</html>
