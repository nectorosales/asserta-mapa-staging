<?php 
	require "core.php";
?>

<!DOCTYPE html>
<html ng-app = 'app' ng-cloak>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,maximum-scale=1, user-scalable=yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="application-name" content="<?php echo $about->nombre;?>">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="#02917b">
		<meta name="apple-mobile-web-app-title" content="<?php echo $about->nombre;?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<base href="<?php echo BASE; ?>" />
		<meta name="robots" content="noindex, nofollow">	
		<title>Asserta - mapa</title>
		<meta name = 'description' content="Asserta - mapa">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!-- STYLE CSS -->
		<link rel="stylesheet" href="css/styles.css">

		<!--ANGULAR MATERIAL-->
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.css">

		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Parisienne" rel="stylesheet">
		<!--Material Icons-->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	   <!--ICONOS PA ABURRIR-->
		<link rel="apple-touch-icon" sizes="57x57" href="img/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="img/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="img/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="img/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="img/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#02917b">
		<meta name="msapplication-TileImage" content="img/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#02917b">
	</head>
	<body ng-controller = 'app as app'>
		<main ui-view></main>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

		<!-- Angular Material requires Angular.js Libraries -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-aria.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-messages.min.js"></script>

		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/1.0.3/angular-ui-router.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.6.9/angular-locale_es-es.js"></script>
	  
		<!-- Angular Material Library -->
		<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.js"></script>
		
		<script src = "//ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-cookies.js"></script>

		<!--GOOGLE MAPS-->
		<script src="//maps.googleapis.com/maps/api/js?sensor=false&libraries=drawing&key=AIzaSyCp9YGpt4qS1ypJbuextKjLOhCml65df70"></script>
		<script src = "https://cdn.jsdelivr.net/npm/ngmap@1.18.4/build/scripts/ng-map.min.js"></script>

		<!--APP FILES-->
		<script src="app/services.js"></script>
		<script src="app/controllers.js"></script>
		<script src="app/filters.js"></script>
		<script src="app/directives.js"></script>
		<script src="app/app.js" type = 'text/javascript'></script>
		
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</body>
</html>
























