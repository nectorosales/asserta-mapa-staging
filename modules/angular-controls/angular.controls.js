angular.module('ngControls', ['ngSearch']);
/**
Declaramos la ruta relativa del módulo para saber donde ir a buscar las plantillas
**/
(function(){ 'use strict' //PATH Service
	var scripts = document.getElementsByTagName("script");
	//angular.module('ngControls').value("PATH", "https://cdn.jsdelivr.net/npm/@alboradait/ng-controls@latest/");
	angular.module('ngControls').value("PATH", scripts[scripts.length-1].src.substring(0, scripts[scripts.length-1].src.lastIndexOf('/') + 1));
})();

/**
	Un servicio sencillo, que devuelve una plantilla dado su nombre, para ahorrar escribir un poquillo
**/
(function(){ 'use strict' //tplService
	angular.module('ngControls').factory('tplService', function(PATH){ 
		return {
			tplUrl:function(name){
				return PATH + name + ".html";
			}
		}
	})
})();

/**
PARSERS PARA LOS INPUTS NUMÉRICOS
**/
(function(){ 'use strict' //NUMBERS
	angular.module('ngControls').directive('type', function () {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function(scope, element, attrs, ctrl) {
				if (!ctrl) return;
				if ( attrs.type != "number" ) return;
				ctrl.$parsers.push(function(value) {
					return '' + value;
				});
				ctrl.$formatters.push(function(value) {
					return parseFloat(value);
				});
			}
		};
	})
})();

/**
Declaramos la proporción áurea, que utilizaremos en varias plantillas
**/
(function(){ 'use strict' //PHI
	angular.module('ngControls').value("PHI", 1.61803);
})();

/**
Servicio para realizar operaciones y validaciones con dnis/cifs/nifs/pasaportes
**/
(function(){ 'use strict' //NIF
	angular.module('ngControls').factory('NIF', function(){
		var letters = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
		var cifLetters = ["J", "A", "B", "C", "D", "E", "F", "G", "H", "I"];
		
		return {
			getType:function(nif){
				if ( nif.match(/^\d{8}[-\s]?[A-Za-z]$/i) ) return "dni";
				else if ( nif.match(/^[XYZ][-\s]?\d{7}[-\s]?[A-Za-z]$/i) ) return "pasaporte";
				else if ( nif.match(/^[KPQSW][-\s]?((\d{2})(\d{5}))([A-Z])$|^[ABEH][-\s]?((\d{2})(\d{5}))(\d)$|^[CDFGJLMNRUV][-\s]?((\d{2})(\d{5}))(\d)$/i) ) return "cif";
				else return undefined;
			},
			getLetterAndNumber:function(nif, tipo){
				if ( tipo == "pasaporte" ){
					var matches = nif.match(/^([A-Za-z])[-\s]?(\d{7})[-\s]?([A-Za-z])$/i);
					
					var digitoControl = matches[1]; 
					var numero = digitoControl + matches[2];
					numero = numero.replace("X", '0').replace("Y", '1').replace("Z", '2');
					numero = parseInt(numero);
					
					var letra = matches[3];
				}
				else if( tipo == "dni" ) {
					var matches = nif.match(/^(\d{8})[-\s]?([A-Za-z])$/i);
					var numero = parseInt(matches[1]);
					var letra = matches[2];
				}
				else if( tipo == "cif" ){
					var matches = nif.match(/^([A-Z])[-\s]?((\d{2})\d{5})([A-Z]|\d)$/i);
					
					var tipoSociedad = matches[1];
					var numero = matches[2];
					var digitoControl = matches[4].match(/^[A-Z]$/i)? matches[4]:cifLetters[parseInt(matches[4])];
					var codigoProvincia = matches[3];
					
					return [digitoControl, numero, tipoSociedad, codigoProvincia];
				}
				else return false;
				
				return [letra, numero];
			},
			validate:function(nif/*, tipo = [dni|pasaporte|cif]*/){
				var tipo;
				if ( arguments.length > 1 ){
					tipo = arguments[1];
					if ( tipo.toLowerCase() == "nif" ) tipo = this.getType(nif);
					else if ( tipo.toLowerCase() != this.getType(nif) ) return false;
				}
				else tipo = this.getType(nif);
				if ( !tipo ) return false;
				
				var arr = this.getLetterAndNumber(nif, tipo);
				if ( !arr || !arr[0] ) throw "No se han podido extraer los dígitos de control del "+tipo+" " + nif;
				var letra = arr[0];

				return letra.toLowerCase() == this.calcLetter(nif, tipo).toLowerCase();
			},
			calcLetter:function(nif, tipo){
				var arr = this.getLetterAndNumber(nif, tipo);
				
				if ( tipo == "cif" ){
					var sumaPares = 0;
					for ( var i = 1; i < arr[1].length; i+=2 ) sumaPares+=parseInt(arr[1].charAt(i));
					
					var sumaImpares = 0;
					for ( var i = 0; i < arr[1].length; i+=2 ){
						var str = (parseInt(arr[1].charAt(i)*2)).toString();
						var val = 0;
						for ( var j = 0; j < str.length; j++ ) val+=parseInt(str.charAt(j));
						sumaImpares+=val;
					}

					var sumaTotal = (sumaPares+sumaImpares).toString();
					var d = parseInt(sumaTotal.charAt(sumaTotal.length - 1));

					if ( d == 0 ) var i = 0;
					else i = 10 - d;
					
					return cifLetters[i];
				}
				else{
					var numero = arr[1];
					var divisor = 23;
					
					var i = numero % divisor;
					return letters[i];
				}	
			}
		}
	})
})();

/**
Manejo de errores y validaciones
**/
(function(){ 'use strict' //CUSTOM VALIDATION
	angular.module('ngControls').factory('Validator', function(PATH, ngDate){ 
		//Pillamos el idioma del navegador para utilizarlo en mensajes de error, etc...
		console.log(navigator.language);
		console.log(navigator.userLanguage);
		
		var lang;
		if ( navigator.language ) lang = navigator.language.substr(0,2);
		else lang = navigator.userLanguage.substr(0,2);
		//La gestión de errores, bastante self explained.
		var E_UNKNOWN = 0, E_REQUIRED = 1, E_NAN = 2, E_MIN = 3, E_MAX = 4, E_EMAIL = 5, E_MINLEN = 6, E_MAXLEN = 7, E_PATTERN = 8, E_BLACKLIST = 9, E_EQUAL = 10, E_INITIALDATE = 11, 
			E_NIF = 12, E_DNI = 13, E_CIF = 14, E_PASSPORT = 15, E_ENDDATE = 16, E_FILETYPE = 17, E_FILESIZE = 18;	
		var ERR_MESSAGES = [
			{'es':'Error desconocido', 'en':'Unknown Error'}, 
			{'es':'Este campo es obligatorio', 'en':'This field is mandatory'}, 
			{'es':'Este campo debe ser numérico', 'en':'This field should be numeric'}, 
			{'es':'El valor es inferior al mínimo', 'en':'Value is lower than the minimum'}, 
			{'es':'El valor es superior al máximo', 'en':'Value is higher than the maximum'}, 
			{'es':'El formato de email no es válido', 'en':'Email format incorrect'},
			{'es':'No cumple longitud mínima', 'en':'Minimum length not matched'}, 
			{'es':'No cumple longitud máxima', 'en':'Maximum length not matched'}, 
			{'es':'El formato no es válido', 'en':'Format incorrect'}, 
			{'es':'Este valor no está permitido', 'en':'This value is not allowed'}, 
			{'es':'Los campos no coinciden', 'en':'Fields doesn´t match'},
			{'es':'La fecha es anterior a la fecha inicial', 'en':'This date is previous to the initial date'},
			{'es':'El formato de NIF no es correcto', 'en':'ID Format is not correct'},
			{'es':'El formato de DNI no es correcto', 'en':'ID Format is not correct'},
			{'es':'El formato de CIF no es correcto', 'en':'ID Format is not correct'},
			{'es':'El formato de Pasaporte no es correcto', 'en':'Passport Format is not correct'},
			{'es':'La fecha es posterior a la fecha final', 'en':'This date is higher to the end date'},
			{'es':'Formato de archivo incorrecto', 'en':'File format incorrect'},
			{'es':'El tamaño del archivo es demasiado grande', 'en':'File size too big'},
		];
		var getErrMsg = function(err){
			if (err){
				switch (err){
					case "required": return ERR_MESSAGES[E_REQUIRED][lang]; break;
					case "number": return ERR_MESSAGES[E_NAN][lang]; break; 
					case "min": return ERR_MESSAGES[E_MIN][lang]; break;
					case "max": return ERR_MESSAGES[E_MAX][lang]; break;
					case "email": return ERR_MESSAGES[E_EMAIL][lang]; break;
					case "minlength": return ERR_MESSAGES[E_MINLEN][lang]; break;
					case "maxlength": return ERR_MESSAGES[E_MAXLEN][lang]; break;
					case "pattern": return ERR_MESSAGES[E_PATTERN][lang]; break;
					case "blacklist": return ERR_MESSAGES[E_BLACKLIST][lang]; break;
					case "equals": return ERR_MESSAGES[E_EQUAL][lang]; break;
					case "initial-date": return ERR_MESSAGES[E_INITIALDATE][lang]; break;
					case "end-date": return ERR_MESSAGES[E_ENDDATE][lang]; break;
					case "nif": return ERR_MESSAGES[E_NIF][lang]; break;
					case "dni": return ERR_MESSAGES[E_DNI][lang]; break;
					case "cif": return ERR_MESSAGES[E_CIF][lang]; break;
					case "pasaporte": return ERR_MESSAGES[E_PASSPORT][lang]; break;
					case "mime": return  ERR_MESSAGES[E_FILETYPE][lang]; break;
					case "filesize": return  ERR_MESSAGES[E_FILESIZE][lang]; break;
					default: return ERR_MESSAGES[E_UNKNOWN][lang]; break;
				}
			}
			else return undefined;
		} 
		
		var validateEmail = function(email){
			var regex = new RegExp(/^[A-Za-z0-9][A-Za-z0-9_\-\.]*[A-Za-z0-9_\-]@[A-Za-z0-9][A-Za-z0-9_\-\.]*\.[A-Za-z0-9]{2,}$/);
			return regex.test(email);
		}
		var validateMin = function(value, min, format){
			if ( typeof format == "undefined" ) format = "number";
			switch ( format ){
				case "date": case "datetime": case "time": return ngDate.strtodate(value) >= ngDate.strtodate(min); break;
				default: return parseFloat(value) >= parseFloat(min);
			}	
		}
		var validateMax = function(value, max, format){
			if ( typeof format === "undefined" ) format = "number";
			switch ( format ){
				case "date": case "datetime": case "time": return ngDate.strtodate(value) <= ngDate.strtodate(max); break;
				default: return parseFloat(value) <= parseFloat(max);
			}	
		}

		return {
			getErrMsg:getErrMsg,
			validateEmail:validateEmail,
			validateMin:validateMin,
			validateMax:validateMax,
		}
	});

	//Error cuando dos inputs no tienen el mismo valor
	angular.module('ngControls').directive("equals", function(){
		return{
			require: '?ngModel',
			restrict:"A",
			link:function(scope, element, attrs, ngModel){
				scope.$watch(attrs.ngModel, function() {
					validate();
				});
				// observe the other value and re-validate on change
				attrs.$observe('equals', function (val) {
					validate();
				});
				var validate = function() {
					var val1 = ngModel.$viewValue;
					var val2 = scope.$eval(attrs.equals);
					ngModel.$setValidity('equals', ! val1 || ! val2 || val1 === val2);
				}
			}
		}
	})
})();

/**
**/
(function(){ 'use strict' //FileService
	angular.module('ngControls').factory('FileService', function($q){ //Un servicio para manejar archivos
		return {
			buildBlobUrl:function(file){
				var deferred = $q.defer();
				
				var reader = new FileReader();
				reader.readAsArrayBuffer(file);
				reader.onload = function (evt) {
					var blob = new Blob([evt.target.result], {type: file.type});
					var blob_url = URL.createObjectURL(blob);
					deferred.resolve(blob_url);
				};
				reader.onerror = function (evt) {
					deferred.reject(err);
				};
				return deferred.promise;
			},
			createBlobFromContent:function(content, type){
				var blob = new Blob([content], {type: type});
				return URL.createObjectURL(blob);
			}
		}
	})
})();

/**
ac-input: Tipo customizado de input con aspecto inspirado en material design;
tiene una gestión personalizada de errores y añadimos varios tipos de inputs y validaciones personalizadas
**/
(function(){ 'use strict' //AC-INPUT
	angular.module('ngControls').directive('acInput', function($window, $compile, tplService, FileService, PATH, NIF, PHI, Validator, $injector, $timeout, $q, ngDate){
		return{
			restrict:"EC",
			templateUrl: function($element, $attrs){
				switch($attrs.type){
					case "date" : return tplService.tplUrl("ac-input--date"); break;
					case "file" : case "directory": return tplService.tplUrl("ac-input--file"); break;
					case "image" : return tplService.tplUrl("ac-input--image"); break;
					case "checkbox" : return tplService.tplUrl("ac-input--checkbox"); break;
					case "radio" : return tplService.tplUrl("ac-input--radio"); break;
					case "number" : return tplService.tplUrl("ac-input--number"); break;
					case "password" : return tplService.tplUrl("ac-input--password"); break;
					default : return tplService.tplUrl("ac-input");
				}
			},
			scope:true,
			transclude:true,
			require: '?ngModel',
			bindToController:{
				type:"@",
				min:"@",
				max:"@",
				value:"@", //Radio inputs
				ngValue:"=?", //Radio inputs
				size:"=?",
				ngModelValue:"=?ngModel",
				ngModelOptions:"=",
				placeholder:"@",
				format:"@", //d-m-Y para inputs tipo date, d-m-Y H:i para inputs datetime, etc...
				accept:"@", //accept = 'application/pdf', [type="file"] y [type="image"]
				maxSize:"@", //2M, 3Kb, etc...
				drag:"@", //Drag&Drop, para los input image, permite intercambiarlos
				drop:"@",
				trueValue:"@",
				falseValue:"@",
				checked:"=?",
				mask:"@",
				onBlur:"&",
				onFocus:"&",
				theme:"@",	/*flat, material, añadir más cuando esté de humor; por cuestiones de compatibilidad, se va a seleccionar por defecto material*/
				thousandsSeparator:"@",
				decimalSeparator:"@",
				precission:"=?",
			},
			controllerAs:"ac",
			controller:function($scope, $element, $attrs, $transclude){
				var ac = this;
				//Inicializador
				this.init = function(){
					var deferred = $q.defer();

					ac.precission = ac.precission ? ac.precission : 0;
					ac.size = ac.size ? ac.size : 15;
					//this.size = this.size ? this.size : parseFloat(window.getComputedStyle($element[0], null).getPropertyValue('font-size').toString().match(/^(\d+)([A-Za-z]+)$/)[1]);
					//this.color = this.color ? this.color : window.getComputedStyle($element[0], null).getPropertyValue('color').toString();
	
					this.theme = this.theme ? this.theme : "material";
					this.type = this.type ? this.type : "text";
					//if ( !this.ngModelValue ) this.ngModelValue = undefined;
					if ( this.type == "date" ) this.format = this.format ? this.format : "d-m-Y";
					if ( this.type == "datetime" ) this.format = this.format ? this.format : "d-m-Y H:i";
					if ( this.type == "time" ) this.format = this.format ? this.format : "H:i";
					if ( this.type == "checkbox" ) this.trueValue = this.trueValue ? this.trueValue : true;
					if ( this.type == "checkbox" ) this.falseValue = this.falseValue ? this.falseValue : false;
					if ( this.type == "file" ) this.link = this.ngModelValue;
					if ( this.type == "file" ) this.text = this.ngModelValue;
					if ( this.type == "file" ){
						this.multiple = "multiple" in $attrs;
						if (this.multiple) $element.find("input").attr('multiple', true);
					}
					if ( this.type == "directory" ){
						$element.find("input").attr('webkitdirectory', true);
						this.multiple = true;
						$element.find("input").attr('multiple', true);
					}
					if ( this.type == "number" ) this.decimalSeparator = this.decimalSeparator ? this.decimalSeparator : ".";
					if ( this.type == "text" && this.value ) this.ngModelValue = this.value;
					
					
					
					
					
					if ( this.theme == "material" ) this.materialize();
					else this.flatten();		

					deferred.resolve();
					
					return deferred.promise;
				}

				//Actualizar el valor
				this.render = function(){
					var deferred = $q.defer();
					var promise = deferred.promise;
					
					if ( ac.type == "checkbox"){
						if ( ac.isTrue(ac.ngModelValue) ){
							angular.element($element[0].querySelector(".ac-toggler")).addClass("active");
							$element.find("input")[0].checked = true;
						}
						else{
							angular.element($element[0].querySelector(".ac-toggler")).removeClass("active");
							$element.find("input")[0].checked = false;
						}
						
						deferred.resolve();
					}
					else if ( ac.type == "radio"){
						if ( ac.ngModelValue !== undefined && (ac.ngModelValue === ac.value || ac.ngModelValue === ac.ngValue) ){
							angular.element($element[0].querySelector(".ac-toggler")).addClass("active");
							$element.find("input")[0].checked = true;
						}
						else{
							angular.element($element[0].querySelector(".ac-toggler")).removeClass("active");
							$element.find("input")[0].checked = false;
						}
						
						deferred.resolve();
					}
					else if ( ac.type == "file" || ac.type == "directory"){
						if ( ac.ngModelValue instanceof File ){
							FileService.buildBlobUrl(ac.ngModelValue).then(function(blobUrl){
								ac.link = blobUrl;	
								ac.mimeType = ac.ngModelValue.type;
								ac.fileSize = ac.ngModelValue.size;
								deferred.resolve();
							})
						}
						else if ( ac.ngModelValue instanceof FileList ){
							var promises = [];
							ac.link = [];	
							ac.mimeType = [];
							ac.fileSize = [];
							for ( var i = 0; i < ac.ngModelValue.length; i++ ){
								(function(i){
									var p = FileService.buildBlobUrl(ac.ngModelValue[i]).then(function(blobUrl){
										ac.link[i] = blobUrl;	
										ac.mimeType[i] = ac.ngModelValue[i].type;
										ac.fileSize[i] = ac.ngModelValue[i].size;
										return;
									});
									promises.push(p);
								})(i);
							}
							$q.all(promises).then(function(){
								deferred.resolve();
							});
						}
						else{
							ac.link = ac.ngModelValue;
							ac.mimeType = undefined;
							ac.fileSize = undefined;
							$element.find("input").val(undefined);
							deferred.resolve();
						}
					}
					else if ( ac.type == "image" ){
						if ( ac.ngModelValue instanceof File ){
							ac.mimeType = ac.ngModelValue.type;
							ac.fileSize = ac.ngModelValue.size;
							if ( /^image/i.test(this.mimeType) ) FileService.buildBlobUrl(ac.ngModelValue).then(function(blobUrl){
								ac.link = blobUrl;	
								deferred.resolve();
							})
						}
						else if ( ac.ngModelValue ){
							ac.link = ac.ngModelValue;
							ac.mimeType = undefined;
							ac.fileSize = undefined;
							deferred.resolve();
						}
						else{
							ac.link = PATH + "noimg.png";
							deferred.resolve();
						}
					}
					else {
						deferred.resolve();
					}
					
					promise.then(function(){ ac.runCustomValidators(); });
					this.updateText();
				}
		
				//Formatear números
				this.formatNumber = function(number, decimals, dec_point, thousands_sep) {
					if ( decimals === null || decimals === undefined ) decimals = 2;
					
					var n = !isFinite(+number) ? 0 : +number, 
						prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
						sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
						dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
						toFixedFix = function (n, prec) {
							// Fix for IE parseFloat(0.55).toFixed(0) = 0;
							var k = Math.pow(10, prec);
							return Math.round(n * k) / k;
						},
						s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
					if (s[0].length > 3) {
						s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
					}
					if ((s[1] || '').length < prec) {
						s[1] = s[1] || '';
						s[1] += new Array(prec - s[1].length + 1).join('0');
					}
					return s.join(dec);
				}
		
				//Actualiza el texto
				this.updateText = function(){
					if ( !ac.$$UserInput ){
						if ( !this.ngModelValue ) this.text = "";
						else if (this.type == "datetime") this.text = ngDate.format(this.format, this.ngModelValue);
						else if ( this.type == "date" ){
							this.text = ngDate.format(this.format,  this.ngModelValue);
							$element.find("input").val(this.text);
						}
						else if ( this.type == "file" || this.type == "directory" ){
							if ( this.ngModelValue instanceof File ) ac.text = ac.ngModelValue.name;
							else if ( this.ngModelValue instanceof FileList ){
								if ( this.ngModelValue.length == 1 ) ac.text = ac.ngModelValue[0].name;
								else ac.text = this.ngModelValue.length + " archivos";
							}
							else ac.text = this.ngModelValue;
						}
						else if ( this.type == "number" ){
							var floatVal = parseFloat(this.ngModelValue);
							var input = $element.find("input");
							this.text = this.formatNumber(floatVal, ac.precission, ",", ".");
							
							var position = input[0].selectionStart; // Capture initial position
							var start = input[0].selectionStart,
							end = input[0].selectionEnd;
							input[0].value = ac.text;
							// restore from variables...
							input[0].setSelectionRange(start, end);	
						}
						else this.text = this.ngModelValue;	
						
						if ( this.type == "text" || this.type == "number" || this.type == "email" ) $element.find("input").val(this.text);
					}
					
					if ( this.text && this.text.toString().length > 0 ) angular.element($element[0].querySelector(".ac-placeholder")).addClass("active");
					else angular.element($element[0].querySelector(".ac-placeholder")).removeClass("active");
				}
			
				//Cambiar a theme material:
				this.materialize = function(){
					$element.attr("theme", "material");
					$element.css({fontSize:ac.size+"px"});

					if ( this.type == "checkbox" || this.type == "radio" ){		
						var togglerBounds = {
							height: this.size + "px",
							width:this.size + "px"
						}
						angular.element($element[0].querySelector(".ac-toggler")).css(togglerBounds);
					}
					else if ( this.type != "image" ){
						var inputBounds = {
							top: ac.size*PHI / 2 + "px",
							height: ac.size * 2 + "px",
							paddingRight:ac.size*PHI + "px",
							paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
							minHeight:"0px"
						}
						var wrapperBounds = {
							height: ac.size*2 + ac.size*PHI/2 + ac.size*PHI/2 + "px"
						};
						var placeholderBounds = {
							top: ac.size*PHI / 2 + "px",
							height: ac.size * 2 + "px",
							paddingRight:ac.size + "px",
							paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
							paddingTop:ac.size / (PHI*PHI) + "px",
						}
						var iconBounds = {
							top: ac.size + ac.size / (PHI*PHI) + "px",
							right: ac.size / (PHI*PHI) + "px",
							width: ac.size + "px",
							height: ac.size + "px"
						}
						var errorBounds = {
							top: ac.size*PHI / 2 + "px",
							paddingRight:ac.size*PHI + "px",
							paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						}
						
						if ( this.type == "file" || this.type == "directory" ){
							var buttonBounds = {
								top:ac.size / (PHI*PHI) + "px", 
								width:ac.size*13 + "px",
								padding:"0 " + ac.size + "px"
							};
							var textBounds = {
								top: ac.size*PHI / 2 + "px",
								height: ac.size * 2 + "px",
								paddingRight:ac.size*2 + "px",
								paddingLeft:ac.size*13 + ac.size  +"px",
								minHeight:"0px",
								lineHeight: ac.size*2 + "px"
							}
							angular.element($element[0].querySelector(".ac-button")).css(buttonBounds);
							angular.element($element[0].querySelector(".ac-text")).css(textBounds);
						}

						$element.find("input").attr("placeholder", null).css(inputBounds);
						if (this.type == 'date') angular.element($element[0].querySelector(".ac-text")).css(inputBounds);
						angular.element($element[0].querySelector(".ac-wrapper")).css(wrapperBounds);
						angular.element($element[0].querySelector(".ac-placeholder")).css(placeholderBounds);
						angular.element($element[0].querySelector(".ac-icon")).css(iconBounds);
						angular.element($element[0].querySelector(".ac-error")).css(errorBounds);
					}	
				}
			
				//Cambiar a flat theme
				this.flatten = function(){
					$element.attr("theme", "flat");	
					$element.css({fontSize:ac.size+"px"});
					//angular.element($element[0].querySelector(".ac-icon")).attr("style", null);
					angular.element($element[0].querySelector(".ac-wrapper")).css({height:""});
					
					if ( this.type == "checkbox" ){
						var inputBounds = {
							width: ac.size + "px",
							height: ac.size + "px",
						}
						$element.find("input").css(inputBounds);
					}
					else{
						$element.find("input").attr("placeholder", this.placeholder).attr("style", null).css({minHeight:"100%"});
						
						if ( this.type == "file" || this.type == "directory" ){
							var inputBounds = {
								paddingRight:ac.size*PHI + "px",
							}
							var iconBounds = {
								top: ac.size / (PHI*PHI) + "px",
								right: ac.size / (PHI*PHI) + "px",
								width: ac.size + "px",
								height: ac.size + "px"
							}
							angular.element($element[0].querySelector("input")).css(inputBounds);
							angular.element($element[0].querySelector(".ac-icon")).css(iconBounds);
						}
					}	
				}
			
				//Validadores personalizados
				this.runCustomValidators = function(){
					if ( this.type == "email" ){
						if ( this.ngModelValue ) this.ngModelController.$setValidity("email", Validator.validateEmail(this.ngModelValue));
						else this.ngModelController.$setValidity("email", true);
					}
					if ( this.type == "file" || this.type == "directory" ){
						if ( this.accept ){
							if ( this.mimeType === undefined ){
								this.ngModelController.$setValidity("mime", true);
							}
							else if ( this.multiple || this.type == "directory" ){
								var invalidMime = false;
								for ( var i = 0; i < this.mimeType.length; i++ ){
									var mimeType = this.mimeType[i];
									if ( mimeType != this.accept){
										invalidMime = true;
										break;
									}
								}
								if (invalidMime) this.ngModelController.$setValidity("mime", false);
								else this.ngModelController.$setValidity("mime", true);
							}
							else{
								this.ngModelController.$setValidity("mime", this.mimeType == this.accept);
							}
						}
						
						if ( this.maxSize ){
							var maxSize, matches = this.maxSize.match(/^(\d{1,})([K|M|G|T])?b?$/i);
							var measure = matches[2] ? matches[2].toLowerCase() : "b";
							
							switch( measure ){
								case "b" : maxSize = matches[1]; break;
								case "k" : maxSize = matches[1]*1024; break;
								case "m" : maxSize = matches[1]*1024*1024; break;
								case "g" : maxSize = matches[1]*1024*1024*1024; break;
								case "t" : maxSize = matches[1]*1024*1024*1024*1024; break;
								default: maxSize = matches[1];
							}
							
							if ( this.multiple || this.type == "directory" ){
								var fileSize = Math.max.apply(null, this.fileSize);
								if ( this.fileSize !== undefined ) this.ngModelController.$setValidity("filesize", fileSize <= maxSize);
								else this.ngModelController.$setValidity("filesize", true);
							}
							else{
								if ( this.fileSize !== undefined ) this.ngModelController.$setValidity("filesize", this.fileSize <= maxSize);
								else this.ngModelController.$setValidity("filesize", true);
							}
						}
					}
					if ( this.type == "image" ){
						if ( this.ngModelValue instanceof File ) this.ngModelController.$setValidity("mime", /^image/i.test(this.mimeType) );
						else this.ngModelController.$setValidity("mime", true);

						if ( this.maxSize ){
							var maxSize, matches = this.maxSize.match(/^(\d{1,})([K|M|G|T])?b?$/i);
							var measure = matches[2] ? matches[2].toLowerCase() : "b";
							
							switch( measure ){
								case "b" : maxSize = matches[1]; break;
								case "k" : maxSize = matches[1]*1024; break;
								case "m" : maxSize = matches[1]*1024*1024; break;
								case "g" : maxSize = matches[1]*1024*1024*1024; break;
								case "t" : maxSize = matches[1]*1024*1024*1024*1024; break;
								default: maxSize = matches[1];
							}
							if ( this.fileSize !== undefined ) this.ngModelController.$setValidity("filesize", this.fileSize <= maxSize);
							else this.ngModelController.$setValidity("filesize", true);
						}
					}
					if ( this.type == 'nif' || this.type == 'cif' || this.type == 'dni' || this.type == 'pasaporte'){
						if ( this.ngModelValue ) this.ngModelController.$setValidity(this.type, NIF.validate(this.ngModelValue, this.type));
						else this.ngModelController.$setValidity(this.type, true);
					}
					if ( this.min ){
						if ( this.ngModelValue ) this.ngModelController.$setValidity("min", Validator.validateMin(this.ngModelValue, this.min, this.type));
						else this.ngModelController.$setValidity("min", true);
					}
					if ( this.max ){
						if ( this.ngModelValue ) this.ngModelController.$setValidity("max", Validator.validateMax(this.ngModelValue, this.max, this.type));
						else this.ngModelController.$setValidity("max", true);
					}
				}
				
				//Enganchar el ngModelController a nuestro controlador para algunas validaciones
				this.subscribeModelController = function(ngModelController){
					this.ngModelController = ngModelController;
					
					//Modificamos la función isEmpty para que considere vacío un array de longitud 0
					if ( this.type == "checkbox" ) ngModelController.$isEmpty = function(value){ return value != ac.trueValue; }
					//No es necesario disparar manualmente la validación en principio, pero lo dejo comentado por si acaso; puede ser que con datos complejos no lo lea siempre.
					//$scope.$watchCollection("ac.ngModelValue", function(){ ngModelController.$validate(); });
					
					//También vamos a verificar los errores en el modelo para mostrarlos 
					$scope.$watchCollection("ac.ngModelController.$error", function($error){ 
						var err = null;
						for ( var i in $error ){
							if ( $error[i] ) { err = i; break; }
						}
						ac.err = Validator.getErrMsg(err);
					});
				}
	
				//Resetar
				this.clear = function(){
					this.ngModelController.$setTouched();
					this.ngModelValue = undefined;
					event.stopPropagation();
				};
			
				//Está función determina si, dado un input checkbox, su valor se ajusta a lo que hayamos definido como true
				this.isTrue = function(value){
					if ( this.trueValue === true ) return !(value === undefined || value === 0 || value === false || value === "0" || value === null || value === null || value === "");
					else return this.trueValue == this.ngModelValue;
				}
				
				//El toggle de los checkbox y el de los radios
				this.toggle = function(){
					if ( ac.type == "checkbox" ){
						if ( ac.isTrue(ac.ngModelValue) ) ac.ngModelValue = ac.falseValue;
						else ac.ngModelValue = ac.trueValue;
					}
					if ( ac.type == "radio" ){
						var value = ac.ngValue ? ac.ngValue : ac.value;
						if ( ac.ngModelValue == value ) ac.ngModelValue = undefined;
						else ac.ngModelValue = value;
					}
					
					this.render();
				}
			
				//El preview de los tipo file:
				this.preview = function(){ 
					if ( ac.link instanceof Array && ac.link.length > 1 ) {
						var html = "<ul>";
						for ( var i = 0; i< ac.link.length; i++ ){
							html+="<li><a href = '"+ac.link[i]+"'>"+ac.ngModelValue[i].name+"</a></li>";
						}
						html+="</ul>";
						var link = FileService.createBlobFromContent(html, "text/html");
						$window.open(link); 
					}
					else $window.open(ac.link); 
				}
			},
			link:function($scope, $element, $attrs, ctrl){
				var ac = $scope.ac;
				
				ac.init().then(function(){ 
					ac.subscribeModelController(ctrl);
					ac.render();
					$timeout(function(){
						$element.addClass("ac-ready");
						ac.ready = true; 
					});
				});

				var input = $element.find("input");
				if ( ac.type == "date" ){
					//datepicker is here
					//input.datetimepicker({dayOfWeekStart:1, lang:"es", timepicker:false, format:ac.format, scrollInput:false});
					//input.attr("readonly", true);
				}
				if ( ac.type == "datetime" ){	
					//input.datetimepicker({dayOfWeekStart:1, lang:"es", timepicker:true, format:ac.format, scrollInput:false});
					//input.attr("readonly", true);
				}
				if ( ac.type == "time" ){
					//input.attr("readonly", true);
				}

				var timer;
				if (ac.type == "text" || ac.type == "email" || ac.type == "number" || ac.type == "date" || ac.type == "datetime" || ac.type == "time" || ac.type == "nif" || ac.type == "cif" || ac.type == "dni" || ac.type == "password") input.on("keyup keypress change", function(event){
					if (ac.ngModelValue === input.val()) return;	
					
					var debounce = 0;
					if ( ac.ngModelOptions && ac.ngModelOptions.debounce) debounce = ac.ngModelOptions.debounce;
					ac.$$UserInput = true;
					
					if (ac.type == "datetime" || ac.type == "date" || ac.type == "time") ac.text = ngDate.format(ac.format, input.val());
					else ac.text = input.val();
					
					
					if ( ac.text && ac.text.length > 0 ) angular.element($element[0].querySelector(".ac-placeholder")).addClass("active");
					else angular.element($element[0].querySelector(".ac-placeholder")).removeClass("active");
					
					if ( timer ) $timeout.cancel(timer);
					
					timer = $timeout(function(){
						if (ac.type == "number") ac.ngModelValue = parseFloat(input.val());
						else ac.ngModelValue = input.val();
						
						ac.ngModelController.$setDirty();
						ac.ngModelController.$setViewValue(ac.ngModelValue);
						ac.render();
						$timeout(function(){ ac.$$UserInput = false; });
					}, debounce);
				})
				if (ac.type != "checkbox" && ac.type != "radio" ) input.on("blur", function(){
					ac.onBlur();
					if (ac.ngModelController) ac.ngModelController.$setTouched();
					$scope.$apply();
				});
				if (ac.type != "checkbox" && ac.type != "radio" ) input.on("focus", function(){
					ac.onFocus();
					$scope.$apply();
				})

				if ( ac.type == "checkbox" || ac.type == "radio" ){
					$element.on("click", function(){
						if ( !event.defaultPrevented ){
							ac.$$UserInput = true;
							ac.toggle();
							ac.ngModelController.$setViewValue(ac.ngModelValue);
							$timeout(function(){ ac.$$UserInput = false; });
						}
					});
				}
				if ( ac.type == "file" || ac.type == "image" || ac.type == "directory"){
					$element.on("click", function(event){ input[0].click(); return; 0});	
					
					input[0].addEventListener("change", function(event){
						if (ac.ngModelController){
							ac.ngModelController.$setDirty();
							ac.ngModelController.$setTouched();
						}
						
						var target = event.target;
						if ( target.files.length ) $scope.$apply(function(){
							if ( ac.multiple ){
								ac.ngModelValue = target.files; 
							}
							else{
								ac.ngModelValue = target.files[0]; 
							}
						});
						else $scope.$apply(function(){ ac.ngModelValue = undefined; });
					});
				}
				
				if ( ac.type == "image" ){
					window.acDraggedItem = undefined;
					if ( ac.drag !== undefined ){
						$element.addClass("ac-draggable");
						$element.attr("draggable", true);
						$element[0].addEventListener("dragstart", function(event){
							window.acDraggedItem = ac;
						});
					}
					if ( ac.drop !== undefined ){
						$element[0].addEventListener("dragover", function(event){
							event.preventDefault();
							$element.addClass("ac-outline");
							event.dataTransfer.dropEffect = 'link';
						});
						$element[0].addEventListener("dragleave", function(event){
							$element.removeClass("ac-outline");
						});
						$element[0].addEventListener("drop", function(event){
							$scope.$apply(function(){
								var value = window.acDraggedItem.ngModelValue instanceof File ? new File([window.acDraggedItem.ngModelValue], window.acDraggedItem.ngModelValue.name, { type:window.acDraggedItem.ngModelValue.type }) : window.acDraggedItem.ngModelValue;
								window.acDraggedItem.ngModelValue = ac.ngModelValue;
								ac.ngModelValue = value;
							});
							window.acDraggedItem = undefined;
							$element.removeClass("ac-outline");
						});
					}
				}

				//Watchers
				$scope.$watch("ac.ngModelValue", function(newVal, oldVal){
					if ( newVal === oldVal ) return;	
					if ( !ac.$$UserInput ){ 
						ac.render(); 
					}
				});
				$scope.$watch("ac.theme", function(newVal, oldVal){
					if ( newVal === oldVal ) return;
					
					if (ac.ready){
						if ( ac.theme == "flat" ) ac.flatten.call(ac);
						if ( ac.theme == "material" ) ac.materialize.call(ac);  
					}
				});
			
				//Observers
				$attrs.$observe("disabled", function(disabled){ 
					if ( disabled !== false ) $element.find("input").attr("disabled", true); 
					else  $element.find("input").attr("disabled", false); 
				})
			}
		}
	})

	//Precargamos las plantillas
	angular.module('ngControls').run(function($rootScope, $templateCache, $http, tplService) {
		$http.get(tplService.tplUrl("ac-input--file"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--image"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--checkbox"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--radio"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--number"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input"), {cache: $templateCache});
	})
})();

/**
**/
(function(){ 'use strict' //AC-RIPPLE
	angular.module('ngControls').directive("acRipple", function($timeout, $window, PHI){
		return {
			restrict:"EC",
			link:function($scope, $element, $attrs){
				if ( $attrs.rippleColor ) var rippleColor = $attrs.rippleColor;
				else var rippleColor = "rgba(0, 0, 0, 0.3)";
				/*if ( !$scope.size ) $scope.size = 15;

				$scope.style = {
					"font-size":$scope.size+"px",
					"padding":$scope.size /(PHI*PHI)+"px "+$scope.size+"px"
				}
				*/

				$element.on('click', function (event) {
					//event.preventDefault();
					
					var btnOffset = $element[0].getBoundingClientRect();
					var xPos = event.pageX - btnOffset.left;
					var yPos = event.pageY - btnOffset.top - $window.scrollY;

					var $div = angular.element('<div class = "ripple-effect"></div>');
					var $ripple = angular.element("<div class = 'ripple-effect'></div>");
					$div.css("height", btnOffset.height + "px");
					$div.css("width", btnOffset.height + "px");

					$element.append($div);

					$div.css({
						top: yPos - ($div[0].getBoundingClientRect().height/2) + "px",
						left: xPos - ($div[0].getBoundingClientRect().width/2) + "px",
						background: rippleColor
					});
					$timeout(function(){
						$div.remove();
					}, 1500);				
				});
			}
		}
	})
	angular.module('ngControls').directive("acRoundRipple", function($timeout, PHI){
		return {
			restrict:"EC",
			link:function($scope, $element, $attrs){
				if ( $attrs.rippleColor ) var rippleColor = $attrs.rippleColor;
				else var rippleColor = "rgba(0, 0, 0, 0.3)";

				$element.on('click', function (event) {
					//event.preventDefault();
					
					var btnOffset = $element[0].getBoundingClientRect();
					//var xPos = event.pageX - btnOffset.left;
					//var yPos = event.pageY - btnOffset.top;
					var xPos = btnOffset.width / 2;
					var yPos = btnOffset.height / 2;
					
					var $div = angular.element('<div class = "ripple-effect"></div>');
					var $ripple = angular.element("<div class = 'ripple-effect'></div>");
					$div.css("height", btnOffset.height + "px");
					$div.css("width", btnOffset.height + "px");

					$element.append($div);
					$div.css({
						top: yPos - ($div[0].getBoundingClientRect().height/2) + "px",
						left: xPos - ($div[0].getBoundingClientRect().width/2) + "px",
						background: rippleColor
					});
					$timeout(function(){
						$div.remove();
					}, 300);				
				});
			}
		}
	})
})();


(function(){ 'use strict' //AC-SELECT
	//Un select con opción de buscar, ordenar, limitar resultados, etc... y que además es to bonico
	angular.module('ngControls').directive("acSelect", function(tplService, $templateRequest, $compile, $document, $q, $interpolate, $sce, $parse, $timeout, ngSearch, PHI, PATH, Validator){
		var getBaseExpr = function(displayName, itemName){
			var regex = new RegExp(itemName+"\\.", "g");
			return displayName.replace(regex, "");
		}
		var OPT_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;

		return {
			restrict: 'EC',
			template: 	"<div class = 'ac-wrapper'>" +
							"<input type = 'text' ng-readonly = '(ac.type != \"search\" && ac.type != \"autocomplete\") || ac.disabled'>" +
							"<span class = 'ac-arrow'>&#9662;</span>" +
							"<span class = 'ac-placeholder' ng-show = 'ac.theme == \"material\"'>{{ac.placeholder}}</span>" +
							"<span class = 'ac-error' ng-show = 'ac.theme == \"material\" && ac.ngModelController.$touched'>{{ac.err}}</span>" +
						"</div>",
			transclude:true,
			scope:true,
			bindToController:{
				placeholder:"@",
				type:"@",
				options:"@",
				value:"=ngModel",
				limit:"=?",
				orderBy:"@",
				size:"=?",
				dropdownSize:"=?",
				theme:"@",
				template:"@",
				templateUrl:"@",
				ngChange:"&",
				searchIn:"@"
			},
			require: '?ngModel',
			controller:function($scope, $element, $attrs, $transclude){
				var ac = this;
				var matches = this.options.match(OPT_REGEXP);
				
	
				//El nombre de la colección.
				this.collectionName = matches[7];
				//Si pertenece al scope o es un array declarado in situ
				this.isBoundedToScope = this.collectionName.charAt(0) != "[";
	
				//En caso de no ser ligada al scope, procesamos inmediatamente la colleción y nos olvidamos
				var collection = this.isBoundedToScope ? [] : $parse(ac.collectionName)($scope.$parent) || [];

				//Construye un objeto local con el nombre correspondiente
				this.getLocals = function(item){ 
					var itemName = matches[4];
					var locals = {};
					locals[itemName] = item;
					return locals;
				}
				//Dado un item, devuelve su valor
				this.valueFn = function(item){
					var valueName = matches[1];
					var locals = ac.getLocals(item);
					return $parse(matches[1])($scope, locals);
				}
				//Dado un item, devuelve su texto
				this.textFn = function(item){
					var displayName = matches[2] || matches[1];
					var locals = ac.getLocals(item);
					return $parse(displayName)($scope, locals);
				}
				//Esta función va a devolver el template, que generalmente coincidirá con la función de display
				this.displayFn = function(item){
					var locals = ac.getLocals(item);
					if ( ac.template ) return $sce.trustAsHtml($interpolate(ac.template)(locals));
					else return $sce.trustAsHtml(ac.textFn(item));
				}
				//La función que devuelve la colección completa de items
				this.collectionFn = function(){
					if ( ac.isBoundedToScope ) return $parse(ac.collectionName)($scope.$parent) || [];
					else return collection;
				}
				//El track by, para cuando necesitamos matchear por un campo aunque tengamos un objeto
				this.trackByFn = function(item){
					var valueName = matches[8] || matches[1];
					var locals = ac.getLocals(item);
					return $parse(valueName)($scope, locals);
				}
				//Cambiar a theme material:
				this.materialize = function(){
					//$element;

					var inputBounds = {
						top: ac.size*PHI / 2 + "px",
						height: ac.size * 2 + "px",
						paddingRight:ac.size*PHI + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						minHeight:"0px"
					}
					var placeholderBounds = {
						top: ac.size*PHI / 2 + "px",
						height: ac.size * 2 + "px",
						paddingRight:ac.size + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						paddingTop:ac.size / (PHI*PHI) + "px",
					}
					var arrowBounds = {
						top: ac.size + ac.size / (PHI*PHI) + "px",
						right: ac.size / (PHI*PHI) + "px",
						width: ac.size + "px",
						height: ac.size + "px"
					}
					
					var errorBounds = {
						top: ac.size*PHI / 2 + "px",
						paddingRight:ac.size*PHI + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
					}
					
					$element.attr("theme", "material");
					$element.find("input").attr("placeholder", null).css(inputBounds);
					$element.css({fontSize:ac.size+"px"});
					
					angular.element($element[0].querySelector(".ac-wrapper")).css({height: ac.size*2 + ac.size*PHI/2 + ac.size*PHI/2 + "px"});
					angular.element($element[0].querySelector(".ac-placeholder")).css(placeholderBounds);
					//angular.element($element[0].querySelector(".ac-arrow")).css(arrowBounds);
					angular.element($element[0].querySelector(".ac-error")).css(errorBounds);
				}
				//Cambiar a flat theme
				this.flatten = function(){
					$element.attr("theme", "flat");
					$element.find("input").attr("placeholder", this.placeholder).attr("style", null).css({minHeight:"100%"});
					$element.css({fontSize:ac.size+"px"});
					//angular.element($element[0].querySelector(".ac-arrow")).attr("style", null);
					angular.element($element[0].querySelector(".ac-wrapper")).css({height:""});
				}
				//Enganchar el ngModelController a nuestro controlador para algunas validaciones, controlar cuando hemos tocado el desplegable, etc...
				this.subscribeModelController = function(ngModelController){
					ac.ngModelController = ngModelController;
	
					//También vamos a verificar los errores en el modelo para mostrarlos 
					$scope.$watchCollection("ac.ngModelController.$error", function($error){ 
						var err = null;
						for ( var i in $error ){
							if ( $error[i] ) { err = i; break; }
						}
						ac.err = Validator.getErrMsg(err);
					})
				}
				//Constructor de mapas
				this.render = function(){
					var results = ac.searcher.results;
					
					ac.results = results;
					ac.valueMap = results.map(ac.valueFn);
					ac.displayMap = results.map(ac.displayFn);
					ac.textMap = results.map(ac.textFn);
	
					if ( !ac.rendered ){
						ac.rendered = true;
						ac.handleValueChange();
					}
					
					//Si tras una modificación el valor no se encuentra en los resultados, saltamos a la gestión específica
					if ( ac.value && ac.valueMap.indexOf(ac.value) === -1 ) ac.handleValueChange();
					
					//Eventos
					for ( var i = 0; i < ac.eventos.nextRender.length; i++ ){
						ac.eventos.nextRender[i].call();
					}
					ac.eventos.nextRender = [];
					
					ac.searchInProgress = false;
				}
				//Actualiza el texto
				this.updateText = function(){
					$element.find("input").val(ac.text);
					if ( ac.text && ac.text.length > 0 ) angular.element($element[0].querySelector(".ac-placeholder")).addClass("active");
					else angular.element($element[0].querySelector(".ac-placeholder")).removeClass("active");
					
					if ( ac.type == "autocomplete" ) ac.value = ac.text;
				}	
				//Mover el scroll
				this.scrollToCursor = function(){
					if ( ac.cursorIndex < 1 ) return;
					var position = ac.cursorIndex - ac.firstIndex - 1;
					var y = ac.dropdown.find("li")[position].offsetTop;		
					ac.dropdown[0].scrollTop = y - ac.dropdown[0].getBoundingClientRect().height / 2;
				}
				//La función que crea el dropdown
				this.createDropdown = function(){
					var tpl = angular.element("<ul class = 'ac-select--dropdown'>"+
						"<li class = 'ac-select-dropdown-item ac-ripple' ng-repeat = 'item in ac.searcher.results' ng-click = 'ac.select($index, $event)' ng-class = '{highlight:ac.cursorIndex == $index}'>"+
							"<div ng-bind-html = 'ac.displayMap[$index]'></div>"+
						"</li>"+
					"</ul>");
					var transcludedNodes = Array.prototype.slice.call($transclude()).filter(function(el){
						return angular.element(el).text().trim().length > 0;
					});
					for ( var i = transcludedNodes.length; i > 0; i-- ){
						var index = i - transcludedNodes.length - 1;
						var clone = transcludedNodes[i - 1].cloneNode(true);
						var li = angular.element("<li class = 'ac-select-dropdown-item ac-ripple' ng-click = 'ac.select("+index+", $event)'  ng-class = '{highlight:ac.cursorIndex == "+index+"}' />").append(clone);
						//Si le ponemos ngClick al elemento creado, naturalmente, queremos que al pinchar sobre la lsita se ejecute, necesitamos cambiar el evento al nodo lista.
						/*
						var ngClick = clone.getAttribute("ng-click");
						if (ngClick){
							clone.removeAttribute("ng-click");
							li.attr("ng-click", ngClick);
						<li ng-repeat = 'item in ac.searcher.results' ng-click = 'ac.select($index, $event)' ng-class = '{highlight:ac.cursorIndex == $index}'>
						}
						
						li.attr("ng-click", "ac.select($index, $event)");
						li.attr('ng-class', '{highlight:ac.cursorIndex == '+index+'}');
						li.addClass('ac-ripple');
						li.addClass('ac-select-dropdown-item');		
						*/						
						tpl.prepend(li);	
					}
				
					ac.transcludedNodes = transcludedNodes;
					var firstIndex;
					for ( var i = 0; i < ac.transcludedNodes.length; i++ ){
						var nodeValue = ac.transcludedNodes[i].getAttribute("value");
						if ( !nodeValue ){
							firstIndex = -transcludedNodes.length + i;
						}
					}
					if ( !firstIndex ) firstIndex = parseInt(-transcludedNodes.length) - 1;
					ac.firstIndex = ac.cursorIndex = firstIndex;
					ac.dropdown = $compile(tpl)($scope);
					ac.dropdown.css({fontSize:ac.size});
				}
				//
				this.loadTemplate = function(){
					if ( ac.templateUrl ) return $templateRequest(ac.templateUrl).then(function(html){ ac.template = html; });
					else return $q.when("miau");
				}
				//
				this.handleValueChange = function(){
					if ( !ac.rendered ) return;

					
					var resolveValue = function(){
						$timeout(function(){
						
							if ( ac.type == "autocomplete"){
								ac.selectedIndex = ac.cursorIndex = ac.firstIndex;
								
								if (  !ac.value  ){
									ac.value = undefined;
									ac.selectedIndex = ac.cursorIndex = ac.firstIndex;
									ac.text = "";
									ac.updateText();
								}
								return;
							}
							if ( ac.value ){
								var pos = -1;
								for ( var i = 0; i < ac.valueMap.length; i++ ){
									if ( ac.valueMap[i] == ac.value ){
										pos = i;
										break;
									}
								}

								ac.selectedIndex = ac.cursorIndex = pos;
								if ( pos !== -1 ) ac.text = ac.textMap[pos];
								else {//Tenemos seleccionado un valor no contenido en la búsqueda
									//Buscamos su posición en la colección completa
									var $index = Math.min(-1, ac.firstIndex);
									for (var i = 0; i < ac.searcher.matches.length; i++ ){
										/*
										if ( angular.equals(ac.valueFn(ac.searcher.matches[i]), ac.value ) ){
											$index = i;
											break;
										}
										*/
										if ( ac.valueFn(ac.searcher.matches[i]) == ac.value ){
											$index = i;
											break;
										}
									}
									//Si existe, desplazamos el buscador hasta la página den la que se encuentra (esto ocurre en la primera carga del buscador con un valor preseleccionado)
									if ( $index != -1 ){
										ac.searcher.page = Math.ceil( ($index + 1) / ac.searcher.limit );
										ac.selectedIndex = ac.cursorIndex = $index % ac.searcher.limit;
										ac.text = ac.textFn(ac.searcher.matches[$index]);
									}
									//Si no existe, vaciamos el valor y el texto (esto ocurre cuando la colección es modificada y no contiene el valor que teníamos seleccionado)
									else{
										ac.value = undefined;
										ac.text = "";
										ac.selectedIndex = ac.cursorIndex = ac.firstIndex;
									}
								}
							}
							else{
								ac.value = undefined;
								ac.selectedIndex = ac.cursorIndex = ac.firstIndex;
								
								if ( ! ac.$$UserInput ) ac.text = ac.getTextAt(ac.firstIndex);
							}
							
							ac.updateText();
						}, 0);
					}
					
					
					if ( ac.searchInProgress ){
						//ac.searcher.on("resolve", resolveValue);
						//ac.searcher.unbind("resolve", resolveValue);
						ac.on("nextRender", resolveValue);
					}
					else resolveValue();
				}

				//Inicializador
				this.init = function(){
					var deferred = $q.defer();
					
					ac.size = ac.size ? ac.size : 15;
					//ac.size = ac.size ? ac.size : parseFloat(window.getComputedStyle($element[0], null).getPropertyValue('font-size').toString().match(/^(\d+)([A-Za-z]+)$/)[1]);

					ac.theme = ac.theme ? ac.theme : "material";
					ac.open = false;
					ac.limit = ac.limit ? ac.limit : ac.type == "search" ? 25 : ac.type == "autocomplete" ? 5 : Infinity;
					ac.firstIndex = ac.cursorIndex = 0;
					ac.searchIn =  ac.searchIn ? ac.searchIn : getBaseExpr(matches[2] || matches[1], matches[4]);
					ac.orderBy = ac.orderBy ? ac.orderBy : ac.searchIn;
					ac.$$UserInput = false;
					ac.rendered = false;
					ac.eventos = {
						firstRender:[],
						nextRender:[]
					};
					
					ac.loadTemplate().then(function(data){
						ac.createDropdown();
						
						//Temas de aspecto
						if ( ac.theme == "material" ) ac.materialize();
						else ac.flatten();	
						
						//Crear el buscador
						ac.searcher = ngSearch.get({
							limit:ac.limit,
							orderBy:ac.orderBy,
							src:ac.collectionFn,
							searchIn:ac.searchIn,
						});
						
						ac.searcher.on("resolve", ac.render);
						ac.searcher.on("search", function(){ ac.searchInProgress = true; });
	
						deferred.resolve();
					});

					return deferred.promise;
				}
				
				//Eventos
				this.on = function(evento, callback){
					ac.eventos[evento].push(callback);
				}
				
				
				//getters
				this.getValueAt = function($index){
					if ( $index >= 0 ) return ac.valueMap[$index];
					else if ( ac.type == "autocomplete" ) return ac.text;

					var pos = $index + ac.transcludedNodes.length;
					if ( pos === -1 ) return undefined;

					if ( ac.transcludedNodes[pos].hasAttribute("value") )  return $scope.$eval(ac.transcludedNodes[pos].getAttribute("value"));
					else return undefined;
					/*
					else if ( ac.transcludedNodes ){
						for ( var i = 0; i < ac.transcludedNodes.length; i++ ){
							if ( ac.transcludedNodes[i].hasAttribute("value") ){
								var value = $scope.$eval(ac.transcludedNodes[i].value);
								if ( value == ac.value ){
									ac.text = ac.transcludedNodes[i].textContent;
								}
							}
						}
					}
					*/
				}
				this.getTextAt = function($index){
					if (isNaN($index)) return "";
					
					if ( $index >= 0 ) return ac.textMap[$index];
					else if ( ac.type == "autocomplete" ) return ac.text;

					var pos = $index + ac.transcludedNodes.length;
					if ( pos === -1 ) return "";
					else{
						if ( ac.transcludedNodes[pos].hasAttribute("value") ){
							var value = $scope.$eval(ac.transcludedNodes[pos].getAttribute("value"));
							
							if ( value == ac.value ) return ac.transcludedNodes[pos].textContent;
							else return "";
						}
						else return ""; 
					}
				}
				
				//Acciones disponibles
				this.select = function($index, $event){
					var event = window.event || $event;
					
					var delay = event && event.type == "click" ? 100:0;
					$timeout(function(){
						ac.$$UserInput = true;
						ac.selectedIndex = ac.cursorIndex = $index;
						ac.value = ac.getValueAt($index);
						ac.text = ac.getTextAt($index);
						ac.hide();	
						$element.find("input")[0].focus();
						
						$timeout(function(){
							ac.ngModelController.$setDirty();
							ac.$$UserInput = false; 
							ac.ngChange();
						});
					}, delay);
					
					if ( event && event.type == "click" ){
						event.stopPropagation();
						var newEvent;
						newEvent = document.createEvent("HTMLEvents");
						newEvent.initEvent("click", true, true);
						$timeout(function(){ $element[0].dispatchEvent(newEvent); } );
					}
				}
				this.next = function(){
					ac.cursorIndex = Math.min(ac.cursorIndex+1, ac.searcher.results.length - 1);
					ac.scrollToCursor();
				}
				this.prev = function(){
					ac.cursorIndex = Math.max(ac.cursorIndex-1, ac.firstIndex);
					ac.scrollToCursor();
				}	
				this.show = function(){
					if ( ac.firstIndex == 0 && ac.searcher.results.length == 0 ) return;

					var rect;
					if (ac.theme == "material") rect = $element.find("input")[0].getBoundingClientRect();
					if (ac.theme == "flat") rect = $element[0].getBoundingClientRect();
					
					var espacioPorArriba = rect.top;
					var espacioPorAbajo = window.innerHeight - ( rect.top + rect.height );
					
					var width = ac.dropdownSize ? ac.dropdownSize: rect.width;
					
					if ( espacioPorAbajo*1.618 > espacioPorArriba ){
						var top = rect.top;
						if ( ac.type == "search" || ac.type == "autocomplete" ) top+= rect.height;
						var top0 = top - ac.size;
						var top1 = top;
						ac.dropdown.css({
							bottom:'',
							top:top0 + "px", //cuando el input se activa aplica una transformación vertical de 19px
							left:rect.left + "px",
							width:width + "px",
							opacity:0
						});
						$timeout(function(){ ac.dropdown.css({opacity:1, top:top1 + "px"}); });
					}
					else{
						var bottom = window.innerHeight - rect.bottom;
						
						if ( ac.type == "search" || ac.type == "autocomplete" ) bottom+= rect.height;
						var bottom0 = bottom - ac.size;
						var bottom1 = bottom;
						ac.dropdown.css({
							top:'',
							bottom:bottom0 + "px", //cuando el input se activa aplica una transformación vertical de 19px
							left:rect.left + "px",
							width:width + "px",
							opacity:0
						});
						$timeout(function(){ ac.dropdown.css({opacity:1, bottom:bottom1 + "px"}); });
					}
					
					$document.find("body").append(ac.dropdown);
					$element.find("input")[0].focus();
					
					var lis = ac.dropdown[0].querySelectorAll(".ac-select-dropdown-item > div");
					for ( var i = 0; i < lis.length; i++){
						var isOverflowing = lis[i].clientWidth < lis[i].scrollWidth;
						if ( isOverflowing ) lis[i].setAttribute("title", ac.textMap[i - ac.firstIndex]);
					}
					ac.scrollToCursor();
					ac.ngModelController.$setTouched();
					ac.open = true;
				}
				this.hide = function(){
					
					
					var rect;
					if (ac.theme == "material") rect = $element.find("input")[0].getBoundingClientRect();
					if (ac.theme == "flat") rect = $element[0].getBoundingClientRect();
					
					if (ac.dropdown[0].style.top){
						var top = rect.top;
						if ( ac.type == "search" || ac.type == "autocomplete" ) top+= rect.height;
						
						var top0 = top - ac.size;
						var top1 = top;
						ac.dropdown.css({opacity:0, top:top0 + "px"});
					}
					else{
						var bottom = window.innerHeight - rect.bottom;
						if ( ac.type == "search" || ac.type == "autocomplete" ) bottom+= rect.height;
						
						var bottom0 = bottom - ac.size;
						var bottom1 = bottom;
						ac.dropdown.css({opacity:0, bottom:bottom0 + "px"});
					}
					
					
					$timeout(function(){ ac.dropdown.detach(); }, 300);

					if ( !ac.value ){
						ac.selectedIndex = ac.firstIndex;
						ac.text = ac.getTextAt(ac.selectedIndex);
						ac.searcher.text = "";
					}
					ac.updateText();
					this.open = false;
				}
				this.reset = function(){
					ac.value = undefined;
					ac.text = "";
					ac.updateText();
					ac.hide();
				}
			},
			controllerAs:"ac",
			link:function($scope, $element, $attrs, ctrl, $transclude){
				var ac = $scope.ac;
				ac.init().then(function(){ ac.subscribeModelController(ctrl); });

				
				//DOM events
				var clickOut = function(event){
					if ( !ac.open ) return;
					
					var isChild = $element[0].contains(event.target) || ac.dropdown[0].contains(event.target);
					var isSelf = $element[0] == event.target || ac.dropdown[0] == event.target;

					if (!isChild && !isSelf) ac.hide();		
				}	
				$document.bind('click', clickOut);
				$element.bind("click", function(){
					if ( ac.disabled ) return;
					$element.find("input").select();
					if (!ac.open && ac.type != "autocomplete") ac.show();
				});
	
				//Vamos a mantener siempre referenciado el valor del input, para que entradas de teclado como flecha arriba, o flecha abajo, no disparen la búsqueda y consiguiente reseteo del valor.
				var $$inputValue = $element.find("input")[0].value;
				var $$searcherTimer = undefined;
				$element.find("input").bind("keydown", function(event){
					$$inputValue = event.target.value;
					switch( event.keyCode ){
						case 9: case 27: ac.hide(); break; //Tab - Escape
						case 38:{ //Arrow up
							$timeout(function(){
								if ( ac.open ) ac.prev(event);
								else ac.show();
							});
						}
						break;
						case 40: {//Arrow down
							$timeout(function(){
								if ( ac.open ) ac.next(event); 
								else ac.show();
							});
						}
						break;
						default: break;
					}
				});
				$element.find("input").bind("keyup", function(event){
					switch( event.keyCode ){
						case 9: { //Tab
							$timeout(function(){
								ac.show();
							});
						}
						break;
						case 13:{ //Enter
							$timeout(function(){
								if ( ac.open ) ac.select(ac.cursorIndex, event); 
								else ac.show();
							});
						}
						break;
						default:{
							ac.$$UserInput = true;
							
							if ( ac.type == "search" && event.target.value != $$inputValue ){
								ac.text = event.target.value;
								ac.updateText();
								$timeout.cancel($$searcherTimer);
								$$searcherTimer = $timeout(function(){
									ac.searcher.text = event.target.value;
								}, 300);
								ac.value = undefined;

								if ( !ac.open ) ac.show();

								$scope.$apply();
								ac.$$UserInput = false;
							}
							if ( ac.type == "autocomplete" && event.target.value != $$inputValue ){
								ac.text = event.target.value;
								ac.updateText();
								$timeout.cancel($$searcherTimer);
								$$searcherTimer = $timeout(function(){
									ac.searcher.text = event.target.value;
								}, 30);

								if ( !ac.open && ac.text.length >= 3 ) ac.on("nextRender", ac.show);

								$scope.$apply();
								ac.$$UserInput = false;
							}
						}
					}
				});

				$scope.$watchCollection("ac.value", ac.handleValueChange);
				$attrs.$observe("disabled", function(disabled){ ac.disabled = disabled !== false; });
				$scope.$watch("ac.placeholder", function(placeholder){ if ( ac.theme == "flat" ) $element.find("input").attr("placeholder", ac.placeholder); });
				
				//Limpieza
				$scope.$on("$destroy", function() {
					$document.unbind('click', clickOut);
					ac.dropdown.remove();
					ac.searcher.destroy();
				});
			
			
			}
		}
	});
})();

(function(){ 'use strict' //AC-SELECT-MULTIPLE
	//Un select con opción de buscar, ordenar, limitar resultados, etc... y que además es to bonico
	angular.module('ngControls').directive("acSelectMultiple", function(tplService, $templateRequest, $compile, $document, $q, $interpolate, $sce, $parse, $timeout, ngSearch, PHI, PATH, Validator){
		var getBaseExpr = function(displayName, itemName){
			var regex = new RegExp(itemName+"\\.", "g");
			return displayName.replace(regex, "");
		}
		var OPT_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;

		
		return {
			restrict:"EC",
			templateUrl: function(){ return  tplService.tplUrl("ac-select--multiple"); },
			transclude:true,
			/*replace:true,*/
			scope:true,
			require: '?ngModel',
			bindToController:{
				placeholder:"@",
				options:"@",
				value:"=ngModel",
				/*orderBy:"@",*/
				dropdownSize:"=?",
				template:"@",
				size:"=?",
				ngReguired:"=",
				theme:"@", /*flat, material, añadir más cuando esté de humor; por cuestiones de compatibilidad, se va a seleccionar por defecto material*/
			},
			controllerAs:"ac",
			controller:function($scope, $element, $attrs, $transclude){
				var ac = this;

				//Lo primero, extraemos las funciones necesarios del atributo options
				var matches = this.options.match(OPT_REGEXP);
				
				this.collectionName = matches[7];
				//this.orderBy = this.orderBy ? this.orderBy : getBaseExpr(matches[2] || matches[1], matches[4]);
				this.getLocals = function(item){ var o = {}; o[matches[4]] = item; return o; };

				//Dado un item, devuelve su valor
				this.valueFn = function(item){
					var valueName = matches[1];
					var locals = ac.getLocals(item);
					return $parse(matches[1])($scope, locals);
				}
				
				//Dado un item, devuelve su texto
				this.textFn = function(item){
					var displayName = matches[2] || matches[1];
					var locals = ac.getLocals(item);
					return $parse(displayName)($scope, locals);
				}
				
				//Esta función va a devolver el template, que generalmente coincidirá con la función de display
				this.displayFn = function(item){
					var locals = ac.getLocals(item);
					if ( ac.template ) return $sce.trustAsHtml($interpolate(ac.template)(locals));
					else return $sce.trustAsHtml(ac.textFn(item));
				}
	
				//La función que devuelve la colección completa de items
				this.collectionFn = function(){
					ac.collectionName = matches[7];
					return $parse(ac.collectionName)($scope.$parent) || [];
				}
				
				//El track by, para cuando necesitamos matchear por un campo aunque tengamos un objeto
				this.trackByFn = function(item){
					var valueName = matches[8] || matches[1];
					var locals = ac.getLocals(item);
					return $parse(valueName)($scope, locals);
				}
				
				//Dado un valor, nos dice en que posición se encuentra su registro asociado en la colección(por ejemplo, si buscamos por "1" y tenemos {id:1, nombre:"guau"}, si usamos track by id ese registro se considera coincidencia)
				this.findIndex = function(val){
					if ( val instanceof Object || val instanceof Array ){
						for ( var i = 0; i < this.trackByMap.length; i++ ) if ( angular.equals(this.trackByFn(val), this.trackByMap[i]) ) return i;
						return -1;
					}
					else return this.valueMap.indexOf(val);
				}

				//Inicializador
				this.init = function(){
					var deferred = $q.defer();

					ac.size = ac.size ? ac.size : 15;
					//this.size = this.size ? this.size : parseFloat(window.getComputedStyle($element[0], null).getPropertyValue('font-size').toString().match(/^(\d+)([A-Za-z]+)$/)[1]);
					this.theme = this.theme ? this.theme : "material";
					
					this.open = false;
					if ( !this.value ) this.value = [];
					
					this.render();
					if ( this.theme == "material" ) this.materialize();
					else this.flatten();		
					
					$templateRequest(tplService.tplUrl("ac-select-dropdown--multiple")).then(function(html){
						var tpl = angular.element(html);
						var transcludedNodes = Array.prototype.slice.call($transclude()).filter(function(el){
							return angular.element(el).text().trim().length > 0;
						});
						for ( var i = transcludedNodes.length; i > 0; i-- ){
							var index = i - transcludedNodes.length - 1;
							var clone = transcludedNodes[i - 1].cloneNode(true);
							var li = angular.element("<li />").append(clone);
							//Si le ponemos ngClick al elemento creado, naturalmente, queremos que al pinchar sobre la lsita se ejecute, necesitamos cambiar el evento al nodo lista.
							var ngClick = clone.getAttribute("ng-click");
							if (ngClick){
								clone.removeAttribute("ng-click");
								li.attr("ng-click", ngClick);
							}
							li.attr('ng-class', '{highlight:ac.cursorIndex == '+index+'}');
							li.addClass('ac-ripple');
							li.addClass('ac-select-dropdown-item');							
							tpl.prepend(li);	
						}
						ac.firstIndex = ac.cursorIndex = ac.selectedIndex = -transcludedNodes.length;
						ac.dropdown = $compile(tpl)($scope);
						ac.dropdown.css({fontSize:ac.size});
					})
					.then(function(){
						if ( ac.template && ac.template.indexOf("url:") == 0 ){
							$templateRequest(ac.template.substr(4)).then(function(html){ 
								ac.template = html; 
								//ac.render();
								deferred.resolve();
							});
						}
						else{
							//ac.render();
							deferred.resolve();
						}
					});
					
					return deferred.promise;
				}
	
				//Reconstruir
				this.render = function(){
					this.collection = this.collectionFn();
					this.valueMap = this.collection.map(this.valueFn);
					this.displayMap = this.collection.map(this.displayFn);
					this.textMap = this.collection.map(this.textFn);
					this.trackByMap = this.collection.map(this.trackByFn);
					this.updateSelection();
				}
				
				//Reconstruye el boolMap y el texto que se debe de mostrar.
				this.updateSelection = function(){
					this.boolMap = this.collection.map(function(){ return false; });	
					for ( var i = 0; i < this.value.length; i++ ){
						var $index = this.findIndex(this.value[i]);
						if ( $index !== -1 ) this.boolMap[$index] = true;
					}
				
					this.updateText();
				}
				
				//Actualiza el texto
				this.updateText = function(){
					this.text = this.textMap.filter(function(text, i){ return ac.boolMap[i]; }).join(", ");	
					if ( this.text.length > 0 ) angular.element($element[0].querySelector(".ac-placeholder")).addClass("active");
					else angular.element($element[0].querySelector(".ac-placeholder")).removeClass("active");
				}
			
				//Cambiar a theme material:
				this.materialize = function(){
					//$element;

					var inputBounds = {
						top: ac.size*PHI / 2 + "px",
						height: ac.size * 2 + "px",
						paddingRight:ac.size*PHI + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						minHeight:"0px"
					}
					var placeholderBounds = {
						top: ac.size*PHI / 2 + "px",
						height: ac.size * 2 + "px",
						paddingRight:ac.size + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						paddingTop:ac.size / (PHI*PHI) + "px",
					}
					var arrowBounds = {
						top: ac.size + ac.size / (PHI*PHI) + "px",
						right: ac.size / (PHI*PHI) + "px",
						width: ac.size + "px",
						height: ac.size + "px"
					}
					
					var errorBounds = {
						top: ac.size*PHI / 2 + "px",
						paddingRight:ac.size*PHI + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
					}
					
					$element.attr("theme", "material");
					$element.find("input").attr("placeholder", null).css(inputBounds);
					$element.css({fontSize:ac.size+"px"});
					
					angular.element($element[0].querySelector(".ac-wrapper")).css({height: ac.size*2 + ac.size*PHI/2 + ac.size*PHI/2 + "px"});
					angular.element($element[0].querySelector(".ac-placeholder")).css(placeholderBounds);
					angular.element($element[0].querySelector(".ac-arrow")).css(arrowBounds);
					angular.element($element[0].querySelector(".ac-error")).css(errorBounds);
				}
			
				//Cambiar a flat theme
				this.flatten = function(){
					$element.attr("theme", "flat");
					$element.find("input").attr("placeholder", this.placeholder).attr("style", null).css({minHeight:"100%"});
					angular.element($element[0].querySelector(".ac-arrow")).attr("style", null);
					angular.element($element[0].querySelector(".ac-wrapper")).css({height:""});
				}
			
				//Enganchar el ngModelController a nuestro controlador para algunas validaciones, controlar cuando hemos tocado el desplegable, etc...
				this.subscribeModelController = function(ngModelController){
					this.ngModelController = ngModelController;
					
					//Modificamos la función isEmpty para que considere vacío un array de longitud 0
					ngModelController.$isEmpty = function(value){ return value ? value.length == 0 : true; }
					
					//Es necesario que permitamos valores incorrectos porque de lo contrario, cuando quede vacío value se pondría automáticamente a undefined y las operaciones de array fallarían.
					if ( !ngModelController.$options ) ngModelController.$options = {}
					ngModelController.$options.allowInvalid = true;
					
					//Es necesario disparar manualmente la validación porque ngModel no está preparado para arrays
					$scope.$watchCollection("ac.value", function(){ ngModelController.$validate(); });
					
					//También vamos a verificar los errores en el modelo para mostrarlos 
					$scope.$watchCollection("ac.ngModelController.$error", function($error){ 
						var err = null;
						for ( var i in $error ){
							if ( $error[i] ) { err = i; break; }
						}
						ac.err = Validator.getErrMsg(err);
					})
				}
				
				//Acciones disponibles
				this.select = function($index){
					this.$$UserInput = true;
					
					var posInValueArray = -1;
					
					var val = this.valueMap[$index];
					if ( val instanceof Object || val instanceof Array ){
						for ( var i = 0; i < this.value.length; i++ ){
							if ( angular.equals(this.trackByFn(this.value[i]), this.trackByMap[$index]) ){
								posInValueArray = i;
								break;
							}
						}
					}
					else posInValueArray = this.value.indexOf(val);

					if ( posInValueArray != -1 ){
						this.value.splice(posInValueArray, 1);
						this.boolMap[$index] = false;	
					}
					else{
						this.value.splice($index, 0, this.valueMap[$index]);
						this.boolMap[$index] = true;	
					}
				
					this.updateText();
					
					$timeout(function(){ ac.$$UserInput = false; })
				}
				this.next = function(){
					this.cursorIndex = Math.min(this.cursorIndex+1, this.collection.length - 1);
				}
				this.prev = function(){
					this.cursorIndex = Math.max(this.cursorIndex-1, this.firstIndex);
				}	
				this.show = function(){
					var rect = $element[0].getBoundingClientRect();
					var top0 = rect.top + rect.height - ac.size;
					var top1 = rect.top + rect.height;
					var width = ac.dropdownSize ? ac.dropdownSize: rect.width;
					
					this.dropdown.css({
						top:top0 + "px", //cuando el input se activa aplica una transformación vertical de 19px
						left:rect.left + "px",
						width:width + "px"
					});
					this.dropdown.css({opacity:0});

					$document.find("body").append(this.dropdown);
					$element.find("input")[0].focus();
					
					
					$timeout(function(){ ac.dropdown.css({opacity:1, top:top1 + "px"}); });
					
					this.ngModelController.$setTouched();
					this.ngModelController.$setTouched();
					this.open = true;
				}
				this.hide = function(){
					var rect = $element[0].getBoundingClientRect();
					var top0 = rect.top + rect.height - ac.size;

					ac.dropdown.css({opacity:0, top:top0 + "px"});
					$timeout(function(){ ac.dropdown.detach(); }, 300);
					
					if ( !ac.value ) ac.text = "";
					this.open = false;
				}
				this.reset = function(){
					this.value = [];
					this.text = "";
					this.hide();
				}
			},
			link:function($scope, $element, $attrs, ctrl, $transclude){
				var ac = $scope.ac;
				ac.init().then(function(){ ac.subscribeModelController(ctrl); ac.ready = true; });

				//DOM events
				var clickOut = function(event){
					if ( !ac.open ) return;
					
					var isChild = $element[0].contains(event.target) || ac.dropdown[0].contains(event.target);
					var isSelf = $element[0] == event.target || ac.dropdown[0] == event.target;

					if (!isChild && !isSelf) ac.hide();		
				}	
				$document.bind('click', clickOut);
				$element.bind("click", function(){
					$element.find("input").select();
					if (!ac.open) ac.show();
				});
				$element.find("input").bind("keyup", function(event){
					switch( event.keyCode ){
						case 9: { //Tab
							ac.show();
							$scope.$apply();
						}
						break;
						case 13:{ //Enter
							ac.select(ac.cursorIndex, event); 
							$scope.$apply();
						}
						break;
						default: break;
					}
				});
				$element.find("input").bind("keydown", function(event){
					switch( event.keyCode ){
						case 9: ac.hide(); break; //Tab
						case 38:{
							ac.prev(); //Arrow up
							event.preventDefault();
							$scope.$apply();
						}
						break;
						case 40: {
							ac.next(); //Arrow down
							event.preventDefault();
							$scope.$apply();
						}
						break;
						default: break;
					}
				});

				//Añadimos hide al scope para poder llamarlo directamente en los elementos que metamos a mano
				$scope.hide = ac.hide;
				
				
				//Watchers
				$scope.$watchCollection("ac.collectionFn()", function(){ if (ac.ready) ac.render.call(ac); });
				$scope.$watchCollection("ac.value", function(){ if (ac.ready && !ac.$$UserInput ) ac.updateSelection(); });
				$scope.$watch("ac.theme", function(){ 
					if (ac.ready){
						if ( ac.theme == "flat" ) ac.flatten.call(ac);
						if ( ac.theme == "material" ) ac.materialize.call(ac);  
					}
				});

				//Destroy
				$scope.$on("$destroy", function() {
					$document.unbind('click', clickOut);
					ac.dropdown.remove();
				});
		
			
				return;
				
				
				$scope.$watch("ac.size", function(newVal){ applyStyles(); })

				//TODO: de momento, esto lo único que hace es deshabilitar si el atributo está presente; lo suyo sería hacerlo más dinámico en un futuro
				$attrs.$observe("disabled", function(newVal, oldVal){ if ( newVal !== false ) $element.addClass("disabled"); });
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							ac.err = Validator.getErrMsg(err);
						}
					}
				});
			
			}
		}
	});
	angular.module('ngControls').directive("acSelectDropdownItem", function(PHI){
		return {
			restrict:"C",
			link:function($scope, $element, $attrs){
				var ac = $scope.ac;
				$element.css({padding:ac.size + "px"});
			}
		}
	});
	
	
	//Cargar las plantillas en la caché
	angular.module('ngControls').run(function($rootScope, $templateCache, $http, tplService) {
		var url = tplService.tplUrl("ac-select--multiple");
		$http.get(url, {cache: $templateCache});
	})
})();

(function(){ 'use strict' //AC-DATEPICKER
	angular.module('ngControls').directive("acDatepicker", function(tplService, $templateRequest, $compile, $document, $q, $timeout, $filter, PHI, PATH, Validator, ngDate){
		return {
			restrict:"EAC",
			templateUrl: function(){ return  tplService.tplUrl("ac-input--date"); },
			scope:true,
			require: '?ngModel',
			bindToController:{
				placeholder:"@",
				value:"=ngModel",
				size:"=?",
				ngReguired:"=",
				theme:"@", /*flat, material, añadir más cuando esté de humor; por cuestiones de compatibilidad, se va a seleccionar por defecto material*/
				showIcon:"=?",
				format:"@",
				hideOnSelect:"=?"
			},
			controllerAs:"ac",
			controller:function($scope, $element, $attrs, $transclude){
				var ac = this;
				this.format = this.format ? this.format: "dd MMM yyyy";

				//Inicializador
				this.init = function(){
					var deferred = $q.defer();

					this.today = ngDate.today();
					this.size = angular.isDefined(this.size) ? this.size : parseFloat(window.getComputedStyle($element[0], null).getPropertyValue('font-size').toString().match(/^(\d+)([A-Za-z]+)$/)[1]);
					this.theme = angular.isDefined(this.theme) ? this.theme : "material";
					this.hideOnSelect = angular.isDefined(this.hideOnSelect) ? this.hideOnSelect : true;

					this.open = false;
					
					this.render();
					if ( this.theme == "material" ) this.materialize();
					else this.flatten();

					$templateRequest(tplService.tplUrl("ac-input--date-dropdown")).then(function(html){
						var tpl = angular.element(html);
						ac.dropdown = $compile(tpl)($scope);
						var wrapper = angular.element(ac.dropdown[0].querySelector(".ac-wrapper"));
						wrapper.on("scroll", function(e){
							if ( e.target.scrollTop < 700 && !ac.rendering ){
								ac.rendering = true;
								ac.render(-10);
								$timeout(function(){ ac.rendering = false; }, 30);							
							}
							if ( e.target.scrollHeight - e.target.scrollTop - e.target.offsetHeight < 700 && !ac.rendering ){
								ac.rendering = true;
								ac.render(+10);
								$timeout(function(){ ac.rendering = false; }, 30);
							}
						});
					})
					.then(function(){
						deferred.resolve();
					});

					return deferred.promise;
				}
	
				//Reconstruir
				this.render = function(offset){
					if ( !offset ) offset = 0;
					
					var monthInCenter;
					if ( offset == 0 ){
						this.months = [];
						var currentDate  = this.value ? this.value : this.today;
						monthInCenter = ngDate.format("1-m-Y", currentDate);
					}
					if ( offset < 0 ) monthInCenter = ngDate.format("1-m-Y", ngDate.strtodate(ngDate.format("1-m-Y", new Date(this.months[0].timestamp)) + " +5 months"));
					if ( offset > 0 ) monthInCenter = ngDate.format("1-m-Y", ngDate.strtodate(ngDate.format("1-m-Y", new Date(this.months[this.months.length - 1].timestamp)) + " -5 months"));

					for ( var i = -5 + offset; i <= 5 + offset; i++ ){
						var d;
						
						if ( i < 0 ) d = ngDate.strtodate(monthInCenter+" "+i+" months");
						else d = ngDate.strtodate(monthInCenter+" +"+i+" months");
						var monthData = {
							name:$filter("date")(d, "MMMM yyyy"),
							focus: i == 0,
							timestamp:d.getTime(),
							date:d
						}
						
						var weeks = [];
					
						var w0 = parseInt(ngDate.date("w", ngDate.firstDayOfMonth(d)));
						var w1 = parseInt(ngDate.date("w", ngDate.lastDayOfMonth(d)));
					
						//Si corresponde a la última semana del año anterior, actuamos en consecuencia
						if ( w0 > w1 ) w0 = 0;

						for ( var k = w0; k <= w1; k++ ){
							var firstDayOfWeek = ngDate.strtodate(d.getFullYear()+"W"+k);
							var week = { weekNumber:k, weekdays:[] }
								
							for ( var j = 0; j < 7; j++ ){
								var weekday = {};
								weekday.date = ngDate.strtodate(ngDate.format("d-m-Y", firstDayOfWeek) + " +" + j + " days"),
								weekday.day = weekday.date.getDate();
								
								weekday.aria = $filter("date")(d, "dd MMMM yyyy");
								weekday.timestamp = weekday.date.getTime();
								weekday.isToday = ngDate.format("d-m-Y") == ngDate.format("d-m-Y", weekday.date);
								weekday.notInMonth = ngDate.format("m", monthData.date) != ngDate.format("m", weekday.date);
								
								
								week.weekdays.push(weekday);
							}
							
							weeks.push(week);
						}
						
						monthData.weeks = weeks;
						
						if ( offset >= 0 ) this.months.push(monthData);
						else this.months.splice(i + 5 - offset, 0, monthData);
					}
				}

				//Reconstruye el boolMap y el texto que se debe de mostrar.
				this.updateSelection = function(){
					this.boolMap = this.collection.map(function(){ return false; });	
					
					for ( var i = 0; i < this.value.length; i++ ){
						var $index = this.findIndex(this.value[i]);
						if ( $index !== -1 ) this.boolMap[$index] = true;
					}
					
					this.updateText();
				}
				
				//Actualiza el texto
				this.updateText = function(){
					this.text =  $filter("date")(this.value,this.format);
					if ( this.text.length > 0 ) angular.element($element[0].querySelector(".ac-placeholder")).addClass("active");
					else angular.element($element[0].querySelector(".ac-placeholder")).removeClass("active");
				}
			
				//Cambiar a theme material:
				this.materialize = function(){
					//$element;
					
					var inputBounds = {
						top: ac.size*PHI / 2 + "px",
						height: ac.size * 2 + "px",
						paddingRight:ac.size*PHI + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						minHeight:"0px"
					}
					var placeholderBounds = {
						top: ac.size*PHI / 2 + "px",
						height: ac.size * 2 + "px",
						paddingRight:ac.size + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
						paddingTop:ac.size / (PHI*PHI) + "px",
					}
					var iconBounds = {
						top: ac.size + ac.size / (PHI*PHI) + "px",
						right: ac.size / (PHI*PHI) + "px",
						width: ac.size + "px",
						height: ac.size + "px"
					}
					var errorBounds = {
						top: ac.size*PHI / 2 + "px",
						paddingRight:ac.size*PHI + "px",
						paddingLeft:ac.size / (PHI*PHI*PHI) + "px",
					}
					
					$element.attr("theme", "material");
					$element.find("input").attr("placeholder", null).css(inputBounds);
					
					angular.element($element[0].querySelector(".ac-wrapper")).css({height: ac.size*2 + ac.size*PHI/2 + ac.size*PHI/2 + "px"});
					angular.element($element[0].querySelector(".ac-placeholder")).css(placeholderBounds);
					angular.element($element[0].querySelector(".ac-icon")).css(iconBounds);
					angular.element($element[0].querySelector(".ac-error")).css(errorBounds);
				}
			
				//Cambiar a flat theme
				this.flatten = function(){
					$element.attr("theme", "flat");
					$element.find("input").attr("placeholder", this.placeholder).attr("style", null).css({minHeight:"100%"});
					angular.element($element[0].querySelector(".ac-arrow")).attr("style", null);
					angular.element($element[0].querySelector(".ac-wrapper")).css({height:""});
				}
			
				//Enganchar el ngModelController a nuestro controlador para algunas validaciones, controlar cuando hemos tocado el desplegable, etc...
				this.subscribeModelController = function(ngModelController){
					this.ngModelController = ngModelController;
					
					//Modificamos la función isEmpty para que considere vacío un array de longitud 0
					ngModelController.$isEmpty = function(value){ return value ? value.length == 0 : true; }
					
					//Es necesario que permitamos valores incorrectos porque de lo contrario, cuando quede vacío value se pondría automáticamente a undefined y las operaciones de array fallarían.
					if ( !ngModelController.$options ) ngModelController.$options = {}
					ngModelController.$options.allowInvalid = true;
					
					//Es necesario disparar manualmente la validación porque ngModel no está preparado para arrays
					$scope.$watchCollection("ac.value", function(){ ngModelController.$validate(); });
					
					//También vamos a verificar los errores en el modelo para mostrarlos 
					$scope.$watchCollection("ac.ngModelController.$error", function($error){ 
						var err = null;
						for ( var i in $error ){
							if ( $error[i] ) { err = i; break; }
						}
						ac.err = Validator.getErrMsg(err);
					})
				}
				
				//
				this.getCurrentMonthSheet = function(){
					var position = 0;
					var thisMonth = this.value ? ngDate.format("1-m-Y", this.value) : ngDate.format("1-m-Y", this.today);
					var timestamp = ngDate.strtodate(thisMonth).getTime();
					
					var monthSheet = ac.dropdown[0].querySelector("tbody[timestamp='"+timestamp+"']");
					return monthSheet;
				}
				
				//Acciones disponibles
				this.select = function(value){
					this.$$UserInput = true;
					this.value = value;
					this.timestamp = ngDate.strtodate(ngDate.format("d-m-Y", this.value)).getTime();
					this.updateText();
					$timeout(function(){ ac.$$UserInput = false; });
					if ( this.hideOnSelect ) this.hide();
				}
				this.next = function(){
					this.cursorIndex = Math.min(this.cursorIndex+1, this.collection.length - 1);
				}
				this.prev = function(){
					this.cursorIndex = Math.max(this.cursorIndex-1, this.firstIndex);
				}	
				this.show = function(){
					var rect = $element[0].getBoundingClientRect();
					var top0 = rect.top + rect.height - ac.size;
					var top1 = rect.top + rect.height;
					var width = ac.dropdownSize ? ac.dropdownSize: rect.width;
					
					this.dropdown.css({
						top:top0 + "px", //cuando el input se activa aplica una transformación vertical de 19px
						left:rect.left + "px",
						opacity:0
					});

					$document.find("body").append(this.dropdown);
					$element.find("input")[0].focus();
					
					
					$timeout(function(){
						var monthSheet = ac.getCurrentMonthSheet();
						var offsetTop = monthSheet.offsetTop;
						var height = monthSheet.getBoundingClientRect().height;

						
						ac.dropdown.css({opacity:1, top:top1 + "px", height:height + 50 + "px"});	
						ac.dropdown[0].querySelector(".ac-wrapper").scrollTop = offsetTop;
					});
					
					this.ngModelController.$setTouched();
					this.open = true;
				}
				this.hide = function(){
					var rect = $element[0].getBoundingClientRect();
					var top0 = rect.top + rect.height - ac.size;

					ac.dropdown.css({opacity:0, top:top0 + "px"});
					$timeout(function(){ 
						ac.dropdown.detach(); 
						ac.render();
					}, 500);
					
					if ( !ac.value ) ac.text = "";
				}
				this.reset = function(){
					this.value = undefined;
					this.text = "";
					this.hide();
				}
			},
			link:function($scope, $element, $attrs, ctrl, $transclude){
				var ac = $scope.ac;
				ac.init().then(function(){ ac.subscribeModelController(ctrl); ac.ready = true; });

				//DOM events
				var clickOut = function(event){
					if ( !ac.open ) return;
					
					var isChild = $element[0].contains(event.target) || ac.dropdown[0].contains(event.target);
					var isSelf = $element[0] == event.target || ac.dropdown[0] == event.target;

					if (!isChild && !isSelf) ac.hide();		
				}	
				$document.bind('click', clickOut);
				$element.bind("click", function(){
					$element.find("input").select();
					ac.show();
				});	
				$element.find("input").bind("keyup", function(event){
					switch( event.keyCode ){
						case 9: { //Tab
							ac.show();
							$scope.$apply();
						}
						break;
						case 13:{ //Enter
							ac.select(ac.cursorIndex, event); 
							$scope.$apply();
						}
						break;
						default: break;
					}
				});
				$element.find("input").bind("keydown", function(event){
					switch( event.keyCode ){
						case 9: ac.hide(); break; //Tab
						case 38:{
							ac.prev(); //Arrow up
							event.preventDefault();
							$scope.$apply();
						}
						break;
						case 40: {
							ac.next(); //Arrow down
							event.preventDefault();
							$scope.$apply();
						}
						break;
						default: break;
					}
				});

				//Añadimos hide al scope para poder llamarlo directamente en los elementos que metamos a mano
				$scope.hide = ac.hide;
				
				
				//Watchers
				$scope.$watchCollection("ac.collectionFn()", function(){ if (ac.ready) ac.render.call(ac); });
				$scope.$watchCollection("ac.value", function(){ if (ac.ready && !ac.$$UserInput ) ac.updateSelection(); });
				$scope.$watch("ac.theme", function(){ 
					if (ac.ready){
						if ( ac.theme == "flat" ) ac.flatten.call(ac);
						if ( ac.theme == "material" ) ac.materialize.call(ac);  
					}
				});

				//Destroy
				$scope.$on("$destroy", function() {
					$document.unbind('click', clickOut);
					ac.dropdown.remove();
				});
		
			
				return;
				
				
				$scope.$watch("ac.size", function(newVal){ applyStyles(); })

				//TODO: de momento, esto lo único que hace es deshabilitar si el atributo está presente; lo suyo sería hacerlo más dinámico en un futuro
				$attrs.$observe("disabled", function(newVal, oldVal){ if ( newVal !== false ) $element.addClass("disabled"); });
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							ac.err = Validator.getErrMsg(err);
						}
					}
				});
			
			}
		}
	});
	
	//Cargar las plantillas en la caché
	angular.module('ngControls').run(function($rootScope, $templateCache, $http, tplService) {
		var url = tplService.tplUrl("ac-input--date");
		$http.get(url, {cache: $templateCache});
	})
})();


/**
**/
(function(){ 'use strict' //AC-MAGNIFIER
	angular.module('ngControls').directive("ngMagnifier", function($document, $timeout, $q){
		return {
			restrict:"EAC",
			link:function($scope, $element, $attrs){
				var rect, src, native_width, native_height, glass, shape, position, top, left, width, height, marginLeft, marginTop, zoom;	
				var options = $attrs.ngMagnifierOptions ? $scope.$eval($attrs.ngMagnifierOptions) : {};

				var createMagnifyingGlass = function(){
					shape = options.shape || "circle";
					width = options.width || $element.width();
					height = options.height || shape == "circle" ? width : $element.height();
					position = options.position || "top right";
					zoom = options.zoom || "1";
					marginLeft = options.marginLeft || 0;
					
					var rect = $element[0].getBoundingClientRect();	
					var posArr = position.split(" ");
					if ( posArr.indexOf("top") !== -1 ) top = rect.top;
					if ( posArr.indexOf("right") !== -1 ) left = rect.right;
					if ( posArr.indexOf("bottom") !== -1 ) top = rect.top + rect.height;
					if ( posArr.indexOf("left") !== -1 ) left = rect.left - width;
					
					//Esto está mal, tenemos que calcular el tamaño de background con el tamaño real de la imagen.
					//var zoomVar = zoom*100 + "%";

					glass = angular.element("<div></div>");
					glass.css({
						top:top,
						left:left,
						background:"url("+$element[0].src+") no-repeat",
						backgroundPosition:"50% 50%",
						width:width + "px",
						height:height + "px",
						position:"absolute",
						boxShadow: "0 0 0 7px rgba(255, 255, 255, 0.85), 0 0 7px 7px rgba(0, 0, 0, 0.25),  inset 0 0 40px 2px rgba(0, 0, 0, 0.25)",
						display:"none",
						zIndex:1,
						marginLeft:marginLeft + "px"
					});
					if ( shape == "circle" ) glass.css({borderRadius:"100%"});

					$document.find("body").append(glass);
					$element.mouseenter(function(){ glass.show(); });
					$element.mouseleave(function(){ glass.hide(); });
					$element.mousemove(function(e){
						var rect = $element[0].getBoundingClientRect();	
						var offsetTop = event.clientY - rect.top;
						var offsetLeft = event.clientX - rect.left;
						glass.css({/*backgroundSize:zoomVar, */backgroundPosition: offsetLeft*100 / rect.width + "%" + " " +offsetTop * 100 / rect.height + "%"});
					});
				}
				
				$attrs.$observe("src", function(newVal, oldVal){
					src = $element[0].src;
					var img = new Image();
					img.src = src;
					img.onload = function(){
						native_width = img.width;
						native_height = img.height;
						createMagnifyingGlass();
					}	
				});
			}
		}
	})
})();

/**
**/
(function(){ 'use strict' //AC-POPOVER
	angular.module('ngControls').directive("acTooltip", function($document, $timeout, $q){
		return {
			restrict:"EAC",
			link:function($scope, $element, $attrs){
				var parent = $element.parent();
				$element.detach();

				parent.mouseenter(function(){
					var rect = parent[0].getBoundingClientRect();
					
					$element.css({ opacity:0 });
					$document.find("body").append($element);

					if ( $element.hasClass("ac-tooltip-top") ){
						var offset = ($element.outerWidth() - rect.width ) / 2;
						$element.css({left: rect.left - offset + "px"});
						$element.css({top:rect.top - $element.outerHeight() + window.scrollY + "px"});
					}
					else if ( $element.hasClass("ac-tooltip-right") ){
						var offset = ($element.outerHeight() - rect.height ) / 2;
						$element.css({top:rect.top - offset + window.scrollY + "px"});
						$element.css({left: rect.right + "px"});
					}
					else if ( $element.hasClass("ac-tooltip-bottom")  ){
						var offset = ($element.outerWidth() - rect.width ) / 2;
						$element.css({left: rect.left - offset + "px"});
						$element.css({top:rect.bottom + window.scrollY + "px"});
					}
					else if ( $element.hasClass("ac-tooltip-left") || 1 ){
						var offset = ($element.outerHeight() - rect.height ) / 2;
						$element.css({top:rect.top - offset + window.scrollY + "px"});
						$element.css({left: rect.left - $element.outerWidth() + "px"});
					}

					$timeout(function(){
						$element.css({opacity:1});
					}, 10);
				});
				
				parent.mouseleave(function(){
					$element.css({ opacity:0 });
					$timeout(function(){ $element.detach(); }, 300);
				});
			}
		}
	})
})();


/**
**/
(function(){ 'use strict' //AC-DROPDOWN
	//Material dropdown
	angular.module('ngControls').directive("acDropdown", function($document, $compile, $timeout, PHI){
		var linkFn = function($scope, $element, $attrs, ctrl, $transclude){	
			var init = function(){
			
				
				$timeout(function(){
					for ( var i = 0; i < ctrl.items.length; i++ ){
						ctrl.dropdown[0].appendChild(ctrl.items[i][0]);
					}
					//dropdown = $compile(dropdown)($scope);
					var size = 15;
					if ("size" in $attrs) size = $attrs.size;
					//El gestor de click en el documento (el que cierra el desplegble)
					var clickHandler = function(event){
						var isChild = $element[0].contains(event.target);
						var isSelf = $element[0] == event.target;

						if (!isChild && !isSelf) {
							ctrl.dropdown.removeClass("active");
							ctrl.dropdown.css("transform", "translateY(-"+size+"px)");	
							/*
							$timeout(function(){
								dropdown.hide();
							}, 300);
							*/
						}	
					}
	
					$document.bind('click', clickHandler);
					$element.on("click", function(){
						var rect = $element[0].getBoundingClientRect();
						ctrl.dropdown.css("transform", "translateY(-"+size+"px)");	
						$document.find("body").append(ctrl.dropdown);
						
						
						var width = rect.width; //Min width
						var totalHeight = 0;
						for ( var i = 0; i < ctrl.items.length; i++ ){
							var w = ctrl.items[i][0].getBoundingClientRect().width;
							if ( w > width ) width = w;
							totalHeight+=ctrl.items[i][0].getBoundingClientRect().height;
						}
						
						var pos;
						if ( $attrs.position ) pos = $attrs.position;
						else{
							if ( window.innerWidth - rect.right > width ) pos = "left";
							else pos = "right";
						}

						
						var top = rect.top;
						var height = rect.height;
						var left;
						if ( pos == "left" ) left = rect.left;	
						else  left = rect.left - width + rect.width;
						
						if ( top + height + totalHeight  > window.innerHeight ) top = top - totalHeight - height;

						ctrl.dropdown.css({
							left:left + "px",
							top:top + height + "px",
							width:width + "px",
							transform: "translateY(0px)"
						});
						ctrl.dropdown.addClass("active");
						angular.forEach(ctrl.items, function(el){
							el.css("width", "100%");
						});
					});

					$scope.$on("$destroy", function(){
						ctrl.dropdown.remove();
						$document.unbind('click', clickHandler);
					});	
					
					for ( var i = 0; i < ctrl.items.length; i++ ){
						ctrl.items[i].removeClass("ng-cloak");
					}
				},0);
			}
			
			init();
		}
		
		return {
			restrict:"C",
			replace:true,
			transclude:true,
			controller:function(){
				this.dropdown = angular.element(
					"<div class = 'ac-dropdown-list'>"+
					"</div>"
				);
			
				this.items = [];
				this.append = function(item){
					this.items.push(item);
					this.dropdown[0].appendChild(item[0]);
				}
				this.remove = function(item){
					var i = this.items.indexOf(item);
					this.items.splice(i, 1);
				}
			},
			template:"<div><ng-transclude></ng-transclude></div>",
			link:linkFn
		}
	})

	angular.module('ngControls').directive("acDropdownItem", function($document, $compile, $timeout, PHI){
		var linkFn = function($scope, $element, $attrs, ctrl, $transclude){	
			var self = this;
		
			ctrl.append($element);

			$scope.$on("$destroy", function(){
				ctrl.remove($element);
			});
		}
		
		return {
			require:"^acDropdown",
			restrict:"C",
			link:linkFn
		}
	})
})();

/**
**/
(function(){ 'use strict' //AC-TABSET
	angular.module('ngControls').directive('acTabset', function($timeout) { //Tabs
		return {
			restrict: 'AC',
			transclude: true,
			scope: { 
				resize:"="
			},
			template: '  <ul class = "tablist">'+
				'	<li ng-repeat="tab in tabsetCtrl.tabs track by $index" ng-class="{\'active\': tab.active}">'+
				'	  <a class = "ac-ripple" ng-click="tabsetCtrl.select($index)" ng-style = "{fontSize:tab.size+\'px\'}">'+
				'		<span style = "font-size: 23px; color: #ff5252; position: absolute; left:13px" ng-show = "tab.$invalid">!</span>' +
				'		{{overflowing ? tab.alt : tab.label}}'+
				'	  </a>'+
				'	</li>'+
				'  </ul>'+
				'	<div class = "bar" ng-style="barStyle"></div>'+
				'   <div class = "wrapper" ng-transclude ng-style="wrapperStyle">'+
				'  </div>',
			bindToController: true,
			controllerAs: 'tabsetCtrl',
			controller: function($scope) {
				var self = this
				self.tabs = [];
				self.addTab = function addTab(tab) {
					self.tabs.push(tab)
					if(self.tabs.length === 1) {
						tab.active = true;
					}
				}
				self.removeTab = function removeTab(tab){
					self.tabs = self.tabs.filter(function(el){
						return el != tab;
					});
				}
				self.select = function(selectedTabIndex) {
					angular.forEach(self.tabs, function(tab, i) {
						if(tab.active && i !== selectedTabIndex) {
							tab.active = false;
						}
					});
					self.tabs[selectedTabIndex].active = true;
					$scope.selectedTabIndex = selectedTabIndex;
				}
			},
			link:function($scope, $element, attrs, ctrl){
				var watchers = [];
				$timeout(function(){
					$scope.barColor = attrs.barColor ? attrs.barColor : "#ff5252";	
					$scope.tabs = ctrl.tabs;
					$scope.selectedTabIndex = 0;
					$scope.barStyle = {};
					$scope.wrapperStyle = {};
					$scope.overflowing = false;
					
					var lis = $element.find("li");
					var r = lis[lis.length-1].getBoundingClientRect().right;
					if ( r > $element[0].getBoundingClientRect().right ){
						$scope.overflowing = true;
					}
					
					$scope.$watch("selectedTabIndex", function(newVal, oldVal){
						var li = $element.find("li")[newVal];
						var rect = li.getBoundingClientRect();
						
						var orig = $element.find("li")[0].getBoundingClientRect().left;
						$scope.barStyle = { 
							background:$scope.barColor,
							left:(rect.left - orig)+"px", 
							width:rect.right - rect.left+"px"
						};
						
						

						if (ctrl.resize){
							var setAuto = false;
							if (!("height" in $scope.wrapperStyle)) setAuto = true;

							if (!setAuto){
								var height = $element[0].querySelector(".wrapper").getBoundingClientRect().height;
								$scope.wrapperStyle = {height:height+"px"};
								$timeout(function(){
									var newHeight = $element[0].querySelectorAll(".wrapper .ac-tab-item")[newVal].getBoundingClientRect().height;
									$scope.wrapperStyle = {height:newHeight+"px"};
									$timeout(function(){
										$scope.wrapperStyle = {height:"auto"};
									}, 300);
								}, 0);
							}
							else{
								$scope.wrapperStyle = {height:"auto"};
							}
						}
					});
				},0);
			}
		}
	});
	angular.module('ngControls').directive('acTabItem', function($timeout) { //Tab item
		function acTabItemController($scope, $element){
			var controls = [];
			
			var self = this;
			
			self.$addControl = function(control){
				controls.push(control);
				control.$$parentTab = self;	
				
				if (control.$name) {
					self[control.$name] = control;
					control.$$parentTab = self;	
				}
			}
			self.$setValidity = function(validationErrorKey, state){
				$scope.$invalid = false;

				if ( state === false ) $scope.$invalid = true;
				else{ $scope.$invalid = false; 
					/*
					for ( var i = 0; i < controls.length; i++ ){
						if ( controls[i].$invalid ){
							$scope.$invalid = true;
							break;
						}
					}
					*/
				}	
				$scope.$valid = !$scope.$invalid;
			}
			self.$removeControl = function(control){
				controls.splice(controls.indexOf(control), 1);
				if (control.$name) {
					delete self[control.$name];
					control.$$parentTab = undefined;	
				}
				
				if ( $scope.$invalid ){
					$scope.$invalid = false;
				
					for ( var i = 0; i < controls.length; i++ ){
						if ( controls[i].$invalid ){
							$scope.$invalid = true;
							break;
						}
					}
					
					$scope.$valid = !$scope.$invalid;
				}
			}
		}
	
		return {
			restrict: 'AC',
			transclude: true,
			replace:true,
			template: '<div ng-show="active" ng-transclude ng-style = "style"></div>',
			require: '^acTabset',
			scope: { 
				label: '@',
				size: '=?',
				alt: '@',
			},
			controller: acTabItemController,
			controllerAs:"acTabItemController",
			link: function(scope, elem, attr, tabsetCtrl, $transclude) {
				scope.size = scope.size ? scope.size : 15;
				scope.active = false;
				tabsetCtrl.addTab(scope);
				scope.style = {};

				scope.$watch("active", function(newVal, oldVal){
					if ( newVal === oldVal ) return;
					
					if ( newVal ){
						$timeout(function(){
							scope.style = {overflow:"visible"};
						}, 0);
					}
					else{
						scope.style = {};
					}	
				});
				
				scope.$on("$destroy", function(){
					tabsetCtrl.removeTab(scope);
				});
			}	
		}
	})
	//Añadir los controles a los tab
	.config(function($provide) {
		$provide.decorator('ngModelDirective', function($delegate) {
			var directive = $delegate[0];
			var compile = directive.compile;

			directive.compile = function(){
				var cmArgs = arguments;
				var o = compile.apply(this, cmArgs);
				var postLink = o.post;
				
				o.post = function(scope, element, attr, ctrls){
					postLink.call(this, scope, element, attr, ctrls);			
					if ( ctrls[3] ){
						ctrls[3].$addControl(ctrls[0]);
						var setValidity = ctrls[0].$setValidity;
					
						
						ctrls[0].$setValidity = function(){
							var svArgs = arguments;
							setValidity.apply(this, svArgs);
							ctrls[0].$$parentTab.$setValidity(svArgs[0], svArgs[1], ctrls[0]);
						}
						element.on('$destroy', function() {
							ctrls[3].$removeControl(ctrls[0]);
						});
					}
				}		
				return o;
			}

			directive.require.push("^?acTabItem");
			return $delegate;
		});
	})
})();


/**
**/
(function(){ 'use strict' //AC-ANIMATE
	angular.module('ngControls').directive('acAnimate', function($window, $interval, $timeout) { //Tabs
		function isGoingToBeVisible(el) {

			if (el.length) {
				el = el[0];
			}
			var rect     = el.getBoundingClientRect(),
				vWidth   = window.innerWidth || doc.documentElement.clientWidth,
				vHeight  = window.innerHeight || doc.documentElement.clientHeight,
				efp      = function (x, y) { return document.elementFromPoint(x, y) };     
			var topLine = window.innerHeight + document.body.scrollTop;
			return rect.top < topLine;
		}
	
		function findKeyframesRule(rule) {
			var ss = document.styleSheets;
			
			for (var i = 0; i < ss.length; ++i) {
				if ( !ss[i].cssRules ) continue;
				
				for (var j = 0; j < ss[i].cssRules.length; ++j) {
					if ( (ss[i].cssRules[j].type == window.CSSRule.WEBKIT_KEYFRAMES_RULE || ss[i].cssRules[j].type == window.CSSRule.KEYFRAMES_RULE ) && ss[i].cssRules[j].name == rule) { 
						return ss[i].cssRules[j]; 
					}
				}
			}
			return null;
		}
		
		return {
			restrict: 'A',
			link:function($scope, $element, $attrs){
				var transition = $element.attr("ac-animate");
				$element.css("display", "none");
				
				//Encontrar la regla de animación aplicada y ponemos el estilo correspondiente al estado inicial
				var rule = findKeyframesRule(transition);
				var style = rule.cssRules[0].style;
				var cssRules = [];
				for ( var i = 0; i < style.length; i++ ){
					var styleName = style[i];
					$element.css(styleName, style[styleName]);
				}

				//Aplicar opciones
				var options = {
					delay:0,
					animateWhen:"visible", /*immediate*/
					duration:300
				}
				var customOptions;
				try{
					customOptions = $scope.$eval($attrs.acAnimateOptions);
				}
				catch(err){
					customOptions = {};
				}
				for ( var i in customOptions ) options[i] = customOptions[i];
				
				//Creamos la función de animación
				var animate = function(){
					$element.css("animationDuration", options.duration+"ms");
					$element.css("animationName", transition);
					$element.css("animationFillMode", "forwards");
					$element.css("animationDelay", options.delay+"ms");
				}
				
				
				//
				var checkVisibility;
				var tick = $interval(function(){
					$element.css("display", "");
					if ( options.animateWhen == "visible" && isGoingToBeVisible($element) ){
						animate();
						$interval.cancel(tick);
					}
					else if ( options.animateWhen == "immediate" ){
						animate();
						$interval.cancel(tick);
					}
				}, 10);
			}
		}
	});
})();


/** V 2.0:: filepicker**/
(function(){ 'use strict'
	angular.module('ngControls').directive('filepicker', function(){
		return{
			restrict:"EAC",
			scope:true,
			require: 'ngModel',
			bindToController:{
				accept:"@",
				maxSize:"@"
			},
			controllerAs:"filepicker",
			controller:function($scope, $element, $attrs){
				var filepicker = this, input;
				
				this.init = function(){
					input = document.createElement("input");
					input.setAttribute("type", "file");
					if ( filepicker.multiple ) input.setAttribute("multiple", true);
					if ( filepicker.isDirectory ) input.setAttribute("webkitdirectory", true);
					
				
					input.addEventListener("change", function(event){
						var modelValue;
						
						if ( filepicker.multiple ) modelValue = event.target.files;
						else modelValue = event.target.files[0];

						filepicker.ngModelController.$setDirty();
						filepicker.ngModelController.$setViewValue(modelValue);	
					});
				}
				this.subscribeModelController = function(ngModelController){
					filepicker.ngModelController = ngModelController;
					filepicker.ngModelController.$isEmpty = function(value){ return !value || ( filepicker.multiple && value.length == 0); }
					if ( filepicker.accept ) filepicker.ngModelController.$validators.accept = filepicker.checkFileType; 
					if ( filepicker.maxSize ) filepicker.ngModelController.$validators.size = filepicker.checkFileSize; 
				}
				this.checkFileType = function(modelValue, viewValue){
					if ( filepicker.multiple ) return !modelValue || !Array.from(modelValue).some(function(el){ return el.type != filepicker.accept; });
					else return !modelValue || modelValue.type == filepicker.accept;
				}
				this.checkFileSize = function(modelValue, viewValue){
					var maxSize, matches = filepicker.maxSize.match(/^(\d{1,})([K|M|G|T])?b?$/i);
					var measure = matches[2] ? matches[2].toLowerCase() : "b";
						
					switch( measure ){
						case "b" : maxSize = matches[1]; break;
						case "k" : maxSize = matches[1]*1024; break;
						case "m" : maxSize = matches[1]*1024*1024; break;
						case "g" : maxSize = matches[1]*1024*1024*1024; break;
						case "t" : maxSize = matches[1]*1024*1024*1024*1024; break;
						default: maxSize = matches[1];
					}
					if ( filepicker.multiple ) return !modelValue || !Array.from(modelValue).some(function(el){ return el.size > maxSize; });
					else return !modelValue || modelValue.size < maxSize;
					
				}
				this.onClick = function(e){
					input.style.height = '0px';
					document.body.appendChild(input);
					//e.preventDefault();
					//$(input).trigger('click');
					
					filepicker.ngModelController.$setTouched();
					input.click();
				}
				this.onDestroy = function(){
					
				}
				
				$element.bind("click", this.onClick);
				$scope.$on("$destroy", this.onDestroy);
			},
			link:function($scope, $element, $attrs, ctrl){
				var filepicker = $scope.filepicker;
				
				if ( $attrs.hasOwnProperty('isDirectory') ) filepicker.isDirectory = true;
				else filepicker.isDirectory = false;

				if ( $attrs.hasOwnProperty('isDirectory') || $attrs.hasOwnProperty('multiple') ) filepicker.multiple = true;
				else filepicker.multiple = false;
				
				
				filepicker.subscribeModelController(ctrl);
				filepicker.init();
			}
		}
	})

	//Precargamos las plantillas
	angular.module('ngControls').run(function($rootScope, $templateCache, $http, tplService) {
		$http.get(tplService.tplUrl("ac-input--file"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--image"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--checkbox"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--radio"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input--number"), {cache: $templateCache});
		$http.get(tplService.tplUrl("ac-input"), {cache: $templateCache});
	})
})();

(function(){ 'use strict'
	var getBaseExpr = function(displayName, itemName){
		var regex = new RegExp(itemName+"\\.", "g");
		return displayName.replace(regex, "");
	}
	var OPT_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;

	angular.module('ngControls').directive('autocomplete', function($q, $compile, $timeout, $document, ngSearch){
		return{
			restrict:"EAC",
			scope:true,
			require: '?ngModel',
			bindToController:{
				options:"@",
				value:"=ngModel",
				limit:"=?",
				orderBy:"@",
				template:"@",
				templateUrl:"@",
				ngChange:"&",
				searchIn:"@"
			},
			controllerAs:"autocomplete",
			controller:function($scope, $element, $attrs){
				var autocomplete = this;
				var matches = this.options.match(OPT_REGEXP);
				
				this.collectionIsExplicitArray = matches[7].charAt(0) == "[";
				if ( !this.collectionIsExplicitArray ) this.collectionName = matches[7];
				
				var collection = [];
				if (this.collectionIsExplicitArray) collection = $scope.$eval(matches[7]);
				else collection = $parse(this.collectionName)($scope.$parent);
				
				this.getLocals = function(item){ 
					var itemName = matches[4];
					var locals = {};
					locals[itemName] = item;
					return locals;
				}
				this.valueFn = function(item){
					var valueName = matches[1];
					var locals = autocomplete.getLocals(item);
					return $parse(matches[1])($scope, locals);
				}
				this.textFn = function(item){
					var displayName = matches[2] || matches[1];
					var locals = autocomplete.getLocals(item);
					return $parse(displayName)($scope, locals);
				}
				this.displayFn = function(item){
					var locals = autocomplete.getLocals(item);
					if ( autocomplete.template ) return $sce.trustAsHtml($interpolate(autocomplete.template)(locals));
					else return $sce.trustAsHtml(autocomplete.textFn(item));
				}
				this.collectionFn = function(){
					if ( autocomplete.collectionIsExplicitArray ) return collection; 
					else return $parse(autocomplete.collectionName)($scope.$parent) || [];
				}
				this.loadTemplate = function(){
					if ( autocomplete.templateUrl ) return $templateRequest(autocomplete.templateUrl).then(function(html){ autocomplete.template = html; });
					else{
						autocomplete.template = "<a class = 'autocomplete-item'>{{" +  getBaseExpr(matches[2] || "item", matches[4]) + "}}</a>";
						return $q.when();
					}
				}
				
				
				
				this.init = function(){
					var deferred = $q.defer();

					autocomplete.open = false;
					autocomplete.limit = 25;
					autocomplete.firstIndex = autocomplete.cursorIndex = 0;
					autocomplete.searchIn =  autocomplete.searchIn ? autocomplete.searchIn : getBaseExpr(matches[2] || matches[1], matches[4]);
					autocomplete.orderBy = autocomplete.orderBy ? autocomplete.orderBy : autocomplete.searchIn;
					autocomplete.searcher = ngSearch.get({
						limit:autocomplete.limit,
						orderBy:autocomplete.orderBy,
						src:autocomplete.collectionFn,
						searchIn:autocomplete.searchIn,
					});
					autocomplete.loadTemplate().then(function(){
						var tpl = "<div class = 'autocomplete-dropdown'>"
							+ "<div ng-repeat = 'item in autocomplete.searcher.results' ng-click = 'autocomplete.select($index, $event)' ng-class = '{highlight:autocomplete.cursorIndex == $index}'> " 
							+ autocomplete.template
							+ "</div>"
							+ "</div>"
						;
						autocomplete.dropdown = $compile(tpl)($scope);
						deferred.resolve();
					});

					return deferred.promise;
				}
				this.show = function(){
					if ( autocomplete.firstIndex == 0 && autocomplete.searcher.results.length == 0 ) return;

					var rect;
					rect = $element[0].getBoundingClientRect();
					
					var espacioPorArriba = rect.top;
					var espacioPorAbajo = window.innerHeight - ( rect.top + rect.height );
					
					var width = autocomplete.dropdownSize ? autocomplete.dropdownSize: rect.width;
					
					if ( espacioPorAbajo*1.618 > espacioPorArriba ){
						var top = rect.top;
						top+= rect.height;
						var top0 = top - autocomplete.size;
						var top1 = top;
						autocomplete.dropdown.css({
							bottom:'',
							top:top0 + "px", //cuando el input se activa aplica una transformación vertical de 19px
							left:rect.left + "px",
							width:width + "px",
							opacity:0
						});
						$timeout(function(){ autocomplete.dropdown.css({opacity:1, top:top1 + "px"}); });
					}
					else{
						var bottom = window.innerHeight - rect.bottom;
						
						if ( autocomplete.type == "search" || autocomplete.type == "autocomplete" ) bottom+= rect.height;
						var bottom0 = bottom - autocomplete.size;
						var bottom1 = bottom;
						autocomplete.dropdown.css({
							top:'',
							bottom:bottom0 + "px", //cuando el input se activa aplica una transformación vertical de 19px
							left:rect.left + "px",
							width:width + "px",
							opacity:0
						});
						$timeout(function(){ autocomplete.dropdown.css({opacity:1, bottom:bottom1 + "px"}); });
					}
					
					$document.find("body").append(autocomplete.dropdown);
					$element.find("input")[0].focus();
					
					var lis = autocomplete.dropdown[0].querySelectorAll(".ac-select-dropdown-item > div");
					for ( var i = 0; i < lis.length; i++){
						var isOverflowing = lis[i].clientWidth < lis[i].scrollWidth;
						if ( isOverflowing ) lis[i].setAttribute("title", autocomplete.textMap[i - autocomplete.firstIndex]);
					}
					//autocomplete.scrollToCursor();
					//autocomplete.ngModelController.$setTouched();
					autocomplete.open = true;
				}
				this.hide = function(){
					
					
					var rect;
					if (ac.theme == "material") rect = $element.find("input")[0].getBoundingClientRect();
					if (ac.theme == "flat") rect = $element[0].getBoundingClientRect();
					
					if (ac.dropdown[0].style.top){
						var top = rect.top;
						if ( ac.type == "search" || ac.type == "autocomplete" ) top+= rect.height;
						
						var top0 = top - ac.size;
						var top1 = top;
						ac.dropdown.css({opacity:0, top:top0 + "px"});
					}
					else{
						var bottom = window.innerHeight - rect.bottom;
						if ( ac.type == "search" || ac.type == "autocomplete" ) bottom+= rect.height;
						
						var bottom0 = bottom - ac.size;
						var bottom1 = bottom;
						ac.dropdown.css({opacity:0, bottom:bottom0 + "px"});
					}
					
					
					$timeout(function(){ ac.dropdown.detach(); }, 300);

					if ( !ac.value ){
						ac.selectedIndex = ac.firstIndex;
						ac.text = ac.getTextAt(ac.selectedIndex);
						ac.searcher.text = "";
					}
					ac.updateText();
					this.open = false;
				}
				this.reset = function(){
					ac.value = undefined;
					ac.text = "";
					ac.updateText();
					ac.hide();
				}
				
				$element.bind("click", this.show);
				
				
				this.init();
			},
			link:function($scope, $element, $attrs, ctrl){
				/*
				var filepicker = $scope.filepicker;
				
				if ( $attrs.hasOwnProperty('isDirectory') ) filepicker.isDirectory = true;
				else filepicker.isDirectory = false;

				if ( $attrs.hasOwnProperty('isDirectory') || $attrs.hasOwnProperty('multiple') ) filepicker.multiple = true;
				else filepicker.multiple = false;
				
				
				filepicker.subscribeModelController(ctrl);
				filepicker.init();
				*/
			}
		}
	})
})();




























