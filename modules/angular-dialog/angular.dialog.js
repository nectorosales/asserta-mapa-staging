angular.module('ngDialog', []);
(function(){ 'use strict' //PATH
	var scripts = document.getElementsByTagName("script");
	angular.module('ngDialog').value("ngDialog.PATH", scripts[scripts.length-1].src.substring(0, scripts[scripts.length-1].src.lastIndexOf('/') + 1));
})();
(function(){ 'use strict'
	angular.module('ngDialog').factory("ngDialog", function($q, $compile, $controller, $http, $injector, $document, $templateRequest, $rootScope){
		function Dialog(params){
			//Parámetros por defecto
			var defaultParams = {
				lang : navigator.language.substr(0,2) || navigator.userLanguage.substr(0,2),
				width: window.innerWidth/1.618 - 63*Math.pow(1.618, (Dialog.count+1)),
				top: 63*Math.pow(1.618, (Dialog.count+1)),
				html: "",
				type: window.innerWidth > 600? 'dialog':'cover',
				background: "transparent",
				target: "body",
				previousSibling: false,
				moveable: true,
				title:false,
				id:"dialog"+(new Date()).getTime(),
				timeout:false,
				height:"auto",
				steps:[],
				buttons:[],
			}
			
			for ( var i in defaultParams ){
				if ( i in params ) this[i] = params[i];
				else this[i] = defaultParams[i];
			}
			this.left = Math.max(($document.find("body")[0].getBoundingClientRect().width - this.width)/2, 0);

			if ( "left" in params ) this.left = params.left;
			if (this.type == "toast" && !this.timeout) this.timeout = (this.html.split(" ").length*79/250)*1000;
			if (this.type == "notify" && !this.timeout) this.timeout = (this.html.split(" ").length*79/250)*1000;

			this.background = this.type == "toast"? "#383838": this.background;

			//Ahora que ya tenemos todas la propiedades asignadas, copiamos el objeto para trabajar mejor
			var self = this;
			//Creamos los dos contenedores principales
			self.mask = angular.element("<div class ='ng-dialog-mask' data-type='"+self.type+"'></div>").attr("id", self.id);
			self.dialog = angular.element("<div class = 'ng-dialog'></div>").attr("moveable", self.moveable).css("background", self.background);
			
			
			//Creamos los diferentes layouts
			self.titleLayer = angular.element("<div class='titleLayer'><h1>"+self.title+"</h1><div class = 'steps'></div><a class = 'ng-dialog-close'>&#10005</a></div>");
			self.contentLayer = angular.element("<div class = 'contentLayer'>"+self.html+"</div>");	
			self.buttonLayer = angular.element("<div class = 'buttonLayer'></div>");

			angular.forEach(self.buttons, function(el){
				if (typeof el == "object" && !("id" in el)) el.id = "button"+(new Date()).getTime();
				if ( el == "close" ) self.buttonLayer.append(angular.element("<a class = 'ng-dialog-button'></a>").attr("id", el.id).html(Dialog.locale["Cerrar"][self.lang]).bind("click", function(){ self.hide(); }));
				else{
					var button = angular.element("<a class = 'ng-dialog-button'></a>");
					
					button.attr("id", el.id);
					button.html(el.label);
					button.bind("click", function(){ el.callback.call(self); });
					
					
					if ( typeof el.css != "undefined" ){
						for ( var prop in el.css ){
							button.css(prop, el.css[prop]);
						}
					}
					self.buttonLayer.append(button);
				}
			});
				
			//Incrustamos los layouts en el cuadro de diálaigo
			if ( self.title ) self.dialog.append(self.titleLayer).addClass("hasTitleLayer");
			self.dialog.append(self.contentLayer);
			if ( self.buttons.length ) self.dialog.append(self.buttonLayer).addClass("hasButtonLayer");

			//Función auxiliar para recuperar los parámetros con el formato correcto
			this.get = function(param){
				switch(param){
					case "width": case "height": case "top": case "left":{
						if ( /^\d+(\.\d+)?$/.test(self[param]) ) return self[param]+"px";
						else return self[param];
					}
					break;
					default: return self[param];
				}
			}
			
			//Manejador del hash. Basicamente, sirve para que al volver atrás con el navegador vaya quitando diálogos
			this.handleHashChange = function(e){	
				var hash =  e.newURL.substr(e.newURL.indexOf("#") + 1);	
				var hashValue =  parseInt(hash.substr(6)) || 0;
				var selfValue = parseInt(self.id.substr(6));
				if ( selfValue > hashValue ) self.hide();
			}
			
			//La que muestra el diálogo. El componente ya esta creado y configurado y sólo hace falta colocarlo y animarlo
			this.show = function(callback){
				//El Dialog count sirve para desplazar una miaja los nuevos cuadros de diálogo, que se más pro
				if ( this.type == "dialog" ) Dialog.count++;
				
				//No queremos doble scroll, pero eso nos la pela si es un toast
				if (self.type != "toast" && self.type != "notify" && self.type != "overlay") angular.element($document[0].querySelector(self.target)).css("overflow", "hidden");
				
				//El hash se utiliza para manejar el back button en móvil. Si lo pusiésemos después de añadir el diálogo, desplazaría el scroll al lugar del diálogo. No se usa para el Toast
				//if (self.type != "toast") window.location.hash = "#"+self.id;
				
				//Creamos el elemento
				if ( self.previousSibling ) self.mask.append(self.dialog).insertAfter(self.previousSibling);
				else if (self.type == 'replace'){
					angular.element($document[0].querySelector(self.target)).html(self.mask.append(self.dialog)); 
				}
				else if (self.type == 'overlay'){
					angular.element($document.find("body")[0]).append(self.mask.append(self.dialog)); 
				}
				else angular.element($document[0].querySelector(self.target)).append(self.mask.append(self.dialog));

				//Según el tipo, rematamos la animación
				switch ( this.type ){
					case "dialog":{	//Estándar, entra con una animación y trata de colocarse en el centro de la pantalla
						self.dialog.css("left", self.get("left")).css("width", self.get("width")).css("height", self.get("height"));
						setTimeout(function(){
							self.mask.css("background", "rgba(0, 0, 0, 0.6)");
							//self.dialog.css("transform", "scale(1)").css("top", self.get("top")).css("margin-bottom", self.get("top"));
							self.dialog.css("opacity", "1").css("top", self.get("top")).css("margin-bottom", self.get("top")).css("margin-top", "0px");
						}, 63);
					}
					break;
					case "cover":{ //Se superpone sobre el objetivo con su mismo tamaño. Es predeterminado en móvil
						self.mask.css("top", angular.element($document[0].querySelector(self.target)).scrollTop()+"px");
						setTimeout(function(){
							self.dialog.css("left", "0%");
						}, 63);
					}
					break;
					case "overlay":{	//Estándar, entra con una animación y trata de colocarse en el centro de la pantalla
						var rect = angular.element(self.target)[0].getBoundingClientRect()
						var w = window.innerWidth;
						var l = "initial";
						var r = "intial";
						if ( rect.left > w / 2 ) r = w - rect.right - 17 + "px";
						else l = rect.left + "px";
						
						var t = rect.top + rect.height;

						self.dialog.css("left", l).css("right", r).css("width", self.get("width")).css("height", self.get("height"));
						setTimeout(function(){
							//self.dialog.css("transform", "scale(1)").css("top", self.get("top")).css("margin-bottom", self.get("top"));
							self.dialog.css("opacity", "1").css("top", t+"px");
						}, 63);
					}
					break;
					case "replace":{ //Hace una animación tipo acordeón. Calcula el alto que debe ocupar si no se le pasa un alto predefinido
					}
					break;
					case "toast":{ //Se pone en la parte de abajo del target, centradico y eso, hace una animación parecida a la del dialog
						self.mask.css("bottom", "13px");
						setTimeout(function(){
							self.dialog.css("transform", "scale(1)").css("opacity", 1);
						}, 63);
						setTimeout(function(){
							self.hide();
						}, self.timeout);
					}
					break;
					
					case "notify":{ //Se pone en la parte de abajo del target, centradico y eso, hace una animación parecida a la del dialog
						self.dialog.css("opacity", "1");
						setTimeout(function(){
							self.hide();
						}, self.timeout);
					}
					break;
				}
				
				//Creamos los eventos necesarios
				self.dialog.bind("click", function(e){ self.preventHiding = true;  /*e.stopPropagation();*/  });
				if ( self.type == "accordion" ) self.mask.bind("click", function(e){ self.hide(); });
				else self.mask.bind("click", function(e){ if ( !self.preventHiding ){ self.hide(); /*e.stopImmediatePropagation();*/ } else { self.preventHiding = false; } });
				
				//Si hay un timeout, pues lo lanzamos
				if ( self.timeout ){
					setTimeout(function(){
						self.hide();
					}, self.timeout)
				}
				

				angular.element(self.mask[0].querySelector(".ng-dialog-close")).bind("click", function(e){ self.hide(); });
				if ( self.type == "dialog" && self.moveable){
					angular.element($document[0].querySelector("#"+self.id+" .titleLayer")).bind("mousedown", function(e){
						var x0 = e.pageX;
						var y0 = e.pageY;

						var x = this.getBoundingClientRect().left;
						var y = this.getBoundingClientRect().top;
						
						var xIni = x;
						var yIni = y;
						
						var fps = 63;
						var lastMove = 0;
						
						angular.element($document[0].querySelector("#"+self.id+" .dialog")).addClass("moving")
						angular.element(document).bind("mousemove", function(e){
							var now = Date.now();
							if (now > lastMove + 1000/fps) {	
								lastMove = now;  
								var x = xIni + e.pageX - x0;
								var y = yIni + e.pageY - y0;
								angular.element($document[0].querySelector("#"+self.id+" .dialog")).css({'top': y + "px",'left': x + "px"});
							}
						});
						angular.element(document).bind("mouseup", function(e){
							angular.element(document).unbind("mousemove");
							angular.element($document[0].querySelector("#"+self.id+" .dialog")).removeClass("moving")
						});
					});
				}
				if (self.type != "toast") window.addEventListener("hashchange", self.handleHashChange);
				
				
				//Ejecutamos los listeners si los hubiese
				for ( var i=0; i < this.listeners["show"].length; i++ ){
					this.listeners["show"][i].call(this);
				}
				//Y el callback
				if ( typeof callback != "undefined" ){
					callback.call(self, self);
				}
				return this;
			};
			
			//Espero que estas no necesiten una explicación
			this.hide = function(callback){
				//El listener beforeHide tiene un valor de retorno. Si devuelve false, deja de ocultar el diálogo. Es útil si queremos avisar de que hay información sin guardar y cosas desas
				var keepHiding = true; 
				for ( var i=0; i < self.listeners["beforeHide"].length; i++ ){
					keepHiding = self.listeners["beforeHide"][i].call(self);
				}
				if (keepHiding === false) return this;
				
				if ( this.type == "dialog" || this.type == "overlay" ){
					Dialog.count--;
					Dialog.dialogs.splice(Dialog.dialogs.indexOf(this), 1);
				}

				switch ( this.type ){
					case "dialog":{
						self.mask.css("background", "rgba(0, 0, 0, 0)");
						self.dialog.css("transform", "scale(0)");	
					}
					break;
					case "overlay":{
						self.dialog.css("opacity", 0);
					}
					break;
					case "cover":{
						self.mask.css("left", "100%");
					}
					break;
					case "replace":{
						self.height = self.dialog.height();
						self.dialog.css("opacity", "0");
					}
					break;
					case "toast":{ //Se pone en la parte de abajo del target, centradico y eso, hace una animación parecida a la del dialog
						self.dialog.css("opacity", 0);
					}
					break;
					case "notify":{ 
						var h = self.dialog.height();
						self.dialog.css("opacity", 0);
						//self.dialog.css("bottom", "-"+( h + 30 ) +"px");
					}
					break;
					
				}

				setTimeout(function(){
					angular.element($document[0].querySelector(self.target)).css("overflow", "");
					self.mask[0].parentNode.removeChild(self.mask[0]);// remove();
					for ( var i=0; i < self.listeners["hide"].length; i++ ){
						self.listeners["hide"][i].call(self);
					}
					if ( typeof callback != "undefined" ) callback.call();
				}, 350);
				
				
				if (self.type != "toast" && self.type != "notify"){
					window.removeEventListener("hashchange", self.handleHashChange);
					var hash =  window.location.hash.substr(window.location.hash.indexOf("#") + 1);
					if ( hash == self.id ) history.back();
				}

				return this;
			}
			this.maximize = function(){
				self.dialog.addClass("maximized");
				angular.element(self.mask[0].querySelector(".maximize")).addClass("minimize").removeClass("maximize").unbind("click").bind("click", function(){ self.minimize();});		
				for ( var i=0; i < this.listeners["maximize"].length; i++ ){
					this.listeners["maximize"][i].call(self);
				}
			}
			this.minimize = function(){
				var self = this;
				
				self.dialog.removeClass("maximized");
				angular.element(self.mask[0].querySelector(".minimize")).addClass("maximize").removeClass("minimize").unbind("click").bind("click", function(){ self.maximize();});
				for ( var i=0; i < this.listeners["minimize"].length; i++ ){
					this.listeners["minimize"][i].call(self);
				}
			}
			
			this.bind = function(bindTo, func){
				this.listeners[bindTo].push(func);
				return this;
			};
			this.unbind = function(bindTo, func){
				if ( typeof func == "undefined" ) this.listeners[bindTo] = [];
				else{
					this.listeners[bindTo].push(func);
					this.listeners = this.listeners.filter(function(f) {
						return f == func;
					});
				}
			
				return this;
			};
			this.listeners = {
				"maximize":[],
				"minimize":[],
				"show":[],
				"hide":[],
				"beforeHide":[]
			};	
			
			return this;
		}
		//Algunas traducciones, añadir más a gusto del consumidor
		Dialog.locale = {
			"Aceptar":{"es":"Aceptar", "en":"Accept"},
			"Mensaje del Sistema":{"es":"Mensaje del Sistema", "en":"System Message"},
			"Cancelar":{"es":"Cancelar", "en":"Cancel"},
			"Cerrar":{"es":"Cerrar", "en":"Close"}
		}
		Dialog.count = 0;
		Dialog.alert = function(message){
			var lang = navigator.language.substr(0,2) || navigator.userLanguage.substr(0,2);
			var locale = Dialog.locale;
			var dialog = new Dialog({
				html:"<p style='padding:19px; box-sizing:border-box'>"+message+"</p>",
				title:locale["Mensaje del Sistema"][lang] || locale["Mensaje del Sistema"]["es"],
				buttons:[],
				background:"#fff",
				width: window.innerWidth/Math.pow(1.618, 2)
			}); 
			
			dialog.show();

			return dialog;
		}
		Dialog.confirm = function(message, callback, cancel_callback, acceptLabel, cancelLabel){
			var lang = navigator.language.substr(0,2) || navigator.userLanguage.substr(0,2);
			
			if ( !acceptLabel ) acceptLabel = Dialog.locale["Aceptar"][lang];
			if ( !cancelLabel ) cancelLabel = Dialog.locale["Cancelar"][lang];

			var locale = Dialog.locale;
			return new Dialog({
				html:"<p style='padding:19px; box-sizing:border-box'>"+message+"</p>",
				title:locale["Mensaje del Sistema"][lang] || locale["Mensaje del Sistema"]["es"],
				background:"#fff",
				buttons:[
					{ label:acceptLabel, callback:function(){ callback.call(this); this.hide(); }},
					{ label:cancelLabel, callback:function(){ 
						if ( typeof cancel_callback !== "undefined" ) cancel_callback.call(this);
						this.hide(); 
					}},
				],
				width:367
			}).show();
		}
		Dialog.toast = function(message){
			var toast = new Dialog({type:"toast", html:message}).show();
		}
		Dialog.dialogs = [];
		
		window.Dialog = Dialog;
		
		return {
			toast:function(text){
				Dialog.toast(text);
			},
			alert:function(text){
				var deferred = $q.defer();
				var d = Dialog.alert(text);
				d.bind("hide", function(){
					deferred.resolve();
				});
				Dialog.dialogs.push(d);
				return deferred.promise;
			},
			showModal:function(options){
				var ngDialog = this;
				
				//Creación del scope del diálogo
				var childScope;
				if ( "parentScope" in options ) childScope = options.parentScope.$new();
				else childScope = $rootScope.$new(); 
				
				if ( "scope" in options ){
					for ( var i in options.scope ){
						childScope[i] = options.scope[i];
					}
				}
				
				//Resolución de locales.
				var resolveLocals = function(){
					var locals = angular.extend({}, options.resolve);
					angular.forEach(locals, function(value, key) {
					  locals[key] = angular.isString(value) ?
						  $injector.get(value) :
						  $injector.invoke(value, null, null, key);
					});
					return $q.all(locals);
				}
				
				//Resolución de html
				var resolveHtml = options.templateUrl ? $templateRequest(options.templateUrl) : $q.resolve(options.html);
				
				//Mostraremos el diálogo tras resolver las variables locales
				return resolveHtml.then(function(html){
					options.html = html;
					var dialog = new Dialog(options);
					return resolveLocals().then(function(locals){
						dialog.show(function(){
							//Resolución del controlador, si lo hubiere
							if ("controller" in options){
								locals["$scope"] = childScope;
								var controller = $controller(options.controller, locals);
								if ("controllerAs" in options) childScope[options.controllerAs] = controller;
								
								dialog.mask.data('$ngControllerController', controller);
								dialog.mask.children().data('$ngControllerController', controller);
							};
						
							
							var self = this;
							childScope.dismiss = function(){ self.hide(); };
							$compile(self.contentLayer)(childScope);
							if (options.askBeforeClosing) dialog.bind("beforeHide", function(){
								var keepHiding = false;
								if ( event && event.target.className == "ng-dialog-mask" ){
									var d = Dialog.confirm("No ha guardado la información. ¿Desea salir de todos modos?", function(){
										keepHiding = true;
										dialog.hide();
									});
									Dialog.dialogs.push(d);
									
									return false;
								}
								
								return true;
							});
							dialog.bind("hide", function(){ childScope.$destroy(); });
						});
						if ( "hide" in options ){ dialog.bind("hide", options.hide); }
						dialog.dismiss = function(){ dialog.hide(); }	
						Dialog.dialogs.push(dialog);
						
						return dialog;
					});
				});
			},
			showOverlay:function(options, variables){
				var scope;
				options.type = "overlay";
				var dialog = new Dialog(options);
				dialog.show(function(){
					var self = this;	
					if ( "parentScope" in options ){
						scope = options.parentScope.$new();
						if ( variables ){
							for ( var i in variables ){
								scope[i] = variables[i];
							}
						}
						scope.dismiss = function(){ self.hide(); };
					}
					else{
						scope = $rootScope.$new();
						if ( "scope" in options ){
							for ( var i in options.scope ){
								scope[i] = options.scope[i];
							}
						}
						scope.dismiss = function(){ self.hide(); };	
					}
					
					$compile(self.contentLayer)(scope);
					dialog.bind("hide", function(){
						scope.$destroy();
					});
				});
				if ( "hide" in options ){
					dialog.bind("hide", options.hide);
				}

				dialog.dismiss = function(){	
					dialog.hide();
				}
				Dialog.dialogs.push(dialog);
				
				return dialog;
			},
			notify:function(options){
				var scope;
				options.type = "notify";
				
				//obtener el scope del diálogo
				if ( "parentScope" in options ){
					var scope = options.parentScope.$new();
					if ( "scope" in options ){
						for ( var i in options.scope ){
							scope[i] = options.scope[i];
						}
					}
					scope.dismiss = function(){ self.hide(); };	
				}
				else{
					scope = $rootScope.$new();
					if ( "scope" in options ){
						for ( var i in options.scope ){
							scope[i] = options.scope[i];
						}
					}
					scope.dismiss = function(){ self.hide(); };	
				}
	
				if ( options.tplUrl ){
					$templateRequest(options.tplUrl).then(function(html){
						options.html = html;
						var dialog = new Dialog(options);
						dialog.dismiss = dialog.hide;
						dialog.show(function(){
							var self = this;	
							$compile(self.contentLayer)(scope);
						});
						dialog.bind("hide", function(){
							scope.$destroy();
						});
					});
				}
				else{
					var dialog = new Dialog(options);
					dialog.dismiss = dialog.hide;
					dialog.show(function(){
						var self = this;	
						$compile(self.contentLayer)(scope);
					});
					dialog.bind("hide", function(){
						scope.$destroy();
					});
				}
				
				return dialog;
			},
			confirm:function(text, config){
				if ( !config ) config = {};;
				
				var deferred = $q.defer();
				var d = Dialog.confirm(text, function(){
					deferred.resolve();
				}, function(){
					deferred.reject();
				}, config.acceptLabel, config.cancelLabel);
				Dialog.dialogs.push(d);
				return deferred.promise;
			},
			dismissAll:function(){
				while ( Dialog.dialogs.length > 0 ) Dialog.dialogs[Dialog.dialogs.length-1].dismiss();
			},
			dismiss:function(){
				Dialog.dialogs[Dialog.dialogs.length-1].dismiss();
			}
		}
	});
})();