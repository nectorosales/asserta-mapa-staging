angular.module('Photobox', []);

(function(){ 'use strict' //colorines
	var scripts = document.getElementsByTagName("script");
	angular.module('Photobox').constant("Photobox.PATH", scripts[scripts.length-1].src.substring(0, scripts[scripts.length-1].src.lastIndexOf('/') + 1));
	
	angular.module('Photobox').config(["Photobox.PATH", function(PATH){
		var head = document.getElementsByTagName("head")[0];
		
		if (!window.jQuery) {  
			//var script = document.querySelector("script[src*='"+url+"']");
			var url = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/core.js";
			var script = document.createElement('script');
			script.setAttribute('src', url);
			script.setAttribute('type', 'text/javascript');
			//if (charset) script.setAttribute('charset', charset);
			head.appendChild(script);
		}
		//Creamos el script
		var url = PATH+"photobox-master/photobox/jquery.photobox.js";
		var script = document.createElement('script');
		script.setAttribute('src', url);
		script.setAttribute('type', 'text/javascript');
		head.appendChild(script);
		
		//Añadimos la hoja de estilos
		var url = PATH+"photobox-master/photobox/photobox.css";
		var link = document.createElement('link');
		link.setAttribute('href', url);
		link.setAttribute('rel', 'stylesheet');
		head.appendChild(link);
	}]);
	angular.module('Photobox').directive('photobox', function($timeout){
		return{
			restrict:"AC",
			scope:{},
			link:function($scope, $element, $attrs, ctrl){
				var callback = function(){};$element.photobox($element, { thumbs:true, loop:false }, callback); 
				$timeout(function(){ 
					$element.photobox($element, { thumbs:true, loop:false }, callback); 
				});
			}
		}
	});
	angular.module('Photobox').directive('photoboxItem', function($timeout){
		return{
			restrict:"EAC",
			replace:true,
			scope:{
				href:"@?",
				alt:"@?",
				thumb:"@?",
				hidden:"=?"
			},
			template:"<a ng-show = '!hidden' href = '{{href}}'><img src = '{{thumb? thumb:href}}' alt = '{{alt}}'></a>",
		}
	});
})();











