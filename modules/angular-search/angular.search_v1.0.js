function ObservableArray(items) {
  var _self = this,
    _array = [],
    _handlers = {
      itemadded: [],
      itemremoved: [],
      itemset: []
    };

  function defineIndexProperty(index) {
    if (!(index in _self)) {
      Object.defineProperty(_self, index, {
        configurable: true,
        enumerable: true,
        get: function() {
          return _array[index];
        },
        set: function(v) {
          _array[index] = v;
          raiseEvent({
            type: "itemset",
            index: index,
            item: v
          });
        }
      });
    }
  }

  function raiseEvent(event) {
    _handlers[event.type].forEach(function(h) {
      h.call(_self, event);
    });
  }

  Object.defineProperty(_self, "addEventListener", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function(eventName, handler) {
      eventName = ("" + eventName).toLowerCase();
      if (!(eventName in _handlers)) throw new Error("Invalid event name.");
      if (typeof handler !== "function") throw new Error("Invalid handler.");
      _handlers[eventName].push(handler);
    }
  });

  Object.defineProperty(_self, "removeEventListener", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function(eventName, handler) {
      eventName = ("" + eventName).toLowerCase();
      if (!(eventName in _handlers)) throw new Error("Invalid event name.");
      if (typeof handler !== "function") throw new Error("Invalid handler.");
      var h = _handlers[eventName];
      var ln = h.length;
      while (--ln >= 0) {
        if (h[ln] === handler) {
          h.splice(ln, 1);
        }
      }
    }
  });

  Object.defineProperty(_self, "push", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function() {
      var index;
      for (var i = 0, ln = arguments.length; i < ln; i++) {
        index = _array.length;
        _array.push(arguments[i]);
        defineIndexProperty(index);
        raiseEvent({
          type: "itemadded",
          index: index,
          item: arguments[i]
        });
      }
      return _array.length;
    }
  });

  Object.defineProperty(_self, "pop", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function() {
      if (_array.length > -1) {
        var index = _array.length - 1,
          item = _array.pop();
        delete _self[index];
        raiseEvent({
          type: "itemremoved",
          index: index,
          item: item
        });
        return item;
      }
    }
  });

  Object.defineProperty(_self, "unshift", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function() {
      for (var i = 0, ln = arguments.length; i < ln; i++) {
        _array.splice(i, 0, arguments[i]);
        defineIndexProperty(_array.length - 1);
        raiseEvent({
          type: "itemadded",
          index: i,
          item: arguments[i]
        });
      }
      for (; i < _array.length; i++) {
        raiseEvent({
          type: "itemset",
          index: i,
          item: _array[i]
        });
      }
      return _array.length;
    }
  });

  Object.defineProperty(_self, "shift", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function() {
      if (_array.length > -1) {
        var item = _array.shift();
        delete _self[_array.length];
        raiseEvent({
          type: "itemremoved",
          index: 0,
          item: item
        });
        return item;
      }
    }
  });

  Object.defineProperty(_self, "splice", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: function(index, howMany /*, element1, element2, ... */ ) {
      var removed = [],
          item,
          pos;

      index = index == null ? 0 : index < 0 ? _array.length + index : index;

      howMany = howMany == null ? _array.length - index : howMany > 0 ? howMany : 0;

      while (howMany--) {
        item = _array.splice(index, 1)[0];
        removed.push(item);
        delete _self[_array.length];
        raiseEvent({
          type: "itemremoved",
          index: index + removed.length - 1,
          item: item
        });
      }

      for (var i = 2, ln = arguments.length; i < ln; i++) {
        _array.splice(index, 0, arguments[i]);
        defineIndexProperty(_array.length - 1);
        raiseEvent({
          type: "itemadded",
          index: index,
          item: arguments[i]
        });
        index++;
      }

      return removed;
    }
  });

  Object.defineProperty(_self, "length", {
    configurable: false,
    enumerable: false,
    get: function() {
      return _array.length;
    },
    set: function(value) {
      var n = Number(value);
      var length = _array.length;
      if (n % 1 === 0 && n >= 0) {        
        if (n < length) {
          _self.splice(n);
        } else if (n > length) {
          _self.push.apply(_self, new Array(n - length));
        }
      } else {
        throw new RangeError("Invalid array length");
      }
      _array.length = n;
      return value;
    }
  });

  Object.getOwnPropertyNames(Array.prototype).forEach(function(name) {
    if (!(name in _self)) {
      Object.defineProperty(_self, name, {
        configurable: false,
        enumerable: false,
        writable: false,
        value: Array.prototype[name]
      });
    }
  });

  if (items instanceof Array) {
    _self.push.apply(_self, items);
  }
}

(function testing() {

return;
 var x = new ObservableArray(["a", "b", "c", "d"]);
 
  console.log("original array: %o", x.slice());

  x.addEventListener("itemadded", function(e) {
    console.log("Added %o at index %d.", e.item, e.index);
  });

  x.addEventListener("itemset", function(e) {
    console.log("Set index %d to %o.", e.index, e.item);
  });

  x.addEventListener("itemremoved", function(e) {
    console.log("Removed %o at index %d.", e.item, e.index);
  });
 
  console.log("popping and unshifting...");
  x.unshift(x.pop());

  console.log("updated array: %o", x.slice());

  console.log("reversing array...");
  console.log("updated array: %o", x.reverse().slice());

  console.log("splicing...");
  x.splice(1, 2, "x");
  console.log("setting index 2...");
  x[2] = "foo";

  console.log("setting length to 10...");
  x.length = 10;
  console.log("updated array: %o", x.slice());

  console.log("setting length to 2...");
  x.length = 2;

  console.log("extracting first element via shift()");
  x.shift();

  console.log("updated array: %o", x.slice());

})();




(function ngSearch(){ 'use strict';
	angular.module('ngSearch', [])
		.factory('StringService', function(){
			var Latinise={};
			Latinise.latin_map={"Á":"A","Â":"A","Ä":"A","À":"A","Å":"A","Ã":"A","Æ":"AE","Ç":"C","Ð":"D","É":"E","Ê":"E","Ë":"E","È":"E","ƒ":"F","Í":"I","Î":"I","Ï":"I","Ì":"I","Ñ":"N","Ó":"O","Ô":"O","Ö":"O","Ò":"O","Ø":"O","Õ":"O","Š":"S","Ú":"U","Û":"U","Ü":"U","Ù":"U","Ý":"Y","Ÿ":"Y","Ž":"Z","Œ":"OE","á":"a","â":"a","ä":"a","à":"a","å":"a","ã":"a","æ":"ae","ç":"c","é":"e","ê":"e","ë":"e","è":"e","í":"i","î":"i","ï":"i","ì":"i","ñ":"n","ó":"o","ô":"o","ö":"o","ò":"o","ø":"o","õ":"o","š":"s","ú":"u","û":"u","ü":"u","ù":"u","ý":"y","ÿ":"y","ž":"z","œ":"oe"};

			return {
				parse_str:function(str){
					var arr = str.split("&");
					var obj = {};
					
					for ( var i = 0; i < arr.length; i++ ){
						var arr2 = arr[i].split("=");
						obj[arr2[0]] = arr2[1];
					}
					
					return obj;
				},
				removeAccents:function(string){return string.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})},
				isLatin:function(string){ return this.removeAccents(string) == string; },
				endsWith:function(string, termination, position){
					var subjectString = string.toString();
					if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
						position = subjectString.length;
					}
					position -= termination.length;
					var lastIndex = subjectString.indexOf(termination, position);
					return lastIndex !== -1 && lastIndex === position;
				},
				replaceTermination:function(string, $find, $replace){
					var $regexp = new RegExp($find+"$");
					return string.replace($regexp, $replace);
				},
				extractKeywords:function(string) {
					var $string = new String(string);
					$string = this.removeConnectors(this.removeAccents($string));
					$string = $string.replace(/[^A-Za-z0-9_]/g, '-');
					var $keywords = $string.split("-");
					for ( var i = 0; i< $keywords.length; i++ ) { 
						$keywords[i] = this.removePlurals($keywords[i]).toLowerCase();	
					}
					return $keywords;
				},
				removePlurals:function(string){
					var $string = string;
					
					if ( this.endsWith(string, "ones") ) $string = this.replaceTermination(string, "ones", "ón"); 
					else if ( this.endsWith(string, "tes") ) $string = this.replaceTermination(string, "tes", "te");
					else if ( this.endsWith(string, "es") ) $string = this.replaceTermination(string, "es", "");
					else if ( this.endsWith(string, "as") ) { $string = this.replaceTermination(string, "as", "a"); }
					else if ( this.endsWith(string, "s") && !this.endsWith(string, "us") ) $string = this.replaceTermination(string, "s", "");
					return $string;
				},
				removeConnectors:function(string) { 
					var $conectores = ["y", "de", "a", "del", "el", "la", "los", "las", "en", "entre", "desde", "hasta"];
					var $regexp = /\s/g; 
					var $words = string.split($regexp);

					var $valid_words =[];
					for ( var i in $words ){
						if ( $conectores.indexOf($words[i]) == -1 ){
							$valid_words.push($words[i]);
						}
					}
					return $valid_words.join(" ");
				},
				levenshtein:function(a, b) {
					if(a.length == 0) return b.length; 
					if(b.length == 0) return a.length;

					// swap to save some memory O(min(a,b)) instead of O(a)
					if(a.length > b.length) {
						var tmp = a;
						a = b;
						b = tmp;
					}

					var row = [];
					// init the row
					for(var i = 0; i <= a.length; i++){
						row[i] = i;
					}

					// fill in the rest
					for( var i = 1; i <= b.length; i++){
						var prev = i;
						for(var j = 1; j <= a.length; j++){
							var val;
							if(b.charAt(i-1) == a.charAt(j-1)){
								val = row[j-1]; // match
							} else {
								val = Math.min(row[j-1] + 1, prev + 1, row[j] + 1);
							}
							row[j - 1] = prev;
							prev = val;
						}
						row[a.length] = prev;
					}

					return row[a.length];
				}
			}
		})
		.factory('ngSearch', function($q, $timeout, $interval, $parse, $rootScope, StringService){
			//Esta función rellena las opciones del buscador con valores predeterminados si no son provistos en la entrada de la función.
			function buildOptions(options){
				var options = options || {};
				options.page = options.page || 1;
				options.limit = options.limit || 25;
				options.searchIn = options.searchIn || "*";
				if ( options.searchIn instanceof Array ) options.searchIn = options.searchIn.join(",");
				options.chunkSize = options.chunkSize || 100;
				options.sort = options.sort || undefined;
				
				return options;
			}
			
			//Esta función va construyendo un mapa de pesos en función de la búsqueda realizada
			function buildWeightMap(collection, wordsSearched, $index, weights, options){
				for ( var k = 0; k < options.chunkSize; k++ ){
					if ( $index >= collection.length ) break;

					var item = collection[$index];
					var weight = 0;	
					for ( var i in wordsSearched ){
						var word = StringService.removeAccents(wordsSearched[i]).toLowerCase();

						if ( word.length < 1 ) continue;
						var distance = 100;

						if ( typeof options.searchIn == "string" ){
							if ( options.searchIn == "*" ){
								for ( var key in item ){
									if ( !item[key] ) continue;
									var keywords = StringService.extractKeywords(item[key].toString());
									for ( var j in keywords ){
										if ( !keywords[j] ) continue;	
										
										var keyword = StringService.removeAccents(keywords[j].toString()).toLowerCase();
										
										distance = Math.min( StringService.levenshtein(keyword, word), distance );

										if ( keyword.indexOf(word) !== -1 ){
											distance = Math.min(distance, 0.5);
										}
									}
								}
							}
							else{
								var $searchInArray = options.searchIn.split(",").map(function(a){ return a.trim(); });
								for ( var ii = 0; ii < $searchInArray.length; ii++ ){
									var textoIdentificativo = $parse($searchInArray[ii])(item);
									var keywords = StringService.extractKeywords(textoIdentificativo);
									for ( var j in keywords ){
										if ( !keywords[j] ) continue;	
										
										var keyword = StringService.removeAccents(keywords[j].toString()).toLowerCase();
										
										distance = Math.min( StringService.levenshtein(keyword, word), distance );

										if ( keyword.indexOf(word) !== -1 ){
											distance = Math.min(distance, 0.5);
										}
									}
								}
							}
						}

						weight+=100/Math.max(1,distance*1.618);
					}
					weights.push(weight);
					$index+=1;
				}

				return $index;
			}
			
			//Una vez que tenemos el mapa de pesos, resolvemos la búsqueda y devolvemos la conexión
			function sortByMatches(collection, weights){
				var weightsMap = [];
				var maxWeight = Math.max.apply(Math, weights);

				var filteredCollection = collection.filter(function(item, index){
					var weight = weights[index];
					if ( weight*1.618 >= maxWeight ){
						weightsMap.push(weight);
						return true;
					}
					//if ( weight > 1 ) return true;
					else return false;
				});
				
				var weightsMap = weightsMap.map(function(w, index){
					return {
						weight:w,
						index:index
					}
				});

				weightsMap.sort(function(a, b){
					if (a.weight > b.weight) return -1;
					else if (b.weight > a.weight) return 1; 
					else return 0;
				});

				var sortedCollection = [];
				for ( var i = 0; i < weightsMap.length; i++ ){
					var index = weightsMap[i].index;
					sortedCollection.push(filteredCollection[index]);
				}
				
				return sortedCollection;
			}

			//Ordenar por campos
			function sortByFields(collection, sortBy){
				var $sortArray = sortBy.split(",").map(function(a){ return a.trim(); });
	
				var col = collection.slice().sort(function(a, b){		
					for ( var i = 0; i < $sortArray.length; i++ ){
						var sort = $sortArray[i];
						var lc, aVal, bVal;
						
						if ( sort.charAt(0) == "-" ){
							aVal = $parse(sort.substring(1))(a);
							bVal = $parse(sort.substring(1))(b);
						}
						else{
							aVal = $parse(sort)(a);
							bVal = $parse(sort)(b);
						}

						aVal = aVal === undefined ? "0" : aVal === null ? "0" : aVal === false ? "0" : aVal === true ? "1": aVal.toString();
						bVal = bVal === undefined ? "0" : bVal === null ? "0" : bVal === false ? "0" : bVal === true ? "1": bVal.toString();


						if ( sort.charAt(0) == "-" ){
							if (!isNaN(aVal) && !isNaN(bVal)) lc = parseFloat(aVal) > parseFloat(bVal) ? -1: parseFloat(aVal) < parseFloat(bVal) ? 1 : 0;
							else lc = bVal.localeCompare(aVal);
						}
						else{
							if (!isNaN(aVal) && !isNaN(bVal)) lc = parseFloat(aVal) < parseFloat(bVal) ? -1: parseFloat(aVal) > parseFloat(bVal) ? 1 : 0;
							else lc =  aVal.localeCompare(bVal);
						}
						if ( lc != 0 ) return lc;
					}
					return 0;	
				});

				return col;
			}
			
			//El buscador
			var Searcher = function(options){
				var options = buildOptions(options);
				var self = this;

				var iterator;
				
				//Devuelve una promesa que resuelve con un array ordenado por pesos con los elementos resultantes de la búsqueda con este formato: [peso, indice]
				this.buildMap = function(){
					var deferred = $q.defer();
					var wordsSearched = StringService.extractKeywords(this.text);

					if ( iterator ) iterator.cancel();
					
					iterator = {
						text:self.text,
						run:function(){
							var iteration = this;
							
							if ( iteration.text == "" ){
								self.map = self.src.map(function(item, index){ return [0,index]; });
								deferred.resolve(self.map);
							}
							else{
								var $index = 0;	
								var weightMap = [];

								iteration.tick = function(){
									if ( iteration.cancelled ){
										deferred.reject();
										return;
									}
									if ( $index >= self.src.length ){	
										var maxWeight = weightMap.length == 0 ? 0 : Math.max.apply(Math, weightMap);
										var arr = weightMap.map(function(value, i){
											return [value, i];
										});

										arr = arr.filter(function(item){
											var weight = item[0];
											if ( weight*1.618 >= maxWeight ){
												return true;
											}
										});
										
										
										
										arr.sort(function(a, b){
											if ( a[0] > b[0] ) return -1;
											else if (b[0] > a[0]) return 1; 
											else return 0;
										});

										self.map = arr;
										deferred.resolve(arr);
									}
									else{
										$index = buildWeightMap(self.src, wordsSearched, $index, weightMap, options);
										$timeout(function(){ iteration.tick(); },1);
									}
								}
									
								$timeout(function(){
									iteration.tick();
								}, 0);
							}
						},
						cancel:function(){
							this.cancelled = true;
						}	
					}
					
					iterator.run();
					
					return deferred.promise;
				}
				this.buildMatches = function(){
					var data = this.map;
					var indexes = data.map(function(item){ return item[1]; });		
					var matches = [];
					for ( var i = 0; i < indexes.length; i++ ){
						var index = indexes[i];
						if ( this.filter && this.filter(self.src[index]) == false ) continue;
						matches.push(self.src[index]);
					}
					
					if (this.orderBy && this.text == "") matches = sortByFields(matches, this.orderBy);
					return matches;
				}
				this.buildResults = function(){
					//var matches = this.matches.filter(this.filter);
					
					this.total = this.matches.length;
					this.first = Math.min((this.page-1)*this.limit+1, this.total);
					this.last = Math.min(this.total, this.limit*this.page);
					this.results = this.matches.slice(this.first-1,this.last);

					for ( var i = 0; i < this.listeners.resolve.length; i++ ) this.listeners.resolve[i].call(this, this.results);
				}
				this.search = function(){
					var deferred = $q.defer();
					this.buildMap().then(function(data){
						self.matches = self.buildMatches();	
						deferred.resolve(self.matches);
					});
					return deferred.promise;
				}
				this.sort = function(){
					this.matches = sortByFields(this.matches, this.orderBy);
				}
				this.refresh = function(){
					this.matches = this.buildMatches();
					this.buildResults();
					this.page = 1;
				}
				
				//Eventos
				this.listeners = {
					search:[], //Cuando comienza la búsqueda
					resolve:[] //Cuando se resuelve una búsqueda
				}
				this.on = function(action, callback){
					switch (action){
						case "search" : self.listeners.search.push(callback); break;
						case "resolve" : self.listeners.resolve.push(callback); break;
					}
				}	
				
				//Métodos
				this.toggleOrder = function(field){
					var descString = "-"+field;
					var ascString = field;
					
					if ( !self.orderBy ){
						self.orderBy = ascString;
						return;
					}
					var $sortArray = self.orderBy.split(",").map(function(a){ return a.trim(); }).sort(function(a, b){
						if ( a == descString || a == ascString ) return -1;
						else if ( b == descString || b == ascString ) return 1;
						else return 0;
					});
					var orderBy = $sortArray.join(",");

					if ( orderBy.indexOf(descString) !== -1 ) self.orderBy = orderBy.replace(descString, ascString);
					else if ( orderBy.indexOf(ascString) !== -1 ) self.orderBy = orderBy.replace(ascString, descString); 
					else self.orderBy = ascString+","+self.orderBy;
				}	
				this.sortDirection = function(field){
					if ( !self.orderBy ) return "";
					if (self.orderBy.indexOf("-"+field) !== -1 ) return "desc";
					else if (self.orderBy.indexOf(field) !== -1 ) return "asc";
					else return "";
				}
				
				//Variables
				this.ready = false;
				if ( !options.src ) this.src = [];
				else if ( typeof options.src == "function" ){
					this.src = options.src();
					this.fn = options.src;
				}
				else{
					this.src = options.src;
				}
				this.filter = options.filter || function(){ return true; }
				this.map = []; //Mapa con los pesos/indices de la búsqueda
				this.matches = []; //Resultados de la búsqueda
				this.results = []; //Resultados paginados, filtrados y ordenados
				this.page = options.page;
				this.limit = options.limit;
				this.orderBy = options.orderBy;
				this.text = options.text || "";
				this.total = undefined;
				this.first = undefined;
				this.last = undefined;
				this.search().then(function(){
					self.buildResults();
					self.ready = true;
				});	
			
			
				
			
				if ( this.fn ){	
					var $watchCollection = $rootScope.$watchCollection(this.fn, function(newVal, oldVal){ self.src = newVal; });
					this.destroy = function(){
						$watchCollection();
					}
				}
				
				/*
				if ( this.fn ){	
					var len = this.src.length;
					var n = len;
					var interval = setInterval(function(){
						
						
						var arr = self.fn();
						n = arr.length;
						if ( n != len ){
							len = n;
							self.src = arr;
						}
					}, 1000);
					
					this.destroy = function(){
						clearInterval(interval);
					}
				}
				*/
			}

			Object.defineProperty(Searcher.prototype, "src", {
				get: function () {
					return this.__src;
				},
				set: function (newValue) {
					//this.__src = new ObservableArray(newValue);
					this.__src = newValue;
						
					if ( this.ready ){
						var self = this;
						this.search().then(function(){
							self.buildResults();
						});
					}
				},
				enumerable: true,
				configurable: true
			});	
			Object.defineProperty(Searcher.prototype, "text", {
				get: function () {
					return this.__text;
				},
				set: function (newValue) {
					this.__text = newValue;
					
					if ( this.ready ){
						var self = this;
						this.search().then(function(){
							self.buildResults();
							self.page = 1;
						});
					}
				},
				enumerable: true,
				configurable: true
			});
			Object.defineProperty(Searcher.prototype, "page", {
				get: function () {
					return this.__page;
				},
				set: function (newValue, oldValue) {
					if ( newValue != oldValue ){
						this.__page = newValue;
						this.buildResults();
					}
				},
				enumerable: true,
				configurable: true
			});
			Object.defineProperty(Searcher.prototype, "limit", {
				get: function () {
					return this.__limit;
				},
				set: function (newValue, oldValue) {	
					if ( newValue != oldValue ){
						this.__limit = newValue;
						this.buildResults();
					}
				},
				enumerable: true,
				configurable: true
			});
			Object.defineProperty(Searcher.prototype, "orderBy", {
				get: function () {
					return this.__orderBy;
				},
				set: function (newValue, oldValue) {
					if ( newValue != oldValue ){
						this.__orderBy = newValue;
						this.sort();
						this.buildResults();
					}
				},
				enumerable: true,
				configurable: true
			});
	
			return {
				searcher: Searcher,
				get:function(options){
					var searcher = new Searcher(options);
					return searcher;
				}
			}
		})
})()