'use strict';
angular.module('app.controllers', [])
	.controller('app', function ($scope, $cookies, $q, $window, $state, $timeout, $stateParams, $transitions, Api, $mdDialog) {
		var app = this;
		app.ready = false;

		app.paramsHome = "";
		
		this.init = function(){

			app.idioma = $cookies.get("idioma");
			var navegador = navigator.language;
			var idioma_navegador = navegador.split('-');
			if(!app.idioma){
				app.idioma = idioma_navegador[0];
			}
			
			return $q.all([
			]).then(function(){
				app.ready = true;
			});
		}

		this.chooseLang = function(idioma){
			Api.post("seleccionar_idioma?codigo_idioma=" + idioma.codigo).then(function(response){
				//Api.get("centros_sanitarios_traducidos?codigo_idioma="+idioma.codigo);
				$cookies.put("idioma", idioma.codigo);
				app.idioma = idioma.codigo;
				app.idioma_seleccionado = idioma.nombre;
			});
		}
		
		this.init();

		$transitions.onBefore({from:"map", to:"centro_sanitario"}, function(){
			app.lastSearch = $stateParams.search;
			app.lastCentre = $stateParams.ce;
			app.lastCountry = $stateParams.co;
			app.lastProffesional = $stateParams.pr;
			app.lastObjetivePublic = $stateParams.op;
			app.lastService = $stateParams.se;
		})

	})

	.controller('home', function($scope, $q, Api, $state, $stateParams){
		var app = $scope.app;
		var vm = this;

		this.ready = false;

		vm.filtros = {
	    	servicios_centros: [],
	    	profesionales_centros: []
	    }

		this.init = function(){
			return $q.all([
				Api.get("paises").then(function(response){
					$scope.paises = response.data;
					if ( $stateParams.co ) { $scope.pais = $scope.paises.filter(function(el){
						return el.id == $stateParams.co;
					})[0] }
				}),
				Api.get("centros_sanitarios").then(function(response){
					$scope.centros_sanitarios = response.data;
					if ( $stateParams.ce ) { $scope.centro = $scope.centros_sanitarios.filter(function(el){
						return el.id == $stateParams.ce;
					})[0] }
				}),
				Api.get("poblaciones").then(function(response){
					$scope.poblaciones = response.data;
					if ( $stateParams.op ) { $scope.poblacion = $scope.poblaciones.filter(function(el){
						return el.id == $stateParams.op;
					})[0] }
				}),
				Api.get("servicios").then(function(response){
					$scope.servicios = response.data;
					for (var i = 0; i < $scope.servicios.length; i++) {
						if($scope.servicios[i].id == 1) $scope.servicios[i].icon = "img/Health.png";
						if($scope.servicios[i].id == 2) $scope.servicios[i].icon = "img/Living.png";
						if($scope.servicios[i].id == 3) $scope.servicios[i].icon = "img/Social.png";
						if($scope.servicios[i].id == 4) $scope.servicios[i].icon = "img/Essentials.png";
						if($scope.servicios[i].id == 5) $scope.servicios[i].icon = "img/Education.png";
						if($scope.servicios[i].id == 6) $scope.servicios[i].icon = "img/Extra.png";
					}

					vm.filtros.servicios_centros = $scope.servicios.map(function(){ return false; });

					if ( $stateParams.se ) { 
						var ids = $stateParams.se.split(",");

						for (var i = 0; i < $scope.servicios.length; i++){
							for ( var j = 0; j < ids.length; j++ ){
								if ( $scope.servicios[i].id == ids[j] ){
									vm.filtros.servicios_centros[i] = true;
								} 
							}
						}
					}
				}),
				Api.get("profesionales").then(function(response){
					$scope.profesionales = response.data;
					for (var i = 0; i < $scope.profesionales.length; i++) {
						if($scope.profesionales[i].id == 4) $scope.profesionales[i].icon = "img/Professionals.png";
						if($scope.profesionales[i].id == 11) $scope.profesionales[i].icon = "img/C_User.png";
					}

					vm.filtros.profesionales_centros = $scope.profesionales.map(function(){ return false; });

					if ( $stateParams.pr ) { 
						var ids = $stateParams.pr.split(",");

						for (var i = 0; i < $scope.profesionales.length; i++){
							for ( var j = 0; j < ids.length; j++ ){
								if ( $scope.profesionales[i].id == ids[j] ){
									vm.filtros.profesionales_centros[i] = true;
								} 
							}
						}
					}
				})
			]).then(function(){ 
				vm.ready = true;
			});
		}

	    this.goMap = function(){
	    	app.paramsHome = angular.copy($stateParams);
			$state.go("map", $stateParams);
		}

		this.updateFilter = function(){
			
			var params = angular.copy($stateParams);

			if ( $scope.centro ){
				params.ce = $scope.centro.id;
			}else{
				params.ce = "";
			}	
			if ( $scope.pais ){
				params.co = $scope.pais.id;
			}else{
				params.co = "";
			}	
			if ( $scope.poblacion ){
				params.op = $scope.poblacion.id;
			}else{
				params.op = "";
			}	

			var ids_profesionales = [];

			if ( vm.filtros.profesionales_centros ) {
				var profesional = $scope.profesionales;
				for (var i = 0; i < vm.filtros.profesionales_centros.length; i++) {
					if( vm.filtros.profesionales_centros[i] ){
						ids_profesionales.push(profesional[i].id);
					}
				}
				params.pr = ids_profesionales+"";
			}

			var ids_servicios = [];

			if ( vm.filtros.servicios_centros ) {
				var servicio = $scope.servicios;
				for (var i = 0; i < vm.filtros.servicios_centros.length; i++) {
					if( vm.filtros.servicios_centros[i] ){
						ids_servicios.push(servicio[i].id);
					}
				}
				params.se = ids_servicios+"";
			}

			$state.transitionTo($state.current, params);
		}

		this.onReadyStateChange = function(readyState){
			if (readyState === true) vm.init();
		}

		$scope.$watch("app.ready", this.onReadyStateChange);
	})
	
	.controller('map', function ($scope, $q, Api, $mdDialog, NgMap, $geo, $mdSidenav, $state, $stateParams){
		var app = $scope.app;
		var vm = this;

		this.ready = false;

		this.toggleSideMenu = function(){
			$mdSidenav("side-menu").toggle();
		}	

		var originatorEv;	

		this.openMenu = function($mdMenu, ev) {
		    originatorEv = ev;
		    $mdMenu.open(ev);
	    };

	    vm.filtros = {
	    	//poblaciones_atendidas: [],
	    	//profesionales_centros: []
	    }

	    var ids = [];

		this.init = function(){

			$scope.menu = [{enlace: '#', icono: 'help', titulo: 'Help'},{enlace: '#', icono: 'business', titulo: 'Sign up'},{enlace: '#', icono: 'account_circle', titulo: 'Login'},]

			if ( $stateParams.search ) { $scope.buscador = $stateParams.search; }

			$scope.zoom = 3;
				
			return $q.all([
				Api.get("centros_sanitarios").then(function(response){
					$scope.centros_sanitarios = response.data;
					if ( $stateParams.ce ) { $scope.centro = $scope.centros_sanitarios.filter(function(el){
						return el.id == $stateParams.ce;
					})[0] }
				}),
				Api.get("paises").then(function(response){
					$scope.paises = response.data;
					if ( $stateParams.co ) { $scope.pais = $scope.paises.filter(function(el){
						return el.id == $stateParams.co;
					})[0] }
				}),
				Api.get("poblaciones").then(function(response){
					$scope.poblaciones = response.data;
					vm.filtros.poblaciones_atendidas = $scope.poblaciones.map(function(){ return false; });

					if ( $stateParams.op ) { 
						var ids = $stateParams.op.split(",");

						for (var i = 0; i < $scope.poblaciones.length; i++){
							for ( var j = 0; j < ids.length; j++ ){
								if ( $scope.poblaciones[i].id == ids[j] ){
									vm.filtros.poblaciones_atendidas[i] = true;
								} 
							}
						}

						if(ids.length == vm.filtros.poblaciones_atendidas.length){
							 $scope.selectedAll = true;
						}else{
							 $scope.selectedAll = false;
						}
					}
				}),
				Api.get("profesionales").then(function(response){
					$scope.servicios = response.data;

					vm.filtros.profesionales_centros = $scope.servicios.map(function(){ return false; });

					if ( $stateParams.pr ) { 
						var ids = $stateParams.pr.split(",");

						for (var i = 0; i < $scope.servicios.length; i++){
							for ( var j = 0; j < ids.length; j++ ){
								if ( $scope.servicios[i].id == ids[j] ){
									vm.filtros.profesionales_centros[i] = true;
								} 
							}
						}
					}
				}),
				Api.get("lenguajes").then(function(response){
					$scope.lenguajes = response.data;
				}),
				$geo.getCurrentPosition().then(function(position){
					$scope.myPosition = [position.coords.latitude, position.coords.longitude];
					$scope.position = [position.coords.latitude, position.coords.longitude];
					return Api.get("distancias_centros_sanitarios?mi_lat="+$scope.position[0]+"&mi_lng="+$scope.position[1]).then(function(response){
						$scope.distancias_centros_sanitarios = response.data;
					});
				}),
				NgMap.getMap().then(function(map){ vm.map = map; })
			]).then(function(){ 
				vm.form.$setPristine();
				vm.ready = true;
			});
		}

		this.search = function(){
			vm.toggleSideMenu();
			vm.form.$setPristine();
		}

		this.onSelect = function(centro_sanitario){
			if(!centro_sanitario){
				$scope.zoom = 8;
				$scope.position = [position.coords.latitude, position.coords.longitude];
			}else{
				$scope.zoom = 13;
				$scope.position = [centro_sanitario.lat, centro_sanitario.lng];
			}
		}

		var icon_education = 'img/marker/Education.png';
		var icon_essentials = 'img/marker/Essentials.png';
		var icon_health = 'img/marker/Health.png';
		var icon_living = 'img/marker/Living.png';
		var icon_social = 'img/marker/Social.png';
		var icon_extra = 'img/marker/Extra.png';

		this.filterCenters = function(){
			
			var  centros_sanitarios = $scope.centros_sanitarios || [];

			centros_sanitarios = centros_sanitarios.filter(function(centro_sanitario){

				if ( $scope.buscador && centro_sanitario.nombre.toLowerCase().indexOf($scope.buscador.toLowerCase()) === -1 )  return false;

				if ( $scope.pais && centro_sanitario.id_pais != $scope.pais.id )  return false;

				if ( $scope.centro_sanitario && centro_sanitario.nombre != $scope.centro_sanitario.nombre )  return false;

				for ( var i = 0; i < vm.filtros.poblaciones_atendidas.length; i++ ){
					if ( vm.filtros.poblaciones_atendidas[i] ){
						var poblacion = $scope.poblaciones[i];
						if ( !centro_sanitario.poblaciones_atendidas.some(function(el){			
							return el.id == poblacion.id;
						}) ) return false; 
					}
				}

				if( centro_sanitario.id_servicio == 0) $scope.icon = icon_extra;
				if( centro_sanitario.id_servicio == 1) $scope.icon = icon_health;
				if( centro_sanitario.id_servicio == 2) $scope.icon = icon_living;
				if( centro_sanitario.id_servicio == 3) $scope.icon = icon_social;
				if( centro_sanitario.id_servicio == 4) $scope.icon = icon_essentials;
				if( centro_sanitario.id_servicio == 5) $scope.icon = icon_education;

		
				for ( var i = 0; i < vm.filtros.profesionales_centros.length; i++ ){
					if ( vm.filtros.profesionales_centros[i] ){
						var profesional = $scope.profesionales[i];
						if ( !centro_sanitario.profesionales_centros.some(function(el){			
							return el.id == profesional.id;
						}) ) return false; 
					}
				}

				return true;
			})
			return centros_sanitarios;
		}

		/*this.distancia_centro_sanitario = function(centro_sanitario){
			var distancias = $scope.distancias_centros_sanitarios;

			if ( !distancias ) return null;

			var returnValue;

			for (var i = 0; i < distancias.length; i++) {
				if(distancias[i].id == centro_sanitario.id) {
					if ( !distancias[i].distance ) return null;

					returnValue = parseFloat(distancias[i].distance) ;   
					break;
				}
			}

			return returnValue;
		}*/

		$scope.limitNumber = 3;

		this.checkAll = function () {
			var poblacion = $scope.poblaciones;
	        if ($scope.selectedAll) {
	            $scope.selectedAll = true;
	            for (var i = 0; i < vm.filtros.poblaciones_atendidas.length; i++) {
	            	vm.filtros.poblaciones_atendidas[i] = true;
	            }
	        } else {
	            $scope.selectedAll = false;
	            for (var i = 0; i < vm.filtros.poblaciones_atendidas.length; i++) {
	            	vm.filtros.poblaciones_atendidas[i] = false;
	            }
	        }
	        
	        vm.update();
	    };

	    this.checkAllService = function () {
			var poblacion = $scope.poblaciones;
	        if ($scope.checkAllService) {
	            $scope.checkAllService = true;
	            for (var i = 0; i < vm.filtros.profesionales_centros.length; i++) {
	            	vm.filtros.profesionales_centros[i] = true;
	            }
	        } else {
	            $scope.checkAllService = false;
	            for (var i = 0; i < vm.filtros.profesionales_centros.length; i++) {
	            	vm.filtros.profesionales_centros[i] = false;
	            }
	        }
	        
	        vm.update();
	    };

		this.showHealthCenterInfo = function(event, centro_sanitario){
			$scope.centro_seleccionado = centro_sanitario;
			vm.map.showInfoWindow('hc_info', this);
			$scope.localizacion = centro_sanitario.lat+","+centro_sanitario.lng;
		}

		this.onReadyStateChange = function(readyState){
			if (readyState === true) vm.init();
		}

		this.onServiceArrayChange = function(arr){
			if ( !arr ) $scope.checkedNumber = 0;
			else $scope.checkedNumber = arr.filter(function(checked){ return checked; }).length;
		}

		this.update = function(){
			ids = [];
			var ids_profesionales = [];
			var params = angular.copy($stateParams);
			if ( $scope.buscador ) {
				params.search = $scope.buscador;
			}else{
				params.search = "";
			}
			if ( $scope.centro_sanitario ){
				params.ce = $scope.centro_sanitario.id;
			}else{
				params.ce = "";
			}	
			if ( $scope.pais ){
				params.co = $scope.pais.id;
			}else{
				params.co = "";
			}	

			if ( vm.filtros.poblaciones_atendidas ) {
				var poblacion = $scope.poblaciones;
				for (var i = 0; i < vm.filtros.poblaciones_atendidas.length; i++) {
					if( vm.filtros.poblaciones_atendidas[i] ){				
						ids.push(poblacion[i].id);
					}
				}
				params.op = ids+"";
			}

			if ( vm.filtros.profesionales_centros ) {
				var profesional = $scope.profesionales;
				for (var i = 0; i < vm.filtros.profesionales_centros.length; i++) {
					if( vm.filtros.profesionales_centros[i] ){			
						ids_profesionales.push(profesional[i].id);
					}
				}
				params.pr = ids_profesionales+"";
			}

			$state.transitionTo($state.current, params);
		}

		

		$scope.$watchCollection("vm.filtros.profesionales_centros", this.onServiceArrayChange)
		$scope.$watch("app.ready", this.onReadyStateChange);

	})
	.controller('centro_sanitario', function($scope, $transitions, $state, $stateParams, Api){
		var app = $scope.app;
		var vm = this;

		this.init = function(){
			var id_centro_sanitario = ($stateParams.url_centro_sanitario).split("-");
			id_centro_sanitario = id_centro_sanitario[0];

			Api.get("info_centro_sanitario_traducido?id="+id_centro_sanitario+"&codigo_idioma="+app.idioma).then(function(response){
				$scope.centro_sanitario = response.data;
			});
					
		}

		this.back = function(){
			var lastSearch = app.lastSearch;
			var lastCentre = app.lastCentre;
			var lastCountry = app.lastCountry;
			var lastObjetivePublic = app.lastObjetivePublic;
			var lastService = app.lastService;
			var lastProfessional = app.lastProfessional;
			var params = {
				search:lastSearch,
				ce:lastCentre,
				co:lastCountry,
				op:lastObjetivePublic,
				se:lastService,
				pr:lastProfessional
			}
			$state.go("map", params);
		}
	})
;