'use strict';
/* Directives */
angular.module('app.directives', [])
	.directive('bindHtmlCompile', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
				var ready = false;
				
				var unwatch = scope.$watch(attrs.bindHtmlCompile, function(newVal, oldVal){
					if ( newVal ){
						element.html(newVal);
						$compile(element.contents())(scope);
					}
					else{
						element.html('');
					}
				});
            }
        };
    }])
;
