'use strict';
angular.module('app', ['ui.router', 'app.controllers', 'app.filters', 'app.services', 'app.directives', 'ngMaterial', 'ngMessages', 'ngMap', 'ngCookies'])
	.config(function($stateProvider, $locationProvider, $sceDelegateProvider, $compileProvider, $provide){
		//Estados de la aplicación
		
		$stateProvider.state('map', {url:'/map?search&ce&co&op&se&pr', reloadOnSearch:false, templateUrl: 'templates/map.html', controller: 'map', controllerAs: 'vm'});
		$stateProvider.state('home', {url:'/?ce&co&op&pr&se', reloadOnSearch:false, templateUrl: 'templates/home.html', controller: 'home', controllerAs: 'vm'});	
		$stateProvider.state('centro_sanitario', {url:'/map/centros_sanitarios/:url_centro_sanitario', templateUrl: 'templates/centro_sanitario.html', controller: 'centro_sanitario', controllerAs: 'vm'});	
		$stateProvider.state('otherwise', {url:"*path", templateUrl: 'templates/404.html', controller: 'Pagina.404', controllerAs:"vm", id:'broken'});
		$locationProvider.html5Mode(true);
		
		//whitelist: dominios desde los que permitimos cargar recursos.
		$sceDelegateProvider.resourceUrlWhitelist([
			'self',
			'https://cdn.jsdelivr.net/**',
			'https://ajax.googleapis.com/**'
		]);
		
		//Saneamos los enlaces que queramos permitir
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|sms|tel|blob):/);
		
		//Añadir método set Touched
		$provide.decorator('formDirective', ['$delegate', function ($delegate) {
			var proto = $delegate[0].controller.prototype;

			proto.$getControls = function(){
				var controls = {};
				for ( var key in this ){
					var value = this[key]
					if (typeof value === 'object' && value.hasOwnProperty('$modelValue')){
						controls[key] = value;     
					}
				}
				return controls;
			}
			proto.$setTouched = function() {
				var controls = this.$getControls();
				for ( var key in controls ){
					var control = controls[key];
					control.$setTouched();
				}
			};

			return $delegate;
		}]);
		$provide.decorator('ngFormDirective', ['$delegate', function ($delegate) {
			var proto = $delegate[0].controller.prototype;

			proto.$getControls = function(){
				var controls = {};
				for ( var key in this ){
					var value = this[key]
					if (typeof value === 'object' && value.hasOwnProperty('$modelValue')){
						controls[key] = value;     
					}
				}
				return controls;
			}
			proto.$setTouched = function() {
				var controls = this.$getControls();
				for ( var key in controls ){
					var control = controls[key];
					control.$setTouched();
				}
			};

			return $delegate;
		}])
		
		//Descomentar si queremos quitar limpiar la caché en cada petición
		/*
		if (!$httpProvider.defaults.headers.get) {
			$httpProvider.defaults.headers.get = {};    
		}
		$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
		$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
		$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
		*/
	})
	.run(function($rootScope){
		$rootScope.copy = function(text){
			var textarea = document.createElement('textarea');
			textarea.textContent = text;
			document.body.appendChild(textarea);

			var selection = document.getSelection();
			var range = document.createRange();
			//  range.selectNodeContents(textarea);
			range.selectNode(textarea);
			selection.removeAllRanges();
			selection.addRange(range);

			console.log('copy success', document.execCommand('copy'));
			selection.removeAllRanges();

			document.body.removeChild(textarea);return;
		}
	})
;