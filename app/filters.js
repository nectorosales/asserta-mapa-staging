'use strict';

angular.module('app.filters', [])
	.filter('interpolate', ['version', function(version) {
		return function(text) {
			return String(text).replace(/\%VERSION\%/mg, version);
		};
	}])
	.filter('range', function() {
		return function(input, min, max) {
			min = parseInt(min); //Make string input int
			max = parseInt(max);
			for (var i=min; i<=max; i++)
				input.push(i);
				return input;
			};
	})
	.filter('filesize', function($filter) {
		return function(input) {
			if ( input === null ) return "Desconocido";

			if ( input < 1024 ) return $filter("number")(input, 2) + "B";
			if ( input < 1024*1024 ) return $filter("number")(input/1024, 2) + "KB";
			if ( input < 1024*1024*1024 ) return $filter("number")(input/(1024*1024), 2) + "MB";
			if ( input < 1024*1024*1024*1024 ) return $filter("number")(input/(1024*1024*1024), 2) + "GB";
			if ( input < 1024*1024*1024*1024*1024 ) return $filter("number")(input/(1024*1024*1024*1024), 2) + "TB";
		}
	})
	.filter('to_trusted', ['$sce', function($sce){
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}])
	.filter('to_trusted_url', ['$sce', function($sce){
		return function(url) {
			return $sce.trustAsResourceUrl(url);
		};
	}])
;